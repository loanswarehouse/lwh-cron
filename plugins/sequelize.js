'use strict';

const Joi = require('joi');
const Hoek = require('hoek');
const Sequelize = require('sequelize');
const glob = require('glob');

const internals = {
	defaults: {
		pool: {
			max: 5,
			min: 0,
			idle: 10000,
		},
		logging: false,
	},
};

internals.schema = Joi.object({
	database: Joi.string(),
	username: Joi.string(),
	password: Joi.string(),
	host: Joi.string(),
	port: Joi.number(),
	dialect: Joi.string(),
	pool: Joi.object(),
	logging: Joi.any(),
});

exports.register = function (server, options, next) {
	const settings = Hoek.applyToDefaults(internals.defaults, options);
	const results = Joi.validate(settings, internals.schema);
	Hoek.assert(!results.error, results.error);

	server.ext('onPreHandler', internals.onPreHandler);

	const db = {};
	const sequelize = new Sequelize(settings.database, settings.username, settings.password, {
		host: settings.host,
		port: settings.port,
		dialect: settings.dialect,
		pool: settings.pool,
		logging: settings.logging,
		dialectOptions: {
			multipleStatements: true,
		},
		define: {
			paranoid: true,
			instanceMethods: {
				jsonApiAttributes: function () {
					return this.$options.attributes.filter(function (i) {
						return i !== 'id';
					});
				},
			},
		},
	});

	const models = glob.sync('models/*.js');
	models.forEach(function (file) {
		const model = sequelize.import('../' + file);
		db[model.name] = model;
	});

	Object.keys(db).forEach(function (modelName) {
		if ('associate' in db[modelName]) {
			db[modelName].associate(db);
		}
	});

	sequelize.sync().then(function () {
		db.sequelize = sequelize;
		server.expose('db', db);
		return next();
	});
};

/*eslint-disable no-param-reassign*/
internals.onPreHandler = function (request, reply) {
	const page = request.query.pageNumber || request.query.page || 1;
	const perPage = request.query.pageSize || request.query.size || 20;

	const skip = page * perPage - perPage; // if page == 1 => skip = 0

	request.app.pagination = {
		limit: perPage,
		offset: skip,
	};

	return reply.continue();
};

exports.register.attributes = {
	name: 'sequelize',
};
