'use strict';

const Joi = require('joi');
const Hoek = require('hoek');
const Producer = require('sqs-producer');
const Consumer = require('sqs-consumer');
const VError = require('verror');

const internals = {
	defaults: {
		region: 'eu-west-1',
	},
};

internals.schema = Joi.object({
	region: Joi.string().required(),
});

exports.register = function (server, options, next) {
	internals.settings = Hoek.applyToDefaults(internals.defaults, options || {});
	Joi.assert(internals.settings, internals.schema, 'Invalid options');

	server.dependency('aws', function (server, next) {
		internals.server = server;

		server.expose('createProducer', internals.createProducer);
		server.expose('createConsumer', internals.createConsumer);

		return next();
	});

	return next();
};

internals.createProducer = function (options) {
	const optionsSchema = Joi.object({
		name: Joi.string().required(),
		queueUrl: Joi.string().required(),
		region: Joi.string().optional(),
	}).label('options');
	Joi.assert(options, optionsSchema, 'Invalid options');

	const producer = Producer.create({
		queueUrl: options.queueUrl,
		region: options.region || internals.settings.region,
		sqs: internals.server.plugins.aws.sqs,
	});

	return producer;
};

internals.createConsumer = function (options) {
	const optionsSchema = Joi.object({
		name: Joi.string().required(),
		queueUrl: Joi.string().required(),
		handler: Joi.func().required(),
		region: Joi.string().optional(),
		messageAttributeNames: Joi.array().optional(),
	}).label('options');
	Joi.assert(options, optionsSchema, 'Invalid options');

	const consumer = Consumer.create({
		queueUrl: options.queueUrl,
		region: options.region || internals.settings.region,
		handleMessage: options.handler,
		sqs: internals.server.plugins.aws.sqs,
		messageAttributeNames: options.messageAttributeNames,
	});

	consumer.on('message_received', function (message) {
		// internals.server.log(['sqs', 'sqs-' + options.name, 'message_received'], { settings: { name: options.name, queueUrl: options.queueUrl }, message: message });
	});

	consumer.on('message_processed', function (message) {
		// internals.server.log(['sqs', 'sqs-' + options.name, 'message_processed'], { settings: { name: options.name, queueUrl: options.queueUrl }, message: message });
	});

	consumer.on('processing_error', function (error) {
		// internals.server.log(['sqs', 'sqs-' + options.name, 'error', 'processing_error'], {	settings: {	name: options.name,	queueUrl: options.queueUrl	}, msg: error.message, error: error }); // tracking both { msg: error.message } and { error: error }. If error !== VError, error will be an empty object
	});

	consumer.on('error', function (error) {
		// internals.server.log(['sqs', 'sqs-' + options.name, 'error'], { settings: { name: options.name, queueUrl: options.queueUrl }, msg: error.message, error: error });
	});

	return consumer;
};

exports.register.attributes = {
	name: 'sqs',
};
