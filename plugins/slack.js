'use strict';

const Joi = require('joi');
const Slack = require('node-slack');
const VError = require('verror');

const internals = {};

internals.schema = Joi.object({
	webhook_url: Joi.string().required(),
});

exports.register = function (server, options, next) {
	Joi.assert(options, internals.schema, 'Invalid options');

	const SlackSlack = new Slack(options.webhook_url, {});

	server.expose('send', (options) => SlackSlack.send(options, (error) => {
		if (error) {
			// server.log(['error', 'slack'], new VError({ cause: error, info: { options: options } }, 'error sending slack message'));
		}
	}));

	return next();
};

exports.register.attributes = {
	name: 'slack',
};
