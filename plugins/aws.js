'use strict';

const AWS = require('aws-sdk');
const Joi = require('joi');
const Hoek = require('hoek');

const internals = {};

internals.schema = Joi.object({
	key: Joi.string().required(),
	secret: Joi.string().required(),
	region: Joi.string().required(),
});

exports.register = function (server, options, next) {
	const settings = Hoek.applyToDefaults(internals.defaults || {}, options || {});
	Joi.assert(settings, internals.schema, 'Invalid options');

	AWS.config.update({
		accessKeyId: settings.key,
		secretAccessKey: settings.secret,
		region: settings.region,
	});

	const SQS = new AWS.SQS();
	const SNS = new AWS.SNS();
	const SES = new AWS.SES();

	server.expose('sqs', SQS);
	server.expose('sns', SNS);
	server.expose('ses', SES);

	return next();
};

exports.register.attributes = {
	name: 'aws',
};
