/* eslint-disable indent */
'use strict';

const Boom = require('boom');
const Hoek = require('hoek');
const Joi = require('joi');
const Promise = require('bluebird');
const sql = require('mssql');
const moment = require('moment');
const _ = require('lodash');

const internals = {
	defaults: {
		requestTimeout: 120000,
	},
};

internals.schema = Joi.object({
	user: Joi.string().required(),
	password: Joi.string().required(),
	server: Joi.string().required(),
	database: Joi.string().required(),
	requestTimeout: Joi.number(),
});

exports.register = function (server, options, next) {
	const settings = Hoek.applyToDefaults(internals.defaults, options);
	const results = Joi.validate(settings, internals.schema);
	Hoek.assert(!results.error, results.error);


	server.expose('addStatusUpdateLog', internals.addStatusUpdateLog);
	server.expose('addStatusUpdateLogDistinct', internals.addStatusUpdateLogDistinct);

	const connection = new sql.Connection(settings, function (error) {
		if (error) {
			return next(error);
		}

		internals.mssqlQuery = function (query) {
			const request = new sql.Request(connection); // or: var request = connection.request();
			return new Promise(function (resolve, reject) {
				request.query(query, function (error, recordset) {
					if (error) {
						return reject(error);
					}
					return resolve(recordset);
				});
			});
		};

		return next();
	});
};

internals.addStatusUpdateLog = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({
			accountNo: Joi.string().required(), // ACCOUNTNO of goldmine record.
			oldStatus: Joi.string().required(),
			newStatus: Joi.string().required(),
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let currentDate = moment();

		let query = `INSERT INTO "UpdateGM"."dbo"."LOG" (AccountNo,Action,DataBefore,DataAfter,Lender,UserName,CurrentOwner,WhatTimeIsNow) ` +
			`VALUES ('${options.accountNo}','UpdateStatus','${options.oldStatus}','${options.newStatus}','','MASTER','','${currentDate.format('YYYY-MM-DD HH:mm')}')`;

		internals.mssqlQuery(query)
			.then((response) => resolve(response))
			.catch((error) => reject(error));

		// return resolve();
	});
}

internals.addStatusUpdateLogDistinct = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({
			accountNo: Joi.string().required(), // ACCOUNTNO of goldmine record.
			oldStatus: Joi.string().required(),
			newStatus: Joi.string().required(),
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let querySelect = `SELECT * FROM "UpdateGM"."dbo"."LOGDISTINCT" WHERE AccountNo='${options.accountNo}'`;

		internals.mssqlQuery(querySelect)
			.then((responseSelect) => {
				let currentDate = moment();
				let query;
				if (responseSelect && responseSelect.length > 0) {
					query = `UPDATE "UpdateGM"."dbo"."LOGDISTINCT" SET Action='UpdateStatus',DataBefore='${options.oldStatus}',DataAfter='${options.newStatus}',UserName='MASTER',WhatTimeIsNow='${currentDate.format('YYYY-MM-DD HH:mm')}' ` +
						`WHERE AccountNo='${options.accountNo}'`;
				} else {
					query = `INSERT INTO "UpdateGM"."dbo"."LOGDISTINCT" (AccountNo,Action,DataBefore,DataAfter,Lender,UserName,CurrentOwner,WhatTimeIsNow) ` +
						`VALUES ('${options.accountNo}','UpdateStatus','${options.oldStatus}','${options.newStatus}','','MASTER','','${currentDate.format('YYYY-MM-DD HH:mm')}')`;
				}

				internals.mssqlQuery(query)
					.then((response) => {
						resolve(response);
					})
					.catch((error) => {
						resolve(); // reject(error);
					});
			})
			.catch((errorSelect) => resolve());
		// .catch((errorSelect) => reject(errorSelect));

		// return resolve();
	});
}

exports.register.attributes = {
	name: 'updateGM',
};
