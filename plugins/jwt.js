'use strict';

const Boom = require('boom');
const jwt = require('jsonwebtoken');

const internals = {};

exports.register = function (server, options, next) {
	internals.settings = options;

	server.ext('onPreHandler', internals.onPreHandler);

	return next();
};

internals.onPreHandler = function (request, reply) {
	if (request.payload && request.payload.token) {
		return jwt.verify(request.payload.token, internals.settings.secret, function (error, decoded) {
			if (error) {
				return reply(Boom.unauthorized('Authentication failed. Failed to authenticate token.', error));
			}

			/*eslint-disable no-param-reassign*/
			request.app.jwt = decoded;
			return reply.continue();
		});
	}

	return reply.continue();
};

exports.register.attributes = {
	name: 'jwt',
};
