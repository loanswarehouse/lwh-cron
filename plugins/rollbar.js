'use strict';

const Joi = require('joi');
const Hoek = require('hoek');
const Rollbar = require('rollbar');
const _ = require('lodash');
const VError = require('verror');

const internals = {};

internals.schema = Joi.object({
	token: Joi.string().required(),
	environment: Joi.string().required(),
	codeVersion: Joi.string().required(),
});

exports.register = function (server, options, next) {
	Joi.assert(options, internals.schema, 'Invalid options');

	Rollbar.init(options.token, {
		environment: options.environment,
		codeVersion: options.codeVersion,
	});

	server.on('log', (event, tags) => {
		let verror = false;
		
		if (tags.error && event.data && (event.data.jse_shortmsg || event.data.jse_info)) { // === VError
			verror = true;
			Rollbar.handleErrorWithPayloadData(event.data, {
				level: 'error',
				fingerprint: event.data.message,
				error: event.data,
				stack: VError.fullStack(event.data),
			});
		}

		if (!verror && tags.error && event.data.error && event.data.error.jse_shortmsg && event.data.error.jse_info) { // === VError
			Rollbar.handleErrorWithPayloadData(event.data.error, {
				level: 'error',
				fingerprint: event.data.error.message,
				error: event.data.error,
				stack: VError.fullStack(event.data.error),
			});
		}
	});

	server.on('request-error', function (request, error) {
		const username = (_.has(request, 'app.jwt.username')) ? request.app.jwt.username : undefined;

		Rollbar.handleErrorWithPayloadData(error, {
			level: 'error',
			msg: error.message,
			req_id: request.id,
			stack: error.stack,
			error: error,
		}, Hoek.merge(request, {
			url: request.url.path, // overwrite url for Rollbar, because request.url is an object causing Rollbar to show url as [object Object]
			user_id: username, // unique user_id, in this case a username
		}));
	});

	return next();
};

exports.register.attributes = {
	name: 'rollbar',
};
