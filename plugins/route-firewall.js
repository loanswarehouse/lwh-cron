'use strict';

const Boom = require('boom');

const internals = {};

exports.register = function (server, options, next) {
	internals.settings = options;

	server.ext('onPreHandler', internals.onPreHandler);

	return next();
};

internals.onPreHandler = function (request, reply) {
	if (request.info.remoteAddress.indexOf('192.168.1') > -1 || request.info.remoteAddress.indexOf('5.148.48.178') > -1 || request.info.remoteAddress.indexOf('127.0.0.1') > -1 || request.url.pathname === '/goldmine-status-changes/today' || request.url.pathname === '/reports/sales-league') {
		return reply.continue();
	}
	return reply(Boom.forbidden());
};

exports.register.attributes = {
	name: 'route-firewall',
};
