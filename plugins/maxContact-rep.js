'use strict';
const pg = require('pg');
const Joi = require('joi');
const Hoek = require('hoek');
const Client= pg.Client;

const internals = {};

internals.schema = Joi.object({
	user: Joi.string().required(),
	password: Joi.string().required(),
	host: Joi.string().required(),
	database: Joi.string().required(),
    port: Joi.number().required(),
});

exports.register = function (server, options, next) {
    const results = Joi.validate(options, internals.schema);
    Hoek.assert(!results.error, results.error);
    const client = new Client(options);
    server.expose('fetchInboundCallsInEnqStatus', internals.fetchInboundCallsInEnqStatus);
    server.expose('fetchListData', internals.fetchListData);
    client.connect(function(err) {
        if (err) {
            next(err);
        }
        console.log('connected to the database:');
        internals.pgQuery = function(query) {
            return new Promise((resolve, reject) => {
                client.query(query, function (err, res) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
                    // .then((response) => resolve(response))
                    // .catch((error) => reject(error))
                    // .then(() => {
                    //     console.log('finally');
                    //     // client.end();
                    // })
            })
        }
        return next();
    })
}

internals.fetchInboundCallsInEnqStatus = function (phoneNumbers) {
    return new Promise((resolve, reject) => {
        const query = `WITH recent_calls AS (SELECT phone_num, MAX(history_id) AS most_recent_call_made FROM history WHERE phone_num IN (${phoneNumbers}) GROUP BY phone_num)
SELECT rc.phone_num, result_code, reference_id FROM history h JOIN recent_calls rc ON h.phone_num = rc.phone_num AND h.history_id = rc.most_recent_call_made`;
        internals.pgQuery(query)
        .then((res) => {
            if (!res) {
                return reject(new Error('Unexpected Response!!'));
            }
            resolve(res.rows);
        })
        .catch((e) => reject(e));
    })
}

internals.fetchListData = function (listIds) {
    return new Promise((resolve, reject) => {
        const query = `SELECT is_fetched, list_id, last_result_code, total_attempts, reference_id FROM lead WHERE (last_result_code != 'API-Remove' OR last_result_code IS NULL) AND list_id IN (${listIds}) 
AND import_date >= '2024-07-01'`;
        internals.pgQuery(query)
            .then((res) => {
                if (!res) {
                    return reject(new Error('Error'));
                }
                resolve(res.rows);
            })
            .catch((e) => {
                reject(e);
            })
    })
}
exports.register.attributes = {
    name: 'maxContactRep'
}