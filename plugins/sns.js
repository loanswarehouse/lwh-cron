'use strict';

const Joi = require('joi');
const Assert = require('assert');
const VError = require('verror');

const internals = {};

internals.optionsSchema = Joi.object().keys({
	region: Joi.string().required(),
	accountNo: Joi.number().integer().min(1).required(),
}).required();

internals.schemas = {
	cmdSendEmail: Joi.object().keys({
		goldmineId: Joi.number().integer().min(1).max(999999), // we'll retrieve latest email from goldmine
		From: Joi.string().email().required(),
		TemplateModel: Joi.object().required(),
		TemplateId: Joi.number().integer().min(1).required(),
	}),
	cmdSendText: Joi.object().keys({
		goldmineId: Joi.number().integer().min(1).max(999999).required(),
		templateId: Joi.number().integer().min(1).required(),
		actionId: Joi.number().integer().min(1).optional(),
	}),
	leadReceived: Joi.number().integer().min(1).description('goldmine id'), // message just a string (if manual, just goldmine id)
	cmdCreateDxiRecord: Joi.number().integer().min(1).required(), // lead id
};

exports.register = function (server, options, next) {
	Joi.assert(options, internals.optionsSchema, 'Invalid options');

	internals.server = server;
	internals.options = options;

	server.dependency('aws', function (server, next) {
		server.expose('publish', internals.publish);
		server.expose('send', internals.send);
		return next();
	});

	return next();
};

internals.send = function (command, message, subject) {
	return new Promise((resolve, reject) => {
		Assert.ok(internals.schemas[command], 'unknown command (schema not found)');

		Joi.validate(message, internals.schemas[command], (error, result) => {
			if (error) {
				return reject(new VError({ info: { data: error.details[0].message } }, 'does not fit schema'));
			}

			const options = {
				Message: (typeof message === 'object' ? JSON.stringify(message) : message.toString()), // if object, stringify, otherwise toString() (e.g. number)
				Subject: subject || undefined,
				TopicArn: `arn:aws:sns:${internals.options.region}:${internals.options.accountNo}:${command}`,
			};

				return internals.server.plugins.aws.sns.publish(options, (error, data) => {
					if (error) {
						return reject(new VError({ info: { data: options } }, error.message));
					}
					return resolve(data);
				});

		});
	});
};

internals.publish = function (message, subject, endpoint) {
	return new Promise((resolve, reject) => {
		const params = {
			Message: (typeof message === 'object' ? JSON.stringify(message) : message.toString()), // if object, stringify, otherwise toString() (e.g. number)
			Subject: subject || undefined,
			TopicArn: endpoint,
		};
		internals.server.plugins.aws.sns.publish(params, (error, data) => {
			if (error) {
				if (!(error instanceof Error)) {
					error = new VError({
						info: {
							error: error,
							options: { message: message, subject: subject, endpoint: endpoint }
						}
					}, 'error plugins.sns.publish()');
				}
				return reject(error);
			}
			return resolve(data);
		});
	});
};

exports.register.attributes = {
	name: 'sns',
};
