'use strict';

const Joi = require('joi');
const Datadog = require('dogapi');
const VError = require('verror');
const _ = require('lodash');
const Oppsy = require('oppsy');
const Moment = require('moment');

const internals = {};

internals.schema = Joi.object({
	name: Joi.string().required(),
	environment: Joi.string().required(),
	apiKey: Joi.string().required(),
	appKey: Joi.string().required(),
});

exports.register = function (server, options, next) {
	Joi.assert(options, internals.schema, 'Invalid options');

	Datadog.initialize({
		api_key: options.apiKey,
		app_key: options.appKey,
	});

	const oppsy = new Oppsy(server);
	oppsy.start(10000);

	server.on('response', function (request) {
		if (request.response && process.env.NODE_ENV === 'production') {
			if (request.route.path === '/health') { return; } // dont log health checks

			const tags = [];

			tags.push(`service:${options.name}`);
			tags.push(`response-code:${request.response.statusCode}`);
			tags.push(`response-code-nxx:${request.response.statusCode.toString().split(0, 1)}xx`);
			tags.push(`request-method:${request.route.method}`);
			tags.push(`request-path:${request.route.path}`);
			tags.push(`request-method-path:${request.route.method.toUpperCase()} ${request.route.path}`);

			// Tenant
			if (_.has(request, 'response.request.auth.credentials.tenant')) {
				tags.push(`tenant:${request.response.request.auth.credentials.tenant}`);
			}

			// Boom responses
			if (_.has(request, 'response.request.response.source.errors')) {
				const error = request.response.request.response.source.errors[0].title.replace(/ /g, '');
				tags.push(`boom-error:${error}`);
			}

			// Environment
			tags.push(`env:${options.environment}`);

			const metrics = [{
				metric: 'http.response.count',
				points: 1,
				type: 'count',
				tags: tags,
			}, {
				metric: 'http.response.time',
				points: (request.info.responded - request.info.received),
				type: 'gauge',
				tags: tags,
			}];

			Datadog.metric.send_all(metrics, (error, response) => {
				if (error) {
					request.server.log('error', new VError({ cause: error }, 'error sending request metrics to datadog'));
				}
			});
		}
	});

	oppsy.on('ops', (data) => {
		const metrics = [];
		const tags = [];

		tags.push(`service:${options.name}`);
		tags.push(`host:${data.host}`);
		tags.push(`pid:${process.pid}`);
		tags.push(`env:${options.environment}`);

		metrics.push({ metric: 'hapi.ops.requests_total', points: (data.requests[process.env.PORT] ? data.requests[process.env.PORT].total : 0), type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.requests_disconnects', points: (data.requests[process.env.PORT] ? data.requests[process.env.PORT].disconnects : 0), type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.response_times_avg', points: (data.responseTimes[process.env.PORT] ? data.responseTimes[process.env.PORT].avg : 0), type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.response_times_max', points: (data.responseTimes[process.env.PORT] ? data.responseTimes[process.env.PORT].max : 0), type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.sockets_http', points: data.sockets.http.total, type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.sockets_https', points: data.sockets.https.total, type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.os_load_1m_avg', points: data.osload[0], type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.os_load_5m_avg', points: data.osload[1], type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.os_load_15m_avg', points: data.osload[2], type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.os_mem_total', points: data.osmem.total, type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.os_mem_free', points: data.osmem.free, type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.os_uptime', points: data.osmem.free, type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.process_uptime', points: data.psup, type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.process_memory_rss', points: data.psmem.rss, type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.process_memory_heap_total', points: data.psmem.heapTotal, type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.process_memory_heap_used', points: data.psmem.heapUsed, type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.concurrents', points: data.concurrents[process.env.PORT], type: 'gauge', tags: tags });
		metrics.push({ metric: 'hapi.ops.event_loop_delay', points: data.psdelay, type: 'gauge', tags: tags });

		server.plugins.sequelize.db.Lead.findAll({ limit: 1, order: [['id', 'desc']] })
			.then((lead) => metrics.push({ metric: 'lwh.last_lead', points: Moment().diff(lead[0].dataValues.createdAt, 'minutes'), type: 'gauge', tags: tags })) // push minute since last lead received
			.then(() => server.plugins.sequelize.db.Lead.findAll({ limit: 1, order: [['id', 'desc']], where: { referral: 'MONEY.CO.UK' } }))
			.then((moneycoukLead) => metrics.push({ metric: 'lwh.last_moneycouk_lead', points: Moment().diff(moneycoukLead[0].dataValues.createdAt, 'minutes'), type: 'gauge', tags: tags })) // push minute since last moneycouk lead received
			.then(() => Datadog.metric.send_all(metrics, (error, response) => {
				if (error) {
					server.log('error', new VError({ cause: error }, 'error sending ops metrics to datadog'));
				}
			}))
			.catch((error) => server.log('error', new VError({ cause: error }, 'error getting lwh last leads')));
	});

	return next();
};

exports.register.attributes = {
	name: 'datadog',
};
