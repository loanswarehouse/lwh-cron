'use strict';

const Boom = require('boom');
const Request = require('request');
const Joi = require('joi');
const Hoek = require('hoek');
const Promise = require('bluebird');
const Moment = require('moment');
const VError = require('verror');

const internals = {
	defaults: {
		retryMax: 2,
	},
};

internals.schema = Joi.object().keys({
	url: Joi.string().required(),
	user: Joi.string().required(),
	password: Joi.string().required(),
	retryMax: Joi.string().required(),
	table: Joi.string().required(),
}).required();

exports.register = function (server, options, next) {
	internals.settings = Hoek.applyToDefaults(internals.defaults || {}, options || {});
	Joi.assert(internals.settings, internals.schema, 'Invalid options');

	internals.server = server;

	server.expose('getNumberOfCalls', internals.getNumberOfCalls);
	server.expose('findRecordById', internals.findRecordById);
	server.expose('findRecords', internals.findRecords);
	server.expose('manipulateDataset', internals.manipulateDataset);
	server.expose('moveRecordToDataset', internals.moveRecordToDataset);
	server.expose('updateCallback', internals.updateCallback);
	server.expose('updateOutcome', internals.updateOutcome);
	server.expose('createRecord', internals.createRecord);
	server.expose('getAgent', internals.getAgent);
	server.expose('getAllAgents', internals.getAllAgents);

	internals.obtainToken()
		.then(() => next())
		.catch((error) => next(error));
};

internals.obtainToken = function () {
	return new Promise((resolve, reject) => {
		Request({
			type: 'GET',
			url: internals.settings.url + '/token.php',
			qs: {
				action: 'get',
				username: internals.settings.user,
				password: internals.settings.password,
			},
		}, (error, response, body) => {
			if (error) {
				return reject(error);
			}

			let dxiResponse = body;

			try {
				dxiResponse = JSON.parse(dxiResponse);
			} catch (error) {
				return reject(Boom.badImplementation('failed to JSON.parse()', dxiResponse));
			}

			if (dxiResponse.error) {
				return reject(dxiResponse.error);
			}

			if (dxiResponse.success === true && dxiResponse.token) {
				internals.token = dxiResponse.token;
				return resolve(dxiResponse.token);
			}

			return reject(dxiResponse);
		});
	});
};

internals.getNumberOfCalls = function (options) {
// options = cli, ddi, tstart, tstop
	return new Promise((resolve, reject) => {
		internals.query('/database.php', {
			method: 'cdr_log',
			action: 'read',
			cli: options.cli || undefined,
			ddi: options.ddi || undefined,
			tstart: options.tstart ? Moment(options.tstart).format('YYYY-MM-DD HH:mm:ss') : '2010-01-01 00:00:00',
			tstop: options.tstop ? Moment(options.tstop).format('YYYY-MM-DD HH:mm:ss') : undefined,
		})
			.then((data) => {
				if (data.list.length === 0) {
					resolve('0');
				} else {
					resolve(data.list.length);
				}
			})
			.catch((error) => reject(error));
	});
};

internals.findRecordById = function (id) {
	return new Promise((resolve, reject) => {
		if (isNaN(parseInt(id)) === true) {
			return reject(new VError({ name: 'Dxi.findRecordById()', info: { data: id } }, 'invalid id argument'));
		}

		const params = {
			method: 'ecnow_records',
			action: 'read',
			urn: id,
		};

		return internals.query('/ecnow.php', params)
			.then((response) => {
				if (response.total === 0) {
					return resolve(undefined);
				}
				return resolve(response.list[0]);
			})
			.catch((error) => reject(error));
	});
};

internals.findRecords = function (options) {
	return new Promise((resolve, reject) => {
		const defaultParams = {
			method: 'ecnow_records',
			action: 'read',
		};

		const params = Hoek.applyToDefaults(defaultParams, options);

		internals.query('/ecnow.php', params)
			.then((response) => (response.success === true ? resolve(response.list) : reject(response)))
			.catch((error) => reject(new VError({
				cause: error,
				info: { options: options }
			}, 'Dxi.findRecords() failed')));
	});
};

internals.moveRecordToDataset = function (recordId, datasetId) {
	return new Promise((resolve, reject) => {
		const params = {
			method: 'ecnow_records',
			action: 'update',
		};

		const json = {
			easycall: {
				dataset: datasetId,
				id: recordId,
				notes_append: `cronJob moved record to dataset ${datasetId}`,
			},
		};

		internals.query('/ecnow.php', params, json)
			.then((response) => resolve(response))
			.catch((error) => reject(error));
	});
};

internals.updateCallback = function (recordId, callback) {
	return new Promise((resolve, reject) => {
		// find records' dataset first
		return internals.findRecordById(recordId)
			.then((record) => {
				if (!record) {
					return reject(Boom.notFound());
				}

				const datasetId = record.datasetid;

				// once datasetId is found, update the record
				const params = {
					method: 'ecnow_records',
					action: 'update',
				};

				const json = {
					easycall: {
						id: recordId,
						dataset: datasetId,
						callback: callback,
						notes_append: `hapi-api updated callback to ${callback}`,
					},
				};

				return internals.query('/ecnow.php', params, json)
					.then((response) => resolve(response))
					.catch((error) => reject(error));
			})
			.catch((error) => reject(error));
	});
};

internals.updateOutcome = function (recordId, outcome, newDatasetId) {
	return new Promise((resolve, reject) => {
		return internals.findRecordById(recordId)
			.then((record) => {
				if (!record) {
					return reject(Boom.notFound());
				}

				const datasetId = newDatasetId || record.datasetid;
				const callback = record.callback;

				// once datasetId is found, update the record
				const params = {
					method: 'ecnow_records',
					action: 'update',
				};

				const json = {
					easycall: {
						id: recordId,
						dataset: datasetId,
						outcomecode: outcome,
						callback: callback, // needs this to avoid a bug, where a CLOSED -> OPEN record will become rec_new
						notes_append: `hapi-api updated outcome from ${record.outcomecode} to ${outcome}`,
					},
				};

				return internals.query('/ecnow.php', params, json)
					.then((response) => resolve(response))
					.catch((error) => reject(error));
			})
			.catch((error) => reject(error));
	});
};

internals.manipulateDataset = function (options) {
	// options = queue, dataset, state (HOLD|LIVE|EXPIRED), priority
	return new Promise((resolve, reject) => {
		const params = {
			method: 'ecnow_datasets',
			action: 'update',
		};

		const json = {
			easycall: {
				qid: options.queueId || undefined,
				dataset: options.dataset,
				d_status: options.state || undefined,
				d_priority: options.priority || undefined,
			},
		};

		internals.query('/ecnow.php', params, json)
			.then((response) => resolve(response))
			.catch((error) => reject(error));
	});
};

internals.createRecord = function (data) {
	return new Promise((resolve, reject) => {
		const params = {
			method: 'ecnow_records',
			action: 'create',
		};

		const json = {
			easycall: {
				dataset: data.dataset,
				goldmine: data.goldmine,
				FirstName: data.firstName,
				LastName: data.lastName,
				HomePhone: data.homePhone,
				WorkPhone: data.workPhone,
				MobilePhone: data.mobilePhone,
				urn: '',
			},
		};

		internals.query('/ecnow.php', params, json)
			.then((response) => resolve(response))
			.catch((error) => reject(error));
	});
};

internals.getAgent = function (data) {
	return new Promise((resolve, reject) => {
		const params = {
			method: 'agents',
			action: 'read',
			agent: data.agentId,
		};

		internals.query('/ajax.php', params)
			.then((response) => resolve(response))
			.catch((error) => reject(error));
	});
};

internals.getAllAgents = function (isWorkingHours) {
	return new Promise((resolve, reject) => {
		if (!isWorkingHours) {
			resolve(null);
		}
		const params = {
			method: 'agents',
			action: 'read',
			mode: 'all',
		};

		internals.query('/ajax.php', params)
			.then((response) => resolve(response))
			.catch((error) => reject(error));
	});
};

internals.query = function (url, queryParameters, json) {
	let retried = 0;
	const retryMax = internals.settings.retryMax;
	const query = queryParameters;

	query.format = 'json';
	query.raw = '1';
	query.table = internals.settings.table;

	return retry(function () {
		return new Promise((resolve, reject) => {
			_useExistingOrGetNewToken()
				.then(_query)
				.then((body) => {
					resolve(body);
					internals.server.log(['dxi', 'debug'], {
						query: {
							url: url,
							query: query,
							json: json,
							response: body,
						},
					});
				})
				.catch((error) => {
					let errorLevel;

					query.token = '*'; // avoid leaking token to logging / error handling

					if (VError.info(error) && VError.info(error).response && VError.info(error).response.error === 'Expired token') {
						errorLevel = 'debug';
					} else {
						errorLevel = 'error';
					}

					internals.server.log(['dxi', errorLevel], {
						error: error,
						query: { url: url, query: query, json: json },
					});

					return reject(new VError({
						name: 'DxiQueryError',
						cause: error,
					}, 'reached maximum retry attempts with dxi.query()'));
				});
		});
	});

	function _useExistingOrGetNewToken() {
		return new Promise((resolve, reject) => {
			if (internals.token) {
				query.token = internals.token;
				return resolve();
			}

			return internals.obtainToken().then((token) => {
				query.token = internals.token;
				return resolve();
			});
		});
	}

	function _query() {
		return new Promise((resolve, reject) => {
			Request({
				method: 'POST',
				url: internals.settings.url + url,
				qs: query,
				body: JSON.stringify(json),
			}, (error, response, body) => {
				if (error) {
					return reject(error);
				}

				let dxiResponse = body;

				try {
					dxiResponse = JSON.parse(body);
				} catch (error) {
					return reject(Boom.badImplementation('failed to JSON.parse()', dxiResponse));
				}

				if (dxiResponse.error) {
					query.token = '*'; // avoid leaking token to logging / error handling
					return reject(new VError({
						name: 'DxiQueryError',
						info: {
							response: dxiResponse,
							body: json,
							query: query,
						},
					}, 'error single dxi.query()'));
				}

				if (dxiResponse.success === true) {
					return resolve(dxiResponse);
				}

				return reject(dxiResponse);
			});
		});
	}

	function retry(f) {
		return f().then(
			undefined,
			function (error) {
				if (retried >= retryMax) {
					internals.server.log(['dxi', 'fatal'], {
						msg: 'Reached maximum retry attempts',
						error: error,
						query: { url: url, query: query, json: json },
					});
					throw error;
				} else {
					retried++;
					const errorResponse = VError.info(error).response.error; // because single dxi.query() throws VError
					if (errorResponse === 'No token argument' || errorResponse === 'Unknown token' || errorResponse === 'Expired token' || errorResponse === 'Invalid token') {
						return internals.obtainToken().then(function () {
							return retry(f);
						});
					}
					return retry(f);
				}
			}
		);
	}
};

exports.register.attributes = {
	name: 'dxi',
};
