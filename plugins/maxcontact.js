'use strict';

const Boom = require('boom');
const Request = require('request');
const Joi = require('joi');
const Hoek = require('hoek');
const Promise = require('bluebird');
const Moment = require('moment');
const VError = require('verror');

const log4js = require('log4js');
log4js.configure({
	appenders: { maxContact: { type: 'file', filename: 'logger.log' } },
	categories: { default: { appenders: ['maxContact'], level: 'debug' } }
});

const logger = log4js.getLogger('maxContact');

const internals = {};

internals.schema = Joi.object().keys({
	url: Joi.string().required(),
	user: Joi.string().required(),
	password: Joi.string().required(),
}).required();

exports.register = function (server, options, next) {
    Joi.assert(options, internals.schema, 'Invalid options');
    internals.settings = options;
    internals.server = server;

    server.expose('addLead', internals.addLead);
    server.expose('moveLeadByLwhRefId', internals.moveLeadByLwhRefId);
    server.expose('deactivateLeadByLwhRefId', internals.deactivateLeadByLwhRefId);
    server.expose('reactivateLeadByLwhRefId', internals.reactivateLeadByLwhRefId);
    internals.obtainLoginData()
        .then(() => next())
        .catch((error) => {
            // logger.error(error.message);
            next()})
}

internals.obtainLoginData = function() {
    return new Promise((resolve, reject) => {
        const passwordBuffer = new Buffer(internals.settings.password, 'ascii')//Buffer.from(internals.settings.password, 'ascii');
        const base64 = passwordBuffer.toString('base64');
        Request({
            url: internals.settings.url + '/apitoken/login/' + internals.settings.user,
            headers: {
                'Authorization': 'Basic ' + base64,
                'Accept': 'application/json',
            }
        }, function (error, response, body) {
            let maxContactResponse = body;
            if (error) {
                return reject(error);
            }

            try {
                maxContactResponse = JSON.parse(body);
            } catch (error) {
                return reject(Boom.badImplementation('failed to JSON.parse() in internals.obtainLoginData() ', maxContactResponse))
            }

            if (maxContactResponse && maxContactResponse.TokenKey && maxContactResponse.ExpiryDate) {
                internals.loginData = {
                    tokenKey: maxContactResponse.TokenKey,
                    expiryDate: maxContactResponse.ExpiryDate
                };
                console.log('internals login: ', internals.loginData);
                return resolve();
            }
            return reject(maxContactResponse);
        })
    })
}

internals.addLead = function(data) {
    return new Promise((resolve, reject) => {
        internals.query('AddLead', data, 'json', 'POST')
        .then(() => resolve())
        .catch((error) => {
            // logger.error('failed to /AddLead: ', JSON.stringify(error), ' - data: ', JSON.stringify(data));
            reject(new Error('Failed internals.addLead()'));
        });
    })
}

internals.moveLeadByLwhRefId = function (data) {
    const options = {
        NewListID: data.newListId,
        OldListID: 0, // Global search in MaxContact
        UpdateFinal: true,
        FinalCode: data.outcome // 0=active, 1=inactive
    }
    return new Promise((resolve, reject) => {
        internals.query('MoveLeadByRefId/' + data.referenceId, options, 'querystring', 'POST')
        .then(() => resolve())
        .catch((error) => {
            logger.error('failed to /MoveLeadByRefId: ', JSON.stringify(error), ' - data: ', JSON.stringify(data));
            reject(error);
        });
    })
}

internals.deactivateLeadByLwhRefId = function (data) {
    const options = {
        ResultCode: 'API-Remove',
        UpdateHistory: true,
    }
    return new Promise((resolve, reject) => {
        internals.query('DeactivateLeadByRefId/' + data.referenceId, options, 'querystring', 'DELETE')
        .then(() => resolve())
        .catch((error) => {
            // logger.error('failed to /DeactivateLeadByRefId: ', JSON.stringify(error), ' - data: ', JSON.stringify(data));
            reject(new Error('Failed internals.deactivateLeadByLwhRefId()'));
        });
    })
}

internals.reactivateLeadByLwhRefId = function (data) {
    const options = {
        ReactivateDNC:true  
    }
    return new Promise((resolve, reject) => {
        internals.query('ReactivateLeadByRefId/' + data.referenceId, options, 'querystring', 'POST')
        .then(() => resolve())
        .catch((error) => {
            // logger.error('failed to /ReactivateLeadByRefId: ', JSON.stringify(error), ' - data: ', JSON.stringify(data));
            reject(new Error('Failed reactivateLeadByLwhRefId()'));
        });
    })
}


internals.query = function(action, data, paramsType, method) {
    return new Promise((resolve, reject) => {
        _useExistingOrGetNewToken()
            .then(_postToMaxContact)
            .then(() => resolve())
            .catch((error) => reject(error));
        })

    function _useExistingOrGetNewToken() {
        return new Promise((resolve, reject) => {
            console.log('Login data in _useExistingOrGetNewToken()', internals.loginData);
            if (Moment(internals.loginData.expiryDate).isValid() && Moment() > Moment(internals.loginData.expiryDate)) {
                internals.obtainLoginData()
                .then(() => {
                    return resolve();
                })
                .catch((error) =>   reject(error));
            } else {
                resolve();
            }
        })
    }

    function _postToMaxContact() {
        return new Promise((resolve, reject) => {
            const requestOptions = {
                method: method, 
                url: internals.settings.url + '/LeadManagement/' + action,
                headers: {
                    'Cookie': 'TokenKey=' + internals.loginData.tokenKey,
                    'Accept': 'application/json',
                },
            }
            if (paramsType === 'json') {
                requestOptions.json = data;
            }

            if (paramsType === 'querystring') {
                requestOptions.qs = data;
            }
            Request(requestOptions, function(error, response, body) {
                if (error) {
                    return reject(error);
                }
                let maxContactResponse = body;
                try {
                    if (typeof maxContactResponse === 'string') {
                        maxContactResponse = JSON.parse(maxContactResponse);
                    }
                } catch (error) {
                    return reject(Boom.badImplementation('failed to JSON.parse() in _postToMaxContact() ', maxContactResponse))
                }
                
                if (maxContactResponse) {
                    if (maxContactResponse.Errors) {
                        const severeErrorFound = maxContactResponse.Errors.find(obj => obj.ErrorSeverity === 3);
                        if (maxContactResponse.Success || !severeErrorFound) {
                            return resolve();
                        }
                    }
                }
                return reject(maxContactResponse);
            })
        })
    }
}
exports.register.attributes = {
	name: 'maxcontact',
};
