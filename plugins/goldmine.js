/* eslint-disable indent */
'use strict';

const Boom = require('boom');
const Hoek = require('hoek');
const Joi = require('joi');
const Promise = require('bluebird');
const sql = require('mssql');
const moment = require('moment');
const _ = require('lodash');

const internals = {
	defaults: {
		requestTimeout: 120000,
	},
};

internals.schema = Joi.object({
	user: Joi.string().required(),
	password: Joi.string().required(),
	server: Joi.string().required(),
	database: Joi.string().required(),
	requestTimeout: Joi.number(),
});

exports.register = function (server, options, next) {
	const settings = Hoek.applyToDefaults(internals.defaults, options);
	const results = Joi.validate(settings, internals.schema);
	Hoek.assert(!results.error, results.error);

	server.expose('getRecord', internals.getRecord);
	server.expose('getRecordBySourceRef', internals.getRecordBySourceRef);
	server.expose('listRecordsByStatus', internals.listRecordsByStatus);
	server.expose('listRecordsStatusHistory', internals.listRecordsStatusHistory);
	server.expose('getStatusChanges', internals.getStatusChanges);
	server.expose('getHistoricCallbacks', internals.getHistoricCallbacks);
	server.expose('getNewBusinessSLCs', internals.getNewBusinessSLCs);
	server.expose('pushScriptContact1', internals.pushScriptContact1);
	server.expose('pushScriptContact2', internals.pushScriptContact2);
	server.expose('appendGoldmineNotes', internals.appendGoldmineNotes);
	server.expose('pushEmailAddress', internals.pushEmailAddress);
	server.expose('getDueNOCs', internals.getDueNOCs);
	server.expose('updateRecordStatus', internals.updateRecordStatus);
	server.expose('addStatusUpdateLog', internals.addStatusUpdateLog);
	server.expose('deleteGMTask', internals.deleteGMTask);
	server.expose('createNewGMTask', internals.createNewGMTask);
	server.expose('updateRecordSource', internals.updateRecordSource);
	server.expose('getRecordStatusHistory', internals.getRecordStatusHistory);
	server.expose('getRecordSummary', internals.getRecordSummary);
	server.expose('fetchSecuredRecords', internals.fetchSecuredRecords);

	const connection = new sql.Connection(settings, function (error) {
		if (error) {
			return next(error);
		}

		internals.mssqlQuery = function (query) {
			const request = new sql.Request(connection); // or: var request = connection.request();
			return new Promise(function (resolve, reject) {
				request.query(query, function (error, recordset) {
					if (error) {
						return reject(error);
					}
					return resolve(recordset);
				});
			});
		};

		return next();
	});
};

internals.getRecordSummary = function (id) {
	const data = {};

	return new Promise(function (resolve, reject) {
		_getContact1()
			.then(_buildModel)
			.then(function () {
				resolve(data.model);
			})
			.catch(function (error) {
				if (error.isBoom && error.output.payload.error === 'Not Found') {
					return resolve(null);
				}
				return reject(error);
			});
	});

	function _getContact1() {
		return new Promise(function (resolve, reject) {
			internals.mssqlQuery('SELECT ACCOUNTNO,DEPARTMENT,CONTACT,LASTNAME,PHONE2,U_COUNTRY,U_KEY2,CREATEON,CREATEAT FROM "GoldMineR"."dbo"."CONTACT1" WHERE U_COMPANY = \'' + id + '\'')
				.then(function (result) {
					if (!result.length) {
						return reject(Boom.notFound());
					}
					data.contact1 = result[0];
					return resolve();
				})
				.catch(function (error) {
					reject(error);
				});
		});
	}

	function _buildModel() {
		return new Promise(function (resolve) {
			data.model = {
				id: id,
				accountNo: data.contact1.ACCOUNTNO,
				source: data.contact1.U_COUNTRY,
				status: data.contact1.U_KEY2,
			};

			function foo(obj) {
				const object = obj;
				return _.forEach(object, function (n, key) { // key = a; n = 1
					if (typeof n === 'function') {
						object[key] = n();
						return;
					}

					if (typeof n === 'object') {
						object[key] = foo(n);
						return;
					}

					object[key] = n;
				});
			}

			data.model = foo(data.model);
			resolve(data.model);
		});
	}
};

internals.getRecordStatusHistory = function (id) {
	const data = {};

	return new Promise(function (resolve, reject) {
		_getRecordStatusHistory()
			.then(_buildModel)
			.then(function () {
				resolve(data.model);
			})
			.catch(function (error) {
				if (error.isBoom && error.output.payload.error === 'Not Found') {
					return resolve(null);
				}
				return reject(error);
			});
	});

	function _getRecordStatusHistory() {
		return new Promise(function (resolve, reject) {
			internals.mssqlQuery('SELECT CONTACT1.NOTES AS RECORDNOTES, CONTACT1.U_KEY2, CONTHIST.* FROM CONTACT1 INNER JOIN CONTHIST ON CONTACT1.ACCOUNTNO = CONTHIST.ACCOUNTNO WHERE CONTACT1.U_COMPANY = \'' + id + '\'')
				.then(function (result) {
					if (!result || result.length === 0) {
						return reject(Boom.notFound());
					}
					data.statusHistory = result;
					return resolve();
				})
				.catch(function (error) {
					reject(error);
				});
		});
	}

	function _buildModel() {
		return new Promise(function (resolve) {
			data.model = {
				id: id,
				accountNo: data.statusHistory[0].ACCOUNTNO,
				status: data.statusHistory[0].U_KEY2,
				notes: data.statusHistory[0].RECORDNOTES,
				lastStatusChange: function () {
					return getLastStatusChange(data.statusHistory);
				},
				statusChangeHistory: function () {
					return getStatusChangeHistory(data.statusHistory, data.statusHistory[0].U_KEY2); // U_KEY2 === current status
				},
			};

			function foo(obj) {
				const object = obj;
				return _.forEach(object, function (n, key) { // key = a; n = 1
					if (typeof n === 'function') {
						object[key] = n();
						return;
					}

					if (typeof n === 'object') {
						object[key] = foo(n);
						return;
					}

					object[key] = n;
				});
			}

			function getLastStatusChange(statusChanges) {
				if (statusChanges[0]) {
					return {
						ref: statusChanges[0].REF,
						statusFrom: statusChanges[0].REF.split('Case updated from ')[1].split(' to ')[0],
						statusTo: statusChanges[0].REF.split('Case updated from ')[1].split(' to ')[1].split(' (oc:)')[0],
						createdBy: statusChanges[0].CREATEBY.trim(),
						updatedBy: statusChanges[0].LASTUSER.trim(),
						createdAt: moment(statusChanges[0].CREATEON).add(moment.duration(statusChanges[0].CREATEAT)),
						updatedAt: moment(statusChanges[0].LASTDATE).add(moment.duration(statusChanges[0].LASTTIME)),
						recid: statusChanges[0].recid,
					};
				}
				return 'n/a';
			}

			function getStatusChangeHistory(statusChanges, currentStatus) {
				const statusChangeHistory = [];
				let firstStatusFrom;

				statusChanges.map((i) => {
					if (i.REF.indexOf('Case updated from') >= 0) { // this is a status change
						firstStatusFrom = i.REF.split('Case updated from ')[1].split(' to ')[0]; // because iterates in reverse order, last iteration will be the first `statusFrom`, so we overwrite it every time, last time being the correct one
						const statusTo = i.REF.split('Case updated from ')[1].split(' to ')[1].split(' (oc:)')[0];
						statusChangeHistory.push(statusTo);
					}
				});

				if (!statusChangeHistory.length) { // no previous status changes, use current status
					statusChangeHistory.push(currentStatus);
				} else {
					statusChangeHistory.push(firstStatusFrom);
				}

				return _.reverse(statusChangeHistory);
			}

			data.model = foo(data.model);
			resolve(data.model);
		});
	}
};

internals.getRecord = function (id) {
	const data = {};

	return new Promise(function (resolve, reject) {
		_getContact1()
			.then(_getContact2)
			.then(_getEmail)
			.then(_getAlarmedTasks)
			.then(_getStatusChanges)
			.then(_buildModel)
			.then(function () {
				resolve(data.model);
			})
			.catch(function (error) {
				if (error.isBoom && error.output.payload.error === 'Not Found') {
					return resolve(null);
				}
				return reject(error);
			});
	});

	function _getContact1() {
		return new Promise(function (resolve, reject) {
			internals.mssqlQuery('SELECT * FROM "GoldMine"."dbo"."CONTACT1" WHERE U_COMPANY = \'' + id + '\'')
				.then(function (result) {
					if (!result.length) {
						return reject(Boom.notFound());
					}
					data.contact1 = result[0];
					return resolve();
				})
				.catch(function (error) {
					reject(error);
				});
		});
	}

	function _getContact2() {
		return new Promise(function (resolve, reject) {
			internals.mssqlQuery('SELECT * FROM "GoldMine"."dbo"."CONTACT2" WHERE ACCOUNTNO = \'' + data.contact1.ACCOUNTNO + '\'')
				.then(function (response) {
					data.contact2 = response[0];
					resolve();
				})
				.catch(function (error) {
					reject(error);
				});
		});
	}

	function _getEmail() {
		return new Promise(function (resolve, reject) {
			internals.mssqlQuery('SELECT ACCOUNTNO AS AccountKey, CONTSUPREF, ADDRESS1 FROM "GoldMine"."dbo"."CONTSUPP" WHERE ACCOUNTNO = \'' + data.contact1.ACCOUNTNO + '\' AND CONTSUPREF LIKE \'%@%\'')
				.then(function (response) {
					if (response.length) {
						data.email = response[response.length - 1].CONTSUPREF + ((response[response.length - 1].ADDRESS1) ? response[response.length - 1].ADDRESS1 : ''); // last if multiple ooptions. If email over 35 chars, 2nd part is in ADDRESS1
					} else {
						data.email = '';
					}
					resolve();
				})
				.catch(function (error) {
					reject(error);
				});
		});
	}

	function _getAlarmedTasks() {
		return new Promise(function (resolve, reject) {
			internals.mssqlQuery('SELECT * FROM "GoldMine"."dbo"."CAL" WHERE ACCOUNTNO = \'' + data.contact1.ACCOUNTNO + '\' AND ALARMFLAG=\'Y\'')
				.then(function (response) {
					data.alarmedTasks = response.length;
					data.alarmedTasksList = response;
					resolve();
				})
				.catch(function (error) {
					reject(error);
				});
		});
	}

	function _getStatusChanges() {
		return new Promise(function (resolve, reject) {
			internals.mssqlQuery('SELECT * FROM "GoldMine"."dbo"."CONTHIST" WHERE ACCOUNTNO = \'' + data.contact1.ACCOUNTNO + '\' AND REF LIKE \'Case updated from %\' ORDER BY CREATEON DESC, CREATEAT DESC')
				.then(function (response) {
					data.statusChanges = response;
					resolve();
				})
				.catch(function (error) {
					reject(error);
				});
		});
	}

	function _buildModel() {
		return new Promise(function (resolve) {
			data.model = {
				id: id,
				accountNo: data.contact1.ACCOUNTNO,
				app1: {
					title: (data.contact1.DEPARTMENT || '').trim(),
					firstName: function () {
						return data.contact1.CONTACT.trim().split(' ')[0].trim();
					},
					middleName: function () {
						if (data.contact1.CONTACT.split(' ').length >= 3) {
							const firstName = data.contact1.CONTACT.trim().split(' ')[0].trim();
							const lastName = data.contact1.LASTNAME.trim();
							return data.contact1.CONTACT.trim().split(firstName)[1].trim().split(lastName)[0].trim();
						}
						return undefined;
					},
					lastName: (data.contact1.LASTNAME || '').trim(),
					maidenName: (data.contact2.UA1MAIDEN || '').trim(),
					dob: function () {
						return moment(data.contact2.UA1DOB).format('YYYY-MM-DD');
					},
					gender: function () {
						return getGender(data.contact1.DEPARTMENT);
					},
					marital: data.contact2.UA1MARITAL,
					homePhone: data.contact1.PHONE1, // same as app2.homePhone
					mobilePhone: data.contact1.PHONE2,
					workPhone: data.contact1.PHONE3,
					employmentDetails: {
						employmentStatus: data.contact2.UA1JBEMPST,
						employmentStatusCode: function () {
							return getEmploymentStatusCode(data.contact2.UA1JBEMPST);
						},
						grossAnnualInc: function () {
							return parseInt(data.contact2.UA1JBINCGR);
						},
						additionalInc: function () {
							return parseInt(data.contact2.UA1JBINCAA);
						},
						occupation: data.contact2.UA1JB,
						yearsEmployed: data.contact2.UA1JBYRS,
						monthsEmployed: data.contact2.UA1JBMTHS,
					},
					plannedRetirementAge: data.contact2.UA4NAME,
				},
				//app2: { see below },
				email: (data.email || '').trim(),
				dependents: data.contact2.CHILDHOME,
				address: {
					postcode: data.contact1.ZIP,
					county: data.contact1.STATE,
					town: data.contact1.CITY,
					address1: data.contact1.ADDRESS1,
					address2: data.contact1.ADDRESS2,
					address3: data.contact1.ADDRESS3,
					buildingName: '',
					residencyStatus: data.contact2.USERDEF01,
					residencyStatusCode: function () {
						if (data.contact2.USERDEF01) {
							return getResidencyStatusCode(data.contact2.USERDEF01);
						}
						return '';
					},
					monthsAtAddress: data.contact2.UPRMTHATAD,
					yearsAtAddress: data.contact2.UPRYRSATAD,
				},
				addressPrevious: function () {
					if (data.contact2.UPRYRSATAD < 3) {
						return {
							postcode: data.contact2.UPRPRVPCDE,
							county: data.contact2.UPRPRVCNTY,
							town: data.contact2.UPRPRVCITY,
							address1: data.contact2.UPRPRVADD1,
							monthsAtAddress: 0,
							yearsAtAddress: data.contact2.UPRPRVYRS,
						};
					}
					return '';
				},
				source: data.contact1.U_COUNTRY,
				status: data.contact1.U_KEY2,
				loanPurpose: data.contact2.ULOANPURPS,
				loanPurposeCode: function () {
					if (data.contact2.ULOANPURPS) {
						return getLoanPurposeCode(data.contact2.ULOANPURPS);
					}
					return '';
				},
				loanAmount: function () {
					return parseFloat(data.contact2.UAGNETLOAN);
				},
				propertyValue: function () {
					return parseInt(data.contact2.UPRVAL);
				},
				mortgageBalance: function () {
					return parseInt(data.contact2.UMGOSBALNC);
				},
				mortgageArrears: function () {
					return parseInt(data.contact2.UMGARREAA);
				},
				monthlyPayment: function () {
					return parseInt(data.contact2.UMGMTHREPY);
				},
				ltv: function () {
					return calculateLtv(data.contact2.UAGNETLOAN, data.contact2.UMGOSBALNC, data.contact2.UPRVAL);
				},
				alarmedTasks: data.alarmedTasks,
				alarmedTasksList: data.alarmedTasksList,
				lender: data.contact1.KEY3,
				owner: data.contact1.KEY1,
				commission: data.contact2.UAGBRKRCOM,
				lenderReference: data.contact1.KEY4,
				sourceReference: data.contact1.KEY5,
				loanTerm: data.contact2.UAGTERM,
				paymentFrequency: data.contact2.UA1JBINCFQ,
				createdOn: data.contact1.CREATEON,
				updatedOn: data.contact1.LASTDATE,
				lastStatusChange: function () {
					return getLastStatusChange(data.statusChanges);
				},
				statusChangeHistory: function () {
					return getStatusChangeHistory(data.statusChanges, data.contact1.U_KEY2); // U_KEY2 === current status
				},
				mortgageType: data.contact2.UMGTYPE,
				mortgageCompany: data.contact2.UMGCOMPANY,
				propertyType: data.contact2.UPRTYPE,
				propertyConstructionType: data.contact2.UPRCONSTCT,
				propertyNumberOfBedrooms: function () {
					return parseInt(data.contact2.UPRBEDROMS);
				},
				propertyNumberOfHabitableRooms: function () {
					return parseInt(data.contact2.UA3PHONEHM);
				},
				propertyEverOwnedByCouncil: data.contact2.UPRCNOWNED,
				propertyPurchasedFromCouncil: data.contact2.UPRCNSITTN,
				numberOfChildrenUnder18LivingAtProperty: function () {
					return parseInt(data.contact2.UCHILDHOME);
				},
				propertyUse: data.contact2.URESBTL,
				dependentAdults: data.contact2.UA3NAME,
				floorsInBuilding: data.contact2.UPRFLATFLR,
				propertyPurchasedOn: moment(data.contact2.UPRPURCHDT).format('YYYY-MM-DD'),
				mortgagePlan: data.contact2.UA3PHONEMB,
				purchasePrice: data.contact2.UPRPURCHPR,
				goldmineNotes: data.contact1.NOTES,
				bestTimeToCall: data.contact2.UA3ADD3,
				callbackType: data.contact2.UA4JBINCAF,
			};

			// model.app2
			if (data.contact1.SOURCE && data.contact1.TITLE) {
				data.model.app2 = {
					title: data.contact1.SOURCE.trim(),
					firstName: function () {
						return data.contact1.TITLE.split(' ')[0].trim();
					},
					middleName: function () {
						if (data.contact1.TITLE.trim().split(' ').length >= 3) {
							const firstName = data.contact1.TITLE.trim().split(' ')[0].trim();
							const lastName = data.contact1.TITLE.trim().split(' ')[data.contact1.TITLE.trim().split(' ').length - 1].trim();
							return data.contact1.TITLE.trim().split(firstName)[1].trim().split(lastName)[0].trim();
						}
						return undefined;
					},
					lastName: function () {
						return data.contact1.TITLE.trim().split(' ')[data.contact1.TITLE.trim().split(' ').length - 1].trim();
					},
					maidenName: (data.contact2.UA2MAIDEN || '').trim(),
					dob: function () {
						return moment(data.contact2.UA2DOB).format('YYYY-MM-DD');
					},
					gender: function () {
						return getGender(data.contact1.SOURCE);
					},
					marital: data.contact2.UA2MARITAL,
					homePhone: data.contact1.PHONE1, // same as app1.homePhone
					mobilePhone: data.contact2.UA2MOBNUM,
					workPhone: data.contact2.UA2WORKNUM,
					employmentDetails: {
						employmentStatus: data.contact2.UA2JBEMPST,
						employmentStatusCode: function () {
							return getEmploymentStatusCode(data.contact2.UA2JBEMPST);
						},
						grossAnnualInc: function () {
							return parseInt(data.contact2.UA2JBINCGR);
						},
						additionalInc: function () {
							return parseInt(data.contact2.UA2JBINCAA);
						},
						occupation: data.contact2.UA2JB,
						yearsEmployed: data.contact2.UA2JBYRS,
						monthsEmployed: data.contact2.UA2JBMTHS,
					},
					plannedRetirementAge: data.contact2.UA3MAIDEN,
				};
			}

			function foo(obj) {
				const object = obj;
				return _.forEach(object, function (n, key) { // key = a; n = 1
					if (typeof n === 'function') {
						object[key] = n();
						return;
					}

					if (typeof n === 'object') {
						object[key] = foo(n);
						return;
					}

					object[key] = n;
				});
			}

			function getGender(title) {
				if (title) {
					let gender;
					switch (title.trim()) {
						case 'Dr F':
						case 'Hon F':
						case 'Lady':
						case 'Miss':
						case 'Mrs':
						case 'Ms':
						case 'Other F':
							gender = 'F';
							break;
						case 'Dr':
						case 'Dr M':
						case 'Hon M':
						case 'Lord':
						case 'Mr':
						case 'Other M':
						case 'Professor':
						case 'Rev':
							gender = 'M';
							break;
						default:
							gender = 'UNKNOWN';
					}
					return gender;
				}
				return '';
			}

			function getEmploymentStatusCode(employmentStatus) {
				if (employmentStatus) {
					let employmentStatusCode;
					switch (employmentStatus.toLowerCase().trim()) {
						case 'employed ft':
							employmentStatusCode = 'EFT';
							break;
						case 'employed pt':
							employmentStatusCode = 'EPT';
							break;
						case 'benefits':
							employmentStatusCode = 'OTH';
							break;
						case 'disabled':
							employmentStatusCode = 'DIS';
							break;
						case 'ft carer':
							employmentStatusCode = 'OTH';
							break;
						case 'house person':
							employmentStatusCode = 'HOU';
							break;
						case 'retired':
							employmentStatusCode = 'RET';
							break;
						case 'self employed':
							employmentStatusCode = 'SEL';
							break;
						case 'student':
							employmentStatusCode = 'STU';
							break;
						case 'unemployed':
							employmentStatusCode = 'UNE';
							break;
						default:
							employmentStatusCode = 'OTH';
					}
					return employmentStatusCode;
				}
				return '';
			}

			function getResidencyStatusCode(residencyStatus) {
				if (residencyStatus) {
					let residencyStatusCode;
					switch (residencyStatus.toLowerCase().trim()) {
						case 'homeowner':
							residencyStatusCode = 'HWM';
							break;
						case 'council te':
							residencyStatusCode = 'COU';
							break;
						case 'living wit':
							residencyStatusCode = 'LWP';
							break;
						case 'tenant':
						case 'private te':
							residencyStatusCode = 'TPR';
							break;
						case 'shared own':
							residencyStatusCode = 'O';
							break;
						default:
							return 'HWM';
					}
					return residencyStatusCode;
				}
				return '';
			}

			function getLoanPurposeCode(loanPurpose) {
				if (loanPurpose) {
					let loanPurposeCode;
					switch (loanPurpose.toLowerCase().trim()) {
						case 'business':
						case 'business & consolidation':
						case 'business vehicle':
							loanPurposeCode = 'BUS';
							break;
						case 'car':
						case 'car repairs':
							loanPurposeCode = 'CAR';
							break;
						case 'caravan':
							loanPurposeCode = 'CVN';
							break;
						case 'christmas':
							loanPurposeCode = 'CHR';
							break;
						case 'surgery':
						case 'cosmetic surgery':
							loanPurposeCode = 'COS';
							break;
						case 'mobile home':
							loanPurposeCode = 'MOB';
							break;
						case 'wedding':
							loanPurposeCode = 'WED';
							break;
						case 'holiday':
							loanPurposeCode = 'HOL';
							break;
						case 'other':
							loanPurposeCode = 'OTH';
							break;
						case 'investment':
						case 'property investment':
							loanPurposeCode = 'PRO';
							break;
						case 'clear mortgage':
						case 'mortgage':
						case 'mortgage arrears':
						case 'mortgage desposit':
							loanPurposeCode = 'MOR';
							break;
						case 'school fees':
						case 'college fees':
							loanPurposeCode = 'SCH';
							break;
						case 'solar panels':
							loanPurposeCode = 'SOL';
							break;
						case 'house deposit':
							loanPurposeCode = 'DEP';
							break;
						case 'consolidation':
							loanPurposeCode = 'CON';
							break;
						case 'home improvements':
							loanPurposeCode = 'HOM';
							break;
						default:
							loanPurposeCode = 'OTH';
							break;
					}
					return loanPurposeCode;
				}
				return '';
			}

			function calculateLtv(loanAmount, mortgageBalance, propertyValue) {
				return ((loanAmount + mortgageBalance) / propertyValue * 100).toFixed(2);
			}

			function getLastStatusChange(statusChanges) {
				if (statusChanges[0]) {
					return {
						ref: statusChanges[0].REF,
						statusFrom: statusChanges[0].REF.split('Case updated from ')[1].split(' to ')[0],
						statusTo: statusChanges[0].REF.split('Case updated from ')[1].split(' to ')[1].split(' (oc:)')[0],
						createdBy: statusChanges[0].CREATEBY.trim(),
						updatedBy: statusChanges[0].LASTUSER.trim(),
						createdAt: moment(statusChanges[0].CREATEON).add(moment.duration(statusChanges[0].CREATEAT)),
						updatedAt: moment(statusChanges[0].LASTDATE).add(moment.duration(statusChanges[0].LASTTIME)),
						recid: statusChanges[0].recid,
					};
				}
				return 'n/a';
			}

			function getStatusChangeHistory(statusChanges, currentStatus) {
				const statusChangeHistory = [];
				let firstStatusFrom;

				statusChanges.map((i) => {
					if (i.REF.indexOf('Case updated from') >= 0) { // this is a status change
						firstStatusFrom = i.REF.split('Case updated from ')[1].split(' to ')[0]; // because iterates in reverse order, last iteration will be the first `statusFrom`, so we overwrite it every time, last time being the correct one
						const statusTo = i.REF.split('Case updated from ')[1].split(' to ')[1].split(' (oc:)')[0];
						statusChangeHistory.push(statusTo);
					}
				});

				if (!statusChangeHistory.length) { // no previous status changes, use current status
					statusChangeHistory.push(currentStatus);
				} else {
					statusChangeHistory.push(firstStatusFrom);
				}

				return _.reverse(statusChangeHistory);
			}

			data.model = foo(data.model);
			resolve(data.model);
		});
	}
};

internals.getRecordBySourceRef = function (sourceRef, goldmineLeadSource, abandonGoldmineLeadSource) {
	const data = {};

	return new Promise(function (resolve, reject) {
		_getContact1()
			.then(_buildModel)
			.then(function () {
				Promise.delay(2000).then(() => {
					resolve(data.model);
				});
			})
			.catch(function (error) {
				if (error.isBoom && error.output.payload.error === 'Not Found') {
					return resolve(null);
				}
				return reject(error);
			});
	});

	function _getContact1() {
		return new Promise(function (resolve, reject) {
			data.counter = 0;
			let loop = function () {
				data.counter++;
				Promise.delay(5000).then(() => {
					internals.mssqlQuery('SELECT COMPANY,CONTACT,PHONE2,U_COUNTRY, KEY3, UAGNETLOAN FROM "GoldMine"."dbo"."CONTACT1" t1 INNER JOIN "GoldMine"."dbo"."CONTACT2" t2 ON t1.ACCOUNTNO = t2.ACCOUNTNO WHERE t1.KEY5 = \'' + sourceRef + '\'' + (goldmineLeadSource && goldmineLeadSource.length > 0 ? ' AND (t1.COUNTRY = \'' + goldmineLeadSource + '\'' + (abandonGoldmineLeadSource && abandonGoldmineLeadSource.length > 0 ? ' OR t1.COUNTRY = \'' + abandonGoldmineLeadSource + '\'' : '') + ')' : ''))
						.then(function (result) {
							if (!result.length) {
								if (data.counter < 50) {
									return loop();
								}
								return reject(Boom.notFound());
							}
							data.contact1 = result[0];
							return resolve();
						})
						.catch(function (error) {
							reject(error);
						});
				});
			}
			loop();
		});
	}

	function _buildModel() {
		return new Promise(function (resolve) {
			data.model = {
				id: data.contact1.COMPANY,
				firstName: data.contact1.CONTACT.split(' ')[0].trim(),
				lastName: data.contact1.CONTACT.split(' ')[data.contact1.CONTACT.split(' ').length - 1].trim(),
				lender: data.contact1.KEY3,
				sourceOriginal: data.contact1.U_COUNTRY,
				loanAmount: data.contact1.UAGNETLOAN,
			};
			resolve(data.model);
		});
	}
};

internals.listRecordsByStatus = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({
			status: Joi.string().min(3).max(3).alphanum().optional().example('slc').description('goldmine status'),
			from: Joi.date().format('YYYY-MM-DD').optional().example('2016-06-01').description('from date, YYYY-MM-DD'),
			to: Joi.date().format('YYYY-MM-DD').optional().example('2016-06-30').description('to date, YYYY-MM-DD'),
			source: Joi.string().optional().allow('').example('Money.co.uk').description('source'),
			type: Joi.string().optional().valid(['all', 'secured', 'unsecured']).description('lead type'),
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		if (!options.status && !options.from && !options.to && !options.source) {
			return reject(Boom.badRequest('Expecting atleast one option'));
		}

		let query = 'SELECT ACCOUNTNO, COMPANY FROM "GoldMine"."dbo"."CONTACT1"';
		const whereConditions = [];

		if (options.status) {
			whereConditions.push(`U_KEY2='${options.status.replace(/'/g, '')}'`);
		}

		if (options.from && options.to) {
			whereConditions.push(`"CREATEON" >= '${moment(options.from).format('YYYY-MM-DD').replace(/'/g, '')}' AND "CREATEON" <= '${moment(options.to).format('YYYY-MM-DD').replace(/'/g, '')}'`);
		}

		if (options.source) {
			whereConditions.push(`"COUNTRY" = '${options.source.replace(/'/g, '')}'`);
		}

		if (whereConditions.length) {
			whereConditions.forEach((item, index) => {
				query += (index === 0) ? ' WHERE ' : ' AND ';
				query += item;
			});
		}

		// get list of all goldmine IDs matching without secured/unsecured filtering first
		return internals.mssqlQuery(query)
			.then(function (response) {
				const goldmineIds = response.map((o) => o.COMPANY);
				const accountNos = response
					.map((o) => o.ACCOUNTNO.trim())
					.filter((accountNo) => accountNo.indexOf("'") === -1); // filter out '', e.g. 660262

				if (options.type === 'secured' || options.type === 'unsecured') { // if type is not all, we need to drill in and get loanAmount and LTV and filter correctly
					query = `SELECT t1.U_COMPANY, t1.CREATEON, t1.CREATEAT, t2.ACCOUNTNO, t2.UAGNETLOAN, t2.UMGOSBALNC, t2.UPRVAL, t2.UMGOSBALNC FROM "GoldMine"."dbo"."CONTACT1" t1 INNER JOIN "GoldMine"."dbo"."CONTACT2" t2 ON t1.ACCOUNTNO = t2.ACCOUNTNO WHERE t1.ACCOUNTNO IN ('${accountNos.join('\',\'')}')`;

					// get loanAmount and ltv per each goldmine id
					return internals.mssqlQuery(query)
						.then((response) => {
							// build an array of loanAmount and ltv objects
							const arrayOfLoanAmountAndLtvs = response.reduce((prev, curr) => {
								prev.push({
									goldmineId: curr.U_COMPANY,
									createdAt: moment.utc(curr.CREATEON).add(moment.duration(curr.CREATEAT)).format('YYYY-MM-DD HH:mm:ss'),
									accountNo: curr.ACCOUNTNO.trim(),
									loanAmount: curr.UAGNETLOAN,
									propertyValue: curr.UPRVAL,
									mortgageBalance: curr.UMGOSBALNC,
									ltv: ((curr.UAGNETLOAN + curr.UMGOSBALNC) / curr.UPRVAL * 100).toFixed(2),
								});
								return prev;
							}, []);

							// filter out those that are required
							const filteredItems = arrayOfLoanAmountAndLtvs.filter((o) => {
								if (moment(o.createdAt).isBefore('2016-09-20')) {
									if (options.type === 'unsecured') {
										return (o.loanAmount < 5000 || o.ltv > 95);
									}

									if (options.type === 'secured') {
										return (o.loanAmount >= 5000 && o.ltv <= 95);
									}
								} else {
									function isUnsecured(goldmineRecord) {
										if (goldmineRecord.loanAmount < 10000) { // rule [2] If loan amount is less than £10,000 – add to Unsecured
											return true;
										}

										if (goldmineRecord.propertyValue && !goldmineRecord.mortgageBalance && goldmineRecord.loanAmount < 25000) { // rule // [3] If there is property value, but no mortgage balance and loan is less than £25,000 – add to Unsecured
											return true;
										}

										if (!goldmineRecord.propertyValue && !goldmineRecord.mortgageBalance && goldmineRecord.loanAmount && goldmineRecord.loanAmount < 10000) { // rule [5] If no property value, no mortgage balance refer and loan size is less than £10,000 – add to Unsecured
											return true;
										}

										return false;
									}

									if (options.type === 'unsecured') {
										return (isUnsecured(o) === true);
									}

									if (options.type === 'secured') {
										return (isUnsecured(o) === false);
									}
								}
							});

							// once filtered, array misses goldmine ids, has accountnos. have to get goldmine ids again
							const accountNosFiltered = filteredItems.map((o) => o.accountNo.trim());
							query = `SELECT COMPANY FROM "GoldMine"."dbo"."CONTACT1"  WHERE ACCOUNTNO IN ('${accountNosFiltered.join('\',\'')}')`;

							return internals.mssqlQuery(query)
								.then((response) => resolve(response.map((o) => o.COMPANY))) // reply with filtered goldmine ids
								.catch((error) => reject(error));
						});
				}

				// return all ids, without filtering out secured/unsecured
				return resolve(goldmineIds);
			})
			.catch(function (error) {
				reject(error);
			});
	});
};

internals.listRecordsStatusHistory = function (ids) {
	return new Promise((resolve, reject) => {
		const schema = Joi.array().required().description('array of goldmine ids').example([653749, 653750, 653779]);

		Joi.validate(ids, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let query = `SELECT ACCOUNTNO, COMPANY FROM "GoldMine"."dbo"."CONTACT1" WHERE U_COMPANY IN ('${ids.join('\',\'')}')`;

		// get all goldmine IDs
		return internals.mssqlQuery(query)
			.then(function (response) {
				const accountNumbers = response.map((o) => o.ACCOUNTNO.trim());
				const arrayOfHistories = [];

				const arrChunks = chunkArray(accountNumbers, 200);
				let countAdded = 0;
				if (arrChunks && arrChunks.length > 0) {
					for (let i = 0; i < arrChunks.length; i++) {

						query = `SELECT t1.*, t2.U_KEY2 FROM "GoldMine"."dbo"."CONTHIST" t1 INNER JOIN "GoldMine"."dbo"."CONTACT1" t2 ON t1.ACCOUNTNO=t2.ACCOUNTNO WHERE t1.ACCOUNTNO IN ('${arrChunks[i].join('\',\'')}') AND t1.REF LIKE 'Case updated from %' ORDER BY t1.CREATEON DESC, t1.CREATEAT DESC`;

						// get history
						internals.mssqlQuery(query)
							.then(function (response) {
								const historyArrayedPerAccountNo = _.groupBy(response, (o) => o.ACCOUNTNO.trim()); // grouped by ACCOUNTNO
								_.map(historyArrayedPerAccountNo, (o) => {
									arrayOfHistories.push(getStatusChangeHistory(o, o.U_KEY2)); // U_KEY2 === current status
								});
								countAdded += 1;
								if (countAdded === arrChunks.length) {
									resolve(arrayOfHistories);
								}
							})
							.catch(function (error) {
								reject(error);
							});

					}
				}

				//resolve(arrayOfHistories);
			})
			.catch(function (error) {
				reject(error);
			});


		function chunkArray(myArray, chunkSize) {
			var index = 0;
			var arrayLength = myArray.length;
			var tempArray = [];

			for (index = 0; index < arrayLength; index += chunkSize) {
				const myChunk = myArray.slice(index, index + chunkSize);
				// Do something if you want with the group
				tempArray.push(myChunk);
			}

			return tempArray;
		}

		function getStatusChangeHistory(statusChanges, currentStatus) {
			const statusChangeHistory = [];
			let firstStatusFrom;

			statusChanges.map((i) => {
				if (i.REF.indexOf('Case updated from') >= 0) { // this is a status change
					firstStatusFrom = i.REF.split('Case updated from ')[1].split(' to ')[0]; // because iterates in reverse order, last iteration will be the first `statusFrom`, so we overwrite it every time, last time being the correct one
					const statusTo = i.REF.split('Case updated from ')[1].split(' to ')[1].split(' (oc:)')[0];
					statusChangeHistory.push(statusTo);
				}
			});

			if (!statusChangeHistory.length) { // no previous status changes, use current status
				statusChangeHistory.push(currentStatus);
			} else {
				statusChangeHistory.push(firstStatusFrom);
			}

			return _.reverse(statusChangeHistory);
		}
	});
};

internals.getStatusChanges = function (date, users, fromStatus, toStatus, lenderName) {
	return new Promise((resolve, reject) => {
		let query;

		const fromQuery = fromStatus || '%';
		const toQuery = toStatus ? `to ${toStatus}%` : '%';

		const lenderNameQuery = lenderName ? ' AND t2.KEY3 = \'' + lenderName + '\'' : '';

		if (Array.isArray(date)) {
			// date range
			query = `SELECT t2.KEY3, t2.U_COMPANY, t1.ACCOUNTNO, t1.REF, RTRIM(t1.USERID) AS USERID, t2.KEY1 FROM "Goldmine"."dbo"."CONTHIST" t1 INNER JOIN "Goldmine"."dbo"."CONTACT1" t2 ON t1.ACCOUNTNO=t2.ACCOUNTNO WHERE t1.CREATEON >= '${date[0]}' AND t1.CREATEON <= '${date[1]}' AND t1.REF LIKE 'Case updated from ${fromQuery} ${toQuery}' ${lenderNameQuery}`;
		} else {
			// single day
			query = `SELECT t2.KEY3, t2.U_COMPANY, t1.ACCOUNTNO, t1.REF, RTRIM(t1.USERID) AS USERID, t2.KEY1 FROM "Goldmine"."dbo"."CONTHIST" t1 INNER JOIN "Goldmine"."dbo"."CONTACT1" t2 ON t1.ACCOUNTNO=t2.ACCOUNTNO WHERE t1.CREATEON = '${date}' AND t1.REF LIKE 'Case updated from ${fromQuery} ${toQuery}' ${lenderNameQuery}`;
		}

		if (users) {
			query = query + ` AND t1.USERID IN ('${users.join('\',\'')}')`;
		}
		else {
			query = query + ' AND t1.USERID != \'MASTER\'';
		}

		return internals.mssqlQuery(query)
			.then((response) => {
				const statusChanges = {};
				const countsTo = {};

				response.map((o) => {
					const statusFrom = o.REF.split(' to ')[0].split('from ')[1];
					const statusTo = o.REF.split('Case updated from ')[1].split(' to ')[1].split(' (oc:)')[0];

					if (!statusChanges[statusFrom]) {
						statusChanges[statusFrom] = {};
					} // seeing for the first time
					if (!statusChanges[statusFrom][statusTo]) {
						statusChanges[statusFrom][statusTo] = 0;
					} // seeing for the first time
					statusChanges[statusFrom][statusTo] += 1;

					if (!countsTo[statusTo]) {
						countsTo[statusTo] = 0;
					}
					countsTo[statusTo] += 1;

					return o;
				});

				resolve({
					response: response,
					statusChanges: statusChanges,
					countsTo: countsTo,
				});
			})
			.catch((error) => reject(error));
	});
};

internals.getHistoricCallbacks = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({
			from: Joi.date().format('YYYY-MM-DD').optional().example('2016-06-01').description('from date, YYYY-MM-DD'),
			to: Joi.date().format('YYYY-MM-DD').optional().example('2016-06-30').description('to date, YYYY-MM-DD'),
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let query = 'SELECT t1.*, t2.COMPANY, t2.CONTACT, t2.LASTNAME, t3.UA4JBINCAF ,t2.U_KEY2 FROM "GoldMine"."dbo"."CONTHIST" t1 INNER JOIN "GoldMine"."dbo"."CONTACT1" t2 ON t1.ACCOUNTNO=t2.ACCOUNTNO INNER JOIN "GoldMine"."dbo"."CONTACT2" t3 ON t2.ACCOUNTNO=t3.ACCOUNTNO';
		const whereConditions = [];

		if (options.from && options.to) {
			whereConditions.push(`t1.ONDATE >= '${moment(options.from).format('YYYY-MM-DD').replace(/'/g, '')}' AND t1.ONDATE <= '${moment(options.to).format('YYYY-MM-DD').replace(/'/g, '')}'`);
		}

		whereConditions.push('t1.REF LIKE \'Callback Customer%\'');

		if (whereConditions.length) {
			whereConditions.forEach((item, index) => {
				query += (index === 0) ? ' WHERE ' : ' AND ';
				query += item;
			});
		}

		return internals.mssqlQuery(query)
			.then((response) => resolve(response))
			.catch((error) => reject(error));
	});
};

internals.getNewBusinessSLCs = function (date, users, fromStatus, toStatus, lenderName) {
	return new Promise((resolve, reject) => {
		let query;

		const fromQuery = fromStatus || '%';
		const toQuery = toStatus ? `to ${toStatus}%` : '%';

		const lenderNameQuery = lenderName ? ' AND t2.KEY3 = \'' + lenderName + '\'' : '';

		if (Array.isArray(date)) {
			// date range
			query = `SELECT t2.KEY3, t2.U_COMPANY, t1.ACCOUNTNO, t1.REF, RTRIM(t3.USERID) AS USERID, t2.KEY1 FROM "Goldmine"."dbo"."CONTHIST" t1 INNER JOIN "Goldmine"."dbo"."CONTACT1" t2 ON t1.ACCOUNTNO=t2.ACCOUNTNO INNER JOIN (SELECT USERID,ACCOUNTNO FROM CONTHIST WHERE REF LIKE \'Case updated from % to APP%\') t3 ON t2.ACCOUNTNO = t3.ACCOUNTNO WHERE t1.CREATEON >= '${date[0]}' AND t1.CREATEON <= '${date[1]}' AND t1.REF LIKE 'Case updated from ${fromQuery} ${toQuery}' ${lenderNameQuery}`;
		} else {
			// single day
			query = `SELECT t2.KEY3, t2.U_COMPANY, t1.ACCOUNTNO, t1.REF, RTRIM(t3.USERID) AS USERID, t2.KEY1 FROM "Goldmine"."dbo"."CONTHIST" t1 INNER JOIN "Goldmine"."dbo"."CONTACT1" t2 ON t1.ACCOUNTNO=t2.ACCOUNTNO INNER JOIN (SELECT USERID,ACCOUNTNO FROM CONTHIST WHERE REF LIKE \'Case updated from % to APP%\') t3 ON t2.ACCOUNTNO = t3.ACCOUNTNO WHERE t1.CREATEON = '${date}' AND t1.REF LIKE 'Case updated from ${fromQuery} ${toQuery}' ${lenderNameQuery}`;
		}

		if (users) {
			query = query + ` AND t3.USERID IN ('${users.join('\',\'')}')`;
		}
		else {
			query = query + ' AND t3.USERID != \'MASTER\'';
		}

		return internals.mssqlQuery(query)
			.then((response) => {
				const statusChanges = {};
				const countsTo = {};

				response.map((o) => {
					const statusFrom = o.REF.split(' to ')[0].split('from ')[1];
					const statusTo = o.REF.split('Case updated from ')[1].split(' to ')[1].split(' (oc:)')[0];

					if (!statusChanges[statusFrom]) {
						statusChanges[statusFrom] = {};
					} // seeing for the first time
					if (!statusChanges[statusFrom][statusTo]) {
						statusChanges[statusFrom][statusTo] = 0;
					} // seeing for the first time
					statusChanges[statusFrom][statusTo] += 1;

					if (!countsTo[statusTo]) {
						countsTo[statusTo] = 0;
					}
					countsTo[statusTo] += 1;

					return o;
				});

				resolve({
					response: response,
					statusChanges: statusChanges,
					countsTo: countsTo,
				});
			})
			.catch((error) => reject(error));
	});
};

internals.pushScriptContact1 = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({
			belongsTo: Joi.string().required(),
			title: Joi.string().required(),
			titleApp2: Joi.string().optional(),
			firstName: Joi.string().required(),
			lastName: Joi.string().required(),
			firstNameApp2: Joi.string().required(),
			lastNameApp2: Joi.string().required(),
			homePhone: Joi.string().required(),
			mobilePhone: Joi.string().required(),
			workPhone: Joi.string().required(),
			addressLine1: Joi.string().required(),
			addressLine2: Joi.string().required(),
			addressLine3: Joi.string().required(),
			city: Joi.string().required(),
			postCode: Joi.string().required(),
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let strOptionalFields = '';


		if (options.titleApp2) {
			strOptionalFields += `, SOURCE='${options.titleApp2}'`;
		}

		let query = `UPDATE "GoldMine"."dbo"."CONTACT1" SET DEPARTMENT='${options.title}', CONTACT='${options.firstName}', LASTNAME='${options.lastName}', TITLE='${options.firstNameApp2 + ' ' + options.lastNameApp2}', PHONE1='${options.homePhone}', PHONE2='${options.mobilePhone}', PHONE3='${options.workPhone}', ADDRESS1='${options.addressLine1}', ADDRESS2='${options.addressLine2}', ADDRESS3='${options.addressLine3}', CITY='${options.city}', ZIP='${options.postCode}'` + strOptionalFields;
		const whereConditions = [];

		whereConditions.push(`U_COMPANY = '${options.belongsTo}'`);

		if (whereConditions.length) {
			whereConditions.forEach((item, index) => {
				query += (index === 0) ? ' WHERE ' : ' AND ';
				query += item;
			});
		}

		internals.mssqlQuery(query)
			.then((response) => resolve(response))
			.catch((error) => reject(error));

		return resolve();
	});
};

internals.pushScriptContact2 = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({

			/* New Fields */

			bedrooms: Joi.number().optional(),
			construction: Joi.string().optional(),
			purchasePrice: Joi.number().optional(),
			purchaseDate: Joi.date().optional(),

			exCouncil: Joi.string().optional(),
			sittingTenant: Joi.string().optional(),
			mortgageCompany: Joi.string().optional(),
			mortgagePayments: Joi.number().optional(),
			maidenName: Joi.string().optional(),
			otherIncomeDesc: Joi.string().optional(),
			otherIncomeAmount: Joi.number().optional(),

			/* New Fields End */

			belongsTo: Joi.string().required(),
			loanAmount: Joi.number().integer().required(),
			loanPurpose: Joi.string().required(),
			residencyStatus: Joi.string().required(),
			yearsAtCurrentAddress: Joi.number().integer().optional(),
			monthsAtCurrentAddress: Joi.number().integer().optional(),
			outstandingMortgage: Joi.number().required(),
			mortgageRentPayment: Joi.number().optional(),
			dateOfBirth: Joi.date().required(),
			dateOfBirthApp2: Joi.date().required(),
			maritalStatus: Joi.string().optional(),
			maritalStatusApp2: Joi.string().optional(),
			employmentStatus: Joi.string().optional(),
			employmentStatusApp2: Joi.string().optional(),
			occupation: Joi.any().required(),
			occupationApp2: Joi.any().required(),
			yearsAtCurrentJob: Joi.number().integer().optional(),
			monthsAtCurrentJob: Joi.number().integer().optional(),
			yearsAtCurrentJobApp2: Joi.number().integer().optional(),
			monthsAtCurrentJobApp2: Joi.number().integer().optional(),
			grossAnnualIncome: Joi.number().required(),
			grossAnnualIncomeApp2: Joi.number().required(),
			yearsWithBank: Joi.number().integer().optional(),
			monthsWithBank: Joi.number().integer().optional(),
			previousAddressLine1: Joi.string().required(),
			previousAddressLine2: Joi.string().required(),
			previousAddressLine3: Joi.string().required(),
			previousCity: Joi.string().required(),
			previousPostCode: Joi.string().required(),
			paymentFrequency: Joi.string().optional(),
			proofOfIncome: Joi.string().optional(),
			propertyValue: Joi.number().integer(),
			paymentFrequencyApp2: Joi.string().optional(),
			proofOfIncomeApp2: Joi.string().optional(),
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let strOptionalFields = '';

		/* New Fields */

		if (options.exCouncil) {
			strOptionalFields += `, UPRCNOWNED='${options.exCouncil}'`;
		}

		if (options.sittingTenant) {
			strOptionalFields += `, UPRCNSITTN='${options.sittingTenant}'`;
		}

		if (options.maidenName) {
			strOptionalFields += `, UA1MAIDEN='${options.maidenName}'`;
		}

		if (options.propertyType) {
			strOptionalFields += `, UPRTYPE='${options.propertyType}'`;
		}

		if (options.bedrooms) {
			strOptionalFields += `, UPRBEDROMS=${options.bedrooms}`;
		}

		if (options.construction) {
			strOptionalFields += `, UPRCONSTCT='${options.construction}'`;
		}

		if (options.purchasePrice) {
			strOptionalFields += `, UPRPURCHPR=${options.purchasePrice}`;
		}

		if (options.purchaseDate) {
			strOptionalFields += `, UPRPURCHDT='${moment(options.purchaseDate).format('YYYY-MM-DD')}'`;
		}

		if (options.otherIncomeDesc) {
			strOptionalFields += `, UA1JBINCAS='${options.otherIncomeDesc}'`;
		}

		if (options.otherIncomeAmount) {
			strOptionalFields += `, UA1JBINCAA=${options.otherIncomeAmount}`;
		}

		if (options.mortgageCompany) {
			strOptionalFields += `, UMGCOMPANY='${options.mortgageCompany}'`;
		}

		if (options.mortgagePayments) {
			strOptionalFields += `, UMGMTHREPY=${options.mortgagePayments}`;
		}

		/*** New Fields End ***/

		if (options.yearsAtCurrentAddress) {
			strOptionalFields += `, UPRYRSATAD=${options.yearsAtCurrentAddress}`;
		}

		if (options.monthsAtCurrentAddress) {
			strOptionalFields += `, UPRMTHATAD=${options.monthsAtCurrentAddress}`;
		}
		if (options.mortgageRentPayment) {
			strOptionalFields += `, UMGMTHREPY=${options.mortgageRentPayment}`;
		}
		if (options.maritalStatus) {
			strOptionalFields += `, UA1MARITAL='${options.maritalStatus}'`;
		}
		if (options.maritalStatusApp2) {
			strOptionalFields += `, UA2MARITAL='${options.maritalStatusApp2}'`;
		}
		if (options.employmentStatus) {
			strOptionalFields += `, UA1JBEMPST='${options.employmentStatus}'`;
		}
		if (options.employmentStatusApp2) {
			strOptionalFields += `, UA2JBEMPST='${options.employmentStatusApp2}'`;
		}
		if (options.yearsAtCurrentJob) {
			strOptionalFields += `, UA1JBYRS=${options.yearsAtCurrentJob}`;
		}
		if (options.monthsAtCurrentJob) {
			strOptionalFields += `, UA1JBMTHS=${options.monthsAtCurrentJob}`;
		}
		if (options.yearsAtCurrentJobApp2) {
			strOptionalFields += `, UA2JBYRS=${options.yearsAtCurrentJobApp2}`;
		}
		if (options.monthsAtCurrentJobApp2) {
			strOptionalFields += `, UA2JBMTHS=${options.monthsAtCurrentJobApp2}`;
		}
		if (options.yearsWithBank) {
			strOptionalFields += `, UA1BANKYRS=${options.yearsWithBank}`;
		}
		if (options.monthsWithBank) {
			strOptionalFields += `, UNBSQ10h1=${options.monthsWithBank}`;
		}
		if (options.paymentFrequency) {
			strOptionalFields += `, UA1JBINCFQ='${options.paymentFrequency}'`;
		}
		if (options.proofOfIncome) {
			strOptionalFields += `, UA1JBINCPF='${options.proofOfIncome}'`;
		}
		if (options.proofOfIncomeApp2) {
			strOptionalFields += `, UA2JBINCPF='${options.proofOfIncomeApp2}'`;
		}


		let query = `UPDATE "GoldMine"."dbo"."CONTACT2" SET UAGNETLOAN=${options.loanAmount}, ULOANPURPS='${options.loanPurpose}', USERDEF01='${options.residencyStatus}', UMGOSBALNC=${options.outstandingMortgage}, UA1DOB='${moment(options.dateOfBirth).format('YYYY-MM-DD')}', UA2DOB='${moment(options.dateOfBirthApp2).format('YYYY-MM-DD')}', UA1JB='${options.occupation}', UA2JB='${options.occupationApp2}', UA1JBINCGR=${options.grossAnnualIncome}, UA2JBINCGR=${options.grossAnnualIncomeApp2}, UPRPRVADD1='${options.previousAddressLine1}', UPRPRVADD2='${options.previousAddressLine2}', UPRPRVADD3='${options.previousAddressLine3}', UPRPRVCITY='${options.previousCity}', UPRPRVPCDE='${options.previousPostCode}', UPRVAL=${options.propertyValue}` + strOptionalFields;
		const whereConditions = [];

		whereConditions.push(`ACCOUNTNO = '${options.belongsTo}'`);

		if (whereConditions.length) {
			whereConditions.forEach((item, index) => {
				query += (index === 0) ? ' WHERE ' : ' AND ';
				query += item;
			});
		}

		internals.mssqlQuery(query)
			.then((response) => resolve(response))
			.catch((error) => reject(error));

		return resolve();
	});
};

internals.appendGoldmineNotes = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({
			id: Joi.string().required(),
			notes: Joi.string().required(),
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let query = `UPDATE "GoldMine"."dbo"."CONTACT1" SET NOTES='${options.notes}' + CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10) + CONVERT(NVARCHAR(MAX),ISNULL(NOTES,'')) `;
		const whereConditions = [];

		whereConditions.push(`U_COMPANY = '${options.id}'`);

		if (whereConditions.length) {
			whereConditions.forEach((item, index) => {
				query += (index === 0) ? ' WHERE ' : ' AND ';
				query += item;
			});
		}

		internals.mssqlQuery(query)
			.then((response) => resolve(response))
			.catch((error) => reject(error));

		return resolve();
	});
};

internals.pushEmailAddress = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({
			belongsTo: Joi.string().required(),
			emailAddress: Joi.string().required(),
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let query = `UPDATE "GoldMine"."dbo"."CONTSUPP" SET CONTSUPREF='${options.emailAddress}' `;
		const whereConditions = [];

		whereConditions.push(`ACCOUNTNO = '${options.belongsTo}'`);

		if (whereConditions.length) {
			whereConditions.forEach((item, index) => {
				query += (index === 0) ? ' WHERE ' : ' AND ';
				query += item;
			});
		}

		internals.mssqlQuery(query)
			.then((response) => resolve(response))
			.catch((error) => reject(error));

		return resolve();
	});
};

internals.getDueNOCs = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({
			dateUpdated: Joi.date().optional(),
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let query = `SELECT T1.COMPANY, T1.KEY2, T2.REF, T2.NOTES, T2.CREATEON, T2.CREATEAT FROM "GoldMine"."dbo"."CONTACT1" T1 INNER JOIN "GoldMine"."dbo"."CONTHIST" T2 ON T1.ACCOUNTNO = T2.ACCOUNTNO `;
		const whereConditions = [];

		whereConditions.push(`T1.KEY2 = 'NOC'`);
		whereConditions.push(`T2.REF LIKE '%to NOC%'`);
		whereConditions.push(`CONVERT(VARCHAR, T2.NOTES) = 'Updated via bulk update tool'`);
		whereConditions.push(`T2.CREATEON = '${options.dateUpdated}'`);

		if (whereConditions.length) {
			whereConditions.forEach((item, index) => {
				query += (index === 0) ? ' WHERE ' : ' AND ';
				query += item;
			});
		}

		return internals.mssqlQuery(query)
			.then((response) => resolve(response))
			.catch((error) => reject(error));
	});
}

internals.updateRecordStatus = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({
			id: Joi.string().required(),
			newStatus: Joi.string().required(),
			notes: Joi.string().required().allow(''),
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let query = `UPDATE "GoldMine"."dbo"."CONTACT1" SET KEY2='${options.newStatus}', U_KEY2='${options.newStatus}', NOTES='${options.notes}' + CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10) + CONVERT(NVARCHAR(MAX),ISNULL(NOTES,'')) `;
		const whereConditions = [];

		whereConditions.push(`U_COMPANY = '${options.id}'`);

		if (whereConditions.length) {
			whereConditions.forEach((item, index) => {
				query += (index === 0) ? ' WHERE ' : ' AND ';
				query += item;
			});
		}

		internals.mssqlQuery(query)
			.then((response) => resolve(response))
			.catch((error) => reject(error));

		return resolve();
	});
};

internals.addStatusUpdateLog = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({
			accountNo: Joi.string().required(), // ACCOUNTNO of goldmine record.
			oldStatus: Joi.string().required(),
			newStatus: Joi.string().required(),
			recid: Joi.string().required(),
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let currentDate = moment();

		let query = `INSERT INTO "GoldMine"."dbo"."CONTHIST" (USERID,ACCOUNTNO,SRECTYPE,RECTYPE,ONDATE,ONTIME,ACTVCODE,RESULTCODE,STATUS,DURATION,UNITS,REF,NOTES,LINKRECID,LOPRECID,CREATEBY,CREATEON,CREATEAT,LASTUSER,LASTDATE,LASTTIME,recid) ` +
			`VALUES ('MASTER','${options.accountNo}','T','T','${currentDate.format('YYYY-MM-DD')}','','','COM',' 0','','','Case updated from ${options.oldStatus} to ${options.newStatus} (oc:)','Updated via bulk update tool','','','MASTER','${currentDate.format('YYYY-MM-DD')}','${currentDate.format('HH:mm')}','MASTER','${currentDate.format('YYYY-MM-DD')}','${currentDate.format('HH:mm')}','${options.recid}')`;

		internals.mssqlQuery(query)
			.then((response) => resolve(response))
			.catch((error) => reject(error));

		return resolve();
	});
}

internals.deleteGMTask = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({
			accountNo: Joi.string().required(), // ACCOUNTNO of goldmine record.
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let query = `DELETE "GoldMine"."dbo"."CAL" WHERE ACCOUNTNO = '${options.accountNo}' AND (ACTVCODE = 'NBC' OR REF = 'Callback Customer' OR REF LIKE 'New Business Call Required%') AND CREATEBY = 'MASTER' `;

		internals.mssqlQuery(query)
			.then((response) => resolve(response))
			.catch((error) => reject(error));

		return resolve();
	});
}

internals.createNewGMTask = function (options) {
	return new Promise((resolve, reject) => {
		const schema = Joi.object().keys({
			userId: Joi.string().required(),
			accountNo: Joi.string().required(),
			customerName: Joi.string().required(),
			reference: Joi.string().required(),
			recid: Joi.string().required(),
		}).required();

		Joi.validate(options, schema, (error) => {
			if (error) {
				return reject(error);
			}
		});

		let currentDate = moment();

		let query = `INSERT INTO "GoldMine"."dbo"."CAL" (USERID,ACCOUNTNO,ONDATE,ONTIME,ENDDATE,ALARMFLAG,ALARMTIME,ALARMDATE,ACTVCODE,RSVP,DURATION,RECTYPE,ACONFIRM,APPTUSER,STATUS,DIRCODE,NUMBER1,NUMBER2,COMPANY,REF,NOTES,LINKRECID,LDOCRECID,LOPRECID,CREATEBY,CREATEON,CREATEAT,LASTUSER,LASTDATE,LASTTIME,recid) ` +
			`VALUES ('${options.userId}','${options.accountNo}','${currentDate.format('YYYY-MM-DD')}','','${currentDate.format('YYYY-MM-DD')}','Y','${currentDate.clone().add(1, 'minutes').format('HH:mm')}','${currentDate.format('YYYY-MM-DD')}','WPL','N',5,'T','','',1,'GM',0,0,'${options.customerName}','${options.reference}','${options.reference}','','','','MASTER','${currentDate.format('YYYY-MM-DD')}','${currentDate.format('HH:mm')}','MASTER','${currentDate.format('YYYY-MM-DD')}','${currentDate.format('HH:mm')}','${options.recid}')`;

		internals.mssqlQuery(query)
			.then((response) => resolve(response))
			.catch((error) => reject(error));

		return resolve();
	});
}

internals.updateRecordSource = function (id, newSource) {
	return new Promise((resolve, reject) => {
		let query = `UPDATE "GoldMine"."dbo"."CONTACT1" SET COUNTRY='${newSource}', U_COUNTRY='${newSource.toUpperCase()}' `;
		const whereConditions = [];

		whereConditions.push(`U_COMPANY = '${id}'`);

		if (whereConditions.length) {
			whereConditions.forEach((item, index) => {
				query += (index === 0) ? ' WHERE ' : ' AND ';
				query += item;
			});
		}

		internals.mssqlQuery(query)
			.then((response) => resolve(response))
			.catch((error) => reject(error));

		return resolve();
	});
};

internals.fetchSecuredRecords = function() {
    return new Promise((resolve, reject) => {
        var query = `SELECT TOP 15 t1.COMPANY AS goldmineId, t1.COUNTRY AS source, t2.UMHQ10 AS applicationType, t1.LASTUSER as creator FROM Goldmine.dbo.CONTACT1 AS t1 INNER JOIN Goldmine.dbo.CONTACT2 AS t2 ON t1.ACCOUNTNO = t2.ACCOUNTNO 
            WHERE (t2.UMHQ10 = 'SECURED' OR t2.UMHQ10 IS NULL) AND CONVERT(time, t1.CREATEAT) <= CONVERT(time, DATEADD(MINUTE, -10, GETDATE()))
            ORDER BY t1.CREATEON DESC`;

        internals.mssqlQuery(query)
            .then((records) => {
                resolve(records);
            })
            .catch((error) => reject(error));
    })
}
exports.register.attributes = {
	name: 'goldmine',
};
