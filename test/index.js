const Code = require('code');
const Lab = require('lab');
const lab = exports.lab = Lab.script();
const server = require('../server.js');

lab.before(function (done) {
	server.on('serverStarted', done);
});

lab.test('It will return leads as JSON API', (done) => {
	server.inject({
		url: '/leads',
	}, (res) => {
		//console.log(res.result);
		//console.log(res.headers)
		Code.expect(res.statusCode).to.equal(200);
		Code.expect(res.headers['content-type']).to.equal('application/vnd.api+json');
		//Code.expect(res.result).to.equal('Hello World\n');
		done();
	});
});

lab.test('It will return leads as JSON API', (done) => {
	server.inject({
		url: '/goldmine-records/1/actions/score-unsecured/zopa',
		method: 'POST',
		payload: {
			application: {
				addresses: {
					current: {},
					employers: {},
				},
				residencyStatus: 'foo',
				occupation: 'foo',
				grossAnnualIncome: '10',
				netMonthlyIncome: '10',
				mortgageRentPayment: '10',
				monthlyCreditCardPayment: '10',
				totalCreditCardDebt: '10',
				monthlyLoansPayment: '10',
				totalLoansDebt: '10',
				monthlyHirePurchasePayment: '10',
				totalHirePurchaseDebt: '10',
				monthlyOverdraftPayment: '10',
				totalOverdraftDebt: '10',
				otherMonthlyExpenses: '10',
				employersName: 'foo',
				employersPhone: '07979797979',
				employmentStatus: 'foo',
				bankAccountName: 'foo',
				bankSortCode: 'foo',
				bankAccountNumber: '10',
				loanPurpose: 'foo',
				bankruptInsolvent: 'foo',
				currentCCJs: 'true',
				hasDebitCard: 'true',
				brokerDeclaration: 'true',
				expectingChangesInCircumstances: 'true',
				yearsAtCurrentAddress: '10',
				scoreAgainst: 'app1',
				disposableIncome: '10',
			},
			uuid: 'foo',
			token: 'foo',
		},
	}, (res) => {
		Code.expect(res.statusCode).to.equal(200);
		Code.expect(res.headers['content-type']).to.equal('application/vnd.api+json');
		Code.expect(res.result).to.equal('Hello World\n');
		done();
	});
});
