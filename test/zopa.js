'use strict';

const Hapi = require('hapi');
const Code = require('code');
const Lab = require('lab');
const lab = exports.lab = Lab.script();

const before = lab.before;
const test = lab.test;
const describe = lab.describe;

const Sinon = require('sinon');
const Promise = require('bluebird');
const Zopa = require('../routes/external/zopa.js');

let server;

const internals = {
	goldmineRecord: {
		id: 1,
		app1: {
			title: 'Mr',
			firstName: 'Nelly',
			lastName: 'Stone',
			maidenName: '',
			dob: '1961-01-01',
			gender: 'M',
			marital: 'Living with partner',
			homePhone: '01923607605',
			mobilePhone: '07777777777',
			workPhone: '01923607605',
			employmentDetails: {
				employmentStatus: 'Employed FT',
				employmentStatusCode: 'EFT',
				grossAnnualInc: 50000,
				additionalInc: 0,
				occupation: 'Bird Watcher',
				yearsEmployed: 8,
				monthsEmployed: 0,
			},
			plannedRetirementAge: '65',
		},
		email: 'hello@healthcfoach.com',
		address: {
			postcode: 'WD25 9DL',
			county: 'Hertfordshire',
			town: 'Watford,',
			address1: '86 Westlea Avenue ',
			address2: 'Garston ',
			address3: '',
			buildingName: '',
			residencyStatus: 'Homeowner',
			residencyStatusCode: 'HWM',
			monthsAtAddress: 1,
			yearsAtAddress: 1,
		},
		addressPrevious: {
			postcode: 'sw11 5aj',
			county: 'England',
			town: 'London',
			address1: '20reform Street',
			monthsAtAddress: 0,
			yearsAtAddress: 2,
		},
		source: 'DIRECT',
		status: 'REJ',
		loanPurpose: 'Campervan',
		loanPurposeCode: 'OTH',
		loanAmount: 15000,
		propertyValue: 420000,
		mortgageBalance: 150000,
		mortgageArrears: 0,
		monthlyPayment: 200,
		ltv: '39.29',
		alarmedTasks: 0,
		lender: '1st Stop Personal Lo',
		owner: 'LUKE',
		commission: 0,
		lenderReference: '',
		sourceReference: '',
		loanTerm: 0,
		paymentFrequency: 'Annually',
		createdOn: '2007-04-26T00:00:00.000Z',
		updatedOn: '2016-07-14T00:00:00.000Z',
		lastStatusChange: {
			ref: 'Case updated from MII to REJ (oc:)',
			statusFrom: 'MII',
			statusTo: 'REJ',
			createdBy: 'MASTER',
			updatedBy: 'MASTER',
			createdAt: '2016-04-06T11:45:00.000Z',
			updatedAt: '2016-04-06T11:45:00.000Z',
			recid: 'J1BXO1K)2OV6 W<',
		},
		mortgageType: 'Endowment',
		mortgageCompany: '',
		propertyType: 'Timber Framed',
		propertyConstructionType: 'Bricks & Mortar',
		propertyNumberOfBedrooms: 4,
		propertyNumberOfHabitableRooms: 2,
		propertyEverOwnedByCouncil: 'No',
		propertyPurchasedFromCouncil: 'Yes',
		numberOfChildrenUnder18LivingAtProperty: 2,
		propertyUse: 'Residentia',
		dependentAdults: '5',
		floorsInBuilding: 5,
		propertyPurchasedOn: '2006-01-01',
		mortgagePlan: 'Variable',
	},
	request: {
		url: '/goldmine-records/1/actions/score-unsecured/zopa',
		method: 'POST',
		payload: {
			application: {
				addresses: {
					current: {
						Id: 'GB|RM|A|25817834',
						DomesticId: '25817834',
						Language: 'ENG',
						LanguageAlternatives: 'ENG',
						Department: '',
						Company: '',
						SubBuilding: '7',
						BuildingNumber: '36',
						BuildingName: 'Belgrave Lodge',
						SecondaryStreet: '',
						Street: 'Wellesley Road',
						Block: '',
						Neighbourhood: '',
						District: '',
						City: 'London',
						Line1: '7 Belgrave Lodge',
						Line2: '36 Wellesley Road',
						Line3: '',
						Line4: '',
						Line5: '',
						AdminAreaName: 'Hounslow',
						AdminAreaCode: '',
						Province: '',
						ProvinceName: '',
						ProvinceCode: '',
						PostalCode: 'W4 4BN',
						CountryName: 'United Kingdom',
						CountryIso2: 'GB',
						CountryIso3: 'GBR',
						CountryIsoNumber: 826,
						SortingNumber1: '76242',
						SortingNumber2: '',
						Barcode: '(W44BN3UH)',
						POBoxNumber: '',
						Label: '7 Belgrave Lodge\n36 Wellesley Road\nLONDON\nW4 4BN\nUNITED KINGDOM',
						Type: 'Residential',
						DataLevel: 'Premise',
					},
					employers: {
						Id: 'GB|RM|A|25817834',
						DomesticId: '25817834',
						Language: 'ENG',
						LanguageAlternatives: 'ENG',
						Department: '',
						Company: '',
						SubBuilding: '7',
						BuildingNumber: '36',
						BuildingName: 'Belgrave Lodge',
						SecondaryStreet: '',
						Street: 'Wellesley Road',
						Block: '',
						Neighbourhood: '',
						District: '',
						City: 'London',
						Line1: '7 Belgrave Lodge',
						Line2: '36 Wellesley Road',
						Line3: '',
						Line4: '',
						Line5: '',
						AdminAreaName: 'Hounslow',
						AdminAreaCode: '',
						Province: '',
						ProvinceName: '',
						ProvinceCode: '',
						PostalCode: 'W4 4BN',
						CountryName: 'United Kingdom',
						CountryIso2: 'GB',
						CountryIso3: 'GBR',
						CountryIsoNumber: 826,
						SortingNumber1: '76242',
						SortingNumber2: '',
						Barcode: '(W44BN3UH)',
						POBoxNumber: '',
						Label: '7 Belgrave Lodge\n36 Wellesley Road\nLONDON\nW4 4BN\nUNITED KINGDOM',
						Type: 'Residential',
						DataLevel: 'Premise',
					},
				},
				residencyStatus: 'foo',
				occupation: 'foo',
				grossAnnualIncome: '10',
				netMonthlyIncome: '5000',
				mortgageRentPayment: '10',
				monthlyCreditCardPayment: '10',
				totalCreditCardDebt: '10',
				monthlyLoansPayment: '10',
				totalLoansDebt: '10',
				monthlyHirePurchasePayment: '10',
				totalHirePurchaseDebt: '10',
				monthlyOverdraftPayment: '10',
				totalOverdraftDebt: '10',
				otherMonthlyExpenses: '10',
				employersName: 'foo',
				employersPhone: '07979797979',
				employmentStatus: 'foo',
				bankAccountName: 'foo',
				bankSortCode: 'foo',
				bankAccountNumber: '10',
				loanPurpose: 'foo',
				bankruptInsolvent: 'foo',
				currentCCJs: 'true',
				hasDebitCard: 'true',
				brokerDeclaration: 'true',
				expectingChangesInCircumstances: 'true',
				yearsAtCurrentAddress: '10',
				scoreAgainst: 'app1',
				disposableIncome: '2000',
				loanAmount: '5000',
				loanTerm: '36',
			},
			uuid: 'foo',
			token: 'foo',
		},
	},
};

before(function (done) {
	// stub Goldmine plugin getRecord method
	server.plugins.goldmine = {
		getRecord: function () {},
	};
	Sinon.stub(server.plugins.goldmine, 'getRecord').returns(Promise.resolve(internals.goldmineRecord));

	// stub Sequelize plugin UnsecuredScore model create method, returning a model with updateAttributes method
	server.plugins.sequelize = {
		db: {
			UnsecuredScore: {
				create: function () {},
			},
		},
	};
	Sinon.stub(server.plugins.sequelize.db.UnsecuredScore, 'create').returns(Promise.resolve({
		updateAttributes: function () {},
	}));

	// stub request.app.jwt.username
	server.ext('onPreHandler', function (request, reply) {
		request.app.jwt = {
			username: 'foo',
		};
		return reply.continue();
	});

	// Register the plugin and start server
	server = new Hapi.Server();
	server.connection();
	server.register({
		register: Zopa,
		options: {
			user: 'foo',
			password: 'bar',
			url: 'https://test-brokerapi.zopa.com',
		},
	}, function (err) {
		Code.expect(err).to.be.undefined();
		done();
	});
});

describe('Zopa', () => {
	test('test Zopa', {
		timeout: 90000,
	}, (done) => {
		server.inject(internals.request, (res) => {
			Code.expect(res.statusCode).to.equal(200);
			Code.expect(res.headers['content-type']).to.equal('application/vnd.api+json');
			Code.expect(res.result).to.equal('Hello World\n');
			done();
		});
	});
});
