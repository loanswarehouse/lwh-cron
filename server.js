'use strict';

require('dotenv').config();

const Hapi = require('hapi');

// Create a server with a host and port
const server = new Hapi.Server({});
const Nes = require('nes');

server.connection({
	port: process.env.WEB_PORT,
	routes: {
		cors: true,
	},
});

const options = {
	HapiSwagger: {
		info: {
			title: 'Loans Warehouse API Documentation',
			version: require('./package').version,
		},
		consumes: ['application/vnd.api+json'],
		produces: ['application/vnd.api+json'],
	},
	goldmine: {
		user: process.env.MSSQL_USER,
		password: process.env.MSSQL_PASSWORD,
		server: process.env.MSSQL_SERVER,
		database: process.env.MSSQL_DATABASE,
	},
	goldmineCloner: {
		user: process.env.MSSQL_CLONER_USER,
		password: process.env.MSSQL_CLONER_PASSWORD,
		server: process.env.MSSQL_CLONER_SERVER,
		database: process.env.MSSQL_CLONER_DATABASE,
	},
	updateGM: {
		user: process.env.MSSQL_UPDATEGM_USER,
		password: process.env.MSSQL_UPDATEGM_PASSWORD,
		server: process.env.MSSQL_UPDATEGM_SERVER,
		database: process.env.MSSQL_UPDATEGM_DATABASE,
	},
	sequelize: {
		host: process.env.SEQUELIZE_HOST,
		username: process.env.SEQUELIZE_USER,
		password: process.env.SEQUELIZE_PASSWORD,
		port: process.env.SEQUELIZE_PORT,
		database: process.env.SEQUELIZE_DATABASE,
		dialect: process.env.SEQUELIZE_DIALECT,
	},
	good: {
		ops: {
			interval: 1000,
		},
		reporters: {
			console: [
				{
					module: 'good-squeeze',
					name: 'Squeeze',
					args: [{
						log: '*',
						response: '*',
						request: '*',
					}],
				},
				{
					module: 'good-console',
				},
				'stdout',
			],
		},
	},
	jwt: {
		secret: process.env.JWT_SECRET,
	},
	rollbar: {
		token: process.env.ROLLBAR_TOKEN,
		environment: process.env.NODE_ENV,
		codeVersion: require('./package.json').version,
	},
	aws: {
		key: process.env.AWS_KEY,
		secret: process.env.AWS_SECRET,
		region: process.env.AWS_REGION,
	},
	leadsMs: {
		subscribeTo: process.env.LEADS_MS_SUBSCRIBE_TO,
		publishTo: {
			leadCreated: process.env.LEADS_MS_PUBLISH_TO_LEADCREATED,
		},
	},
	dxiMs: {
		subscribeTo: process.env.DXI_MS_SUBSCRIBE_TO,
		publishTo: {
			dxiRecordCreated: process.env.DXI_MS_PUBLISH_TO_DXIRECORDCREATED,
		},
		datasets: {
			secured: process.env.DXI_MS_DATASETS_SECURED,
			secured22: process.env.DXI_MS_DATASETS_SECURED_22,
			secured23: process.env.DXI_MS_DATASETS_SECURED_23,
			secured24: process.env.DXI_MS_DATASETS_SECURED_24,
			secured26: process.env.DXI_MS_DATASETS_SECURED_26,
			secured27: process.env.DXI_MS_DATASETS_SECURED_27,
			secured28: process.env.DXI_MS_DATASETS_SECURED_28,
			securedOOH: process.env.DXI_MS_DATASETS_SECURED_OOH,
			unsecured: process.env.DXI_MS_DATASETS_UNSECURED,
			unsecured33: process.env.DXI_MS_DATASETS_UNSECURED_33,
			unsecured34: process.env.DXI_MS_DATASETS_UNSECURED_34,
			unsecured35: process.env.DXI_MS_DATASETS_UNSECURED_35,
		},
		outcomes: {
			open: process.env.DXI_MS_OUTCOMES_OPEN,
			closed: process.env.DXI_MS_OUTCOMES_CLOSED,
		},
	},
	statusChangesMs: {
		subscribeTo: process.env.STATUSCHANGES_MS_SUBSCRIBE_TO,
	},
	emailsMs: {
		subscribeTo: process.env.EMAILS_MS_SUBSCRIBE_TO,
		publishTo: {
			emailSent: process.env.EMAILS_MS_PUBLISH_TO,
		},
		postmarkKey: process.env.EMAILS_MS_POSTMARK_KEY,
	},
	sns: {
		region: process.env.SNS_REGION,
		accountNo: process.env.SNS_ACCOUNTNO,
	},
	automationsMs: {
		subscribeTo: process.env.AUTOMATIONS_MS_SUBSCRIBE_TO,
	},
	textsMs: {
		subscribeTo: process.env.TEXTS_MS_SUBSCRIBE_TO,
		comapi: {
			spaceId: process.env.TEXTS_MS_COMAPI_SPACEID,
			accessToken: process.env.TEXTS_MS_COMAPI_TOKEN,
		},
	},
	datadog: {
		name: require('./package.json').name,
		environment: process.env.NODE_ENV,
		apiKey: process.env.DATADOG_API_KEY,
		appKey: process.env.DATADOG_APP_KEY,
	},
	slack: {
		webhook_url: process.env.SLACK_WEBHOOK_URL,
	},
	token: {
		secret: process.env.JWT_TOKEN_SECRET,
		expiresIn: process.env.JWT_TOKEN_EXPIRES_IN,
	},

	maxContact: {
		url: process.env.MAXCONTACT_URL,
		user: process.env.MAXCONTACT_USER,
		password: process.env.MAXCONTACT_PASSWORD
	},
	maxContactReplica: {
		user: process.env.PG_USER,
		password: process.env.PG_PASSWORD,
		host: process.env.PG_HOST,
		database: process.env.PG_DATABASE,
		port: process.env.PG_PORT
	}
};

const plugins = [
	{ register: require('nes') },
	{ register: require('hapi-auth-jwt2') },
	{ register: require('hapi-qs') },

	{ register: require('./routes/pm2-control') },

	{ register: require('./plugins/jwt'), options: options.jwt },
	{ register: require('./plugins/aws'), options: options.aws },
	{ register: require('./plugins/sqs') },
	{ register: require('./plugins/sns'), options: options.sns },
	{ register: require('./plugins/sequelize'), options: options.sequelize },
	{ register: require('./plugins/goldmine'), options: options.goldmine },
	// { register: require('./plugins/goldmineCloner'), options: options.goldmineCloner },
	{ register: require('./plugins/updateGM'), options: options.updateGM },
	{ register: require('./plugins/jsonapi'), options: { meta: { version: require('./package.json').version } } },
	//{ register: require('./plugins/slack'), options: options.slack },
	{ register: require('./plugins/route-firewall'), options: options.slack },

	//{ register: require('./cron/leads-distribution'), options: options.dxiMs },

	// { register: require('./cron/old-leads-reallocation'), options: options.dxiMs },
	// { register: require('./cron/leads-reopening') },
	// { register: require('./cron/unsecured-applications-chasing'), options: options.dxiMs },
	// { register: require('./cron/personal-loans-old-lead-reallocation'), options: options.dxiMs },
	{ register: require('./cron/lenders-applications-statuses') },
	// { register: require('./cron/secured-findercom-leads') },
	{ register: require('./cron/update-applications-overviews') },
	{ register: require('./cron/unsecured-reporting') },
	// { register: require('./cron/suspicious-leads-report') },
	{ register: require('./cron/missed-callbacks') },
	{ register: require('hapi-swagger'), options: options.HapiSwagger },
	{ register: require('good'), options: options.good },
	{ register: require('inert') },
	{ register: require('vision') },
	{ register: require('lout') },
	{ register: require('./plugins/maxcontact'), options: options.maxContact },
	{ register: require('./cron/old-leads-reallocation')},
	{ register: require('./cron/email-alerts') },

];

if (process.env.NODE_ENV === 'production') {
	plugins.push({ register: require('./plugins/rollbar'), options: options.rollbar });
}

if (process.env.NODE_ENV === 'development') {
	plugins.push({ register: require('./plugins/maxContact-rep'), options: options.maxContactReplica });
	plugins.push({ register: require('./routes/maxContact-records') });
}

// Load plugins and start server
server.register(plugins, (err) => {
	if (err) {
		throw err;
	}

	// Start the server
	server.start((err) => {
		if (err) {
			throw err;
		}
		server.log('info', { msg: 'Server running at: ' + server.info.uri });
	});

	server.on('request', function (request, event, tags) {
		// server.log('request', event);
	});

	server.on('request-internal', function (request, event, tags) {
		// server.log('request-internal', event);
	});

	server.on('request-error', function (request, error) {
		// server.log('request-error', { msg: error.message, req_id: request.id, stack: error.stack, error: error });
	});

	server.on('response', function (request) {
		// server.log('response', { msg: 'response sent for request: ' + request.id });
	});
});
