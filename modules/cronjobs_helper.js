'use strict';

const Promise = require('bluebird');

const loanTypeOptions = {
    homeownerLoan: { id: 1, value: 'Homeowner Loan' },
    personalLoan: { id: 2, value: 'Personal Loan' },
    guarantorLoan: { id: 3, value: 'Guarantor Loan' },
}

/**
 * The CronjobHelper class should be used for core functions.
 */
class CronjobHelper {

    static getUnsecuredScores(request, data, goldmineId) {
        return new Promise(function (resolve, reject) {

            request.server.plugins.sequelize.db.sequelize.query(
                `SELECT UnsecuredScores.id, UnsecuredScores.applicationReference, UnsecuredScores.requestUuid, UnsecuredScores.goldmineId, UnsecuredScores.lenderId, UnsecuredScores.loanTypeId, UnsecuredScores.lenderName AS scoreLenderName, UnsecuredScores.result, UnsecuredScores.requestResponse, UnsecuredScores.expiresAt, UnsecuredScores.customerAccepted, Lenders.name AS lenderName, Lenders.settings AS lenderSettings 
                FROM UnsecuredScores 
                INNER JOIN Lenders ON UnsecuredScores.lenderId = Lenders.id 
                WHERE UnsecuredScores.goldmineId = ${goldmineId}`
            ).then((unsecuredScores) => {
                if (unsecuredScores && unsecuredScores.length > 0) {
                    data.unsecuredScores = unsecuredScores;
                    resolve();
                } else {
                    reject();
                }
            }).catch((error) => {
                reject();
            });
        });
    }

    static getLenderDisplayName(lenderName) {
        if (lenderName) {
            let lenderDisplayName;
            if (lenderName.toLowerCase().indexOf('smart-quote') > -1) {
                lenderName = lenderName.replace('smart-quote-', '').replace(/-/g, ' ');

                let splitStr = lenderName.toLowerCase().split(' ');
                for (var i = 0; i < splitStr.length; i++) {
                    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
                }
                lenderDisplayName = splitStr.join(' ');
            } else if (lenderName.toLowerCase().indexOf('carbon-finance') > -1) {
                lenderName = lenderName.replace('carbon-finance-', '').replace(/-/g, ' ');

                let splitStr = lenderName.toLowerCase().split(' ');
                for (var i = 0; i < splitStr.length; i++) {
                    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
                }
                lenderDisplayName = splitStr.join(' ');
            } else {
                switch (lenderName.toLowerCase()) {
                    case 'amigo':
                        lenderDisplayName = 'Amigo';
                        break;
                    case 'amigo-online':
                        lenderDisplayName = 'Amigo';
                        break;
                    case 'azure-money':
                        lenderDisplayName = 'Azure Money';
                        break;
                    case 'evolution':
                        lenderDisplayName = 'Evolution Money';
                        break;
                    case 'guarantor':
                        lenderDisplayName = 'Guarantor My Loan';
                        break;
                    case 'everydayloans':
                        lenderDisplayName = 'Everyday Loans';
                        break;
                    case 'shawbrook-online':
                        lenderDisplayName = 'Shawbrook Bank';
                        break;
                    case 'mcb':
                        lenderDisplayName = 'My Community Finance';
                        break;
                    case 'firststop-group':
                        lenderDisplayName = 'First Stop Personal Loans';
                        break;
                    case 'likelyloans':
                        lenderDisplayName = 'Likely Loans';
                        break;
                    case 'lendable':
                        lenderDisplayName = 'Lendable';
                        break;
                    case 'lendingworks':
                    case 'lendingworks-online':
                        lenderDisplayName = 'Lending Works';
                        break;
                    case 'bamboo':
                        lenderDisplayName = 'Bamboo';
                        break;
                    case 'progressive':
                        lenderDisplayName = 'Progressive';
                        break;
                    case 'hitachi':
                        lenderDisplayName = 'Hitachi Capital Finance';
                        break;
                    case 'besavvi':
                        lenderDisplayName = 'Besavvi';
                        break;
                    case 'zopa':
                        lenderDisplayName = 'Zopa';
                        break;
                    case '1plus1loans':
                        lenderDisplayName = '1Plus1 Loans';
                        break;
                    case 'leap-lending':
                        lenderDisplayName = 'Leap Lending';
                        break;
                    case 'george-banco':
                        lenderDisplayName = 'George Banco';
                        break;
                    case 'trust-two':
                        lenderDisplayName = 'Trust Two';
                        break;
                    case 'buddy-loans':
                        lenderDisplayName = 'Buddy Loans';
                        break;
                    case 'livelend':
                        lenderDisplayName = 'LiveLend';
                        break;
                    case 'betterborrow':
                        lenderDisplayName = 'BetterBowwor';
                        break;
                    case 'koyo-loans':
                        lenderDisplayName = 'Koyo Loans';
                        break;
                    case 'oplo':
                        lenderDisplayName = 'Oplo';
                        break;
                    case 'tm-advances':
                        lenderDisplayName = 'TM Advances';
                        break;
                    case '118118-money':
                        lenderDisplayName = '118118 Money';
                        break;
                    case 'rac':
                        lenderDisplayName = 'RAC';
                        break;
                    case 'salad-money':
                            lenderDisplayName = 'Salad Money';
                            break;
                    case 'fintern':
                            lenderDisplayName = 'Fintern';
                            break;
                    default:
                        lenderDisplayName = lenderName;
                        break;
                }
            }
            return lenderDisplayName;
        }
        return lenderName;
    }

}/**End CronjobHelper*/

module.exports = {
    helper: CronjobHelper,
    loanTypeOptions: loanTypeOptions
};