'use strict';

const Joi = require('joi');
const Promise = require('bluebird');
const moment = require('moment');
const Boom = require('boom');

module.exports.validateBusinessRules = function (goldmineRecord, application, businessRulesSchema) {
	return new Promise(function (resolve, reject) {
		const businessAttributes = {};

		const dob = new Date(application.dateOfBirth).toISOString().slice(0, 10);

		businessAttributes.age = moment().diff(dob, 'years');
		//console.log(3, businessAttributes.age)
		businessAttributes.monthlyNetIncome = application.netMonthlyIncome;
		businessAttributes.debtToIncomeRatio = (function () {
			return ((application.creditCost ? parseInt(application.creditCost) : 0) + parseInt(application.loanAmount)) / parseInt(application.grossAnnualIncome) * 100;
		}());

		if (application.scoreAgainst === 'app1') {
			businessAttributes.loanToIncomeRatio = (function () {
				return parseInt(application.loanAmount) / parseInt(application.grossAnnualIncome) * 100;
			}());
			businessAttributes.grossAnnualIncome = application.grossAnnualIncome;
		}
		if (application.scoreAgainst === 'joint') {
			businessAttributes.loanToIncomeRatio = (function () {
				return parseInt(application.loanAmount) / (parseInt(application.grossAnnualIncome) + parseInt(application.app2.grossAnnualIncome)) * 100;
			}());
			businessAttributes.grossAnnualIncome = parseInt(application.grossAnnualIncome) + parseInt(application.app2.grossAnnualIncome);
		}

		businessAttributes.loanAmount = application.loanAmount;
		businessAttributes.loanTerm = application.loanTerm;
		businessAttributes.insolvent = application.bankruptInsolvent;
		businessAttributes.disposableIncome = application.disposableIncome;
		businessAttributes.employmentStatus = application.employmentStatus;
		businessAttributes.hasHadPaydayLoanInLast12Months = application.hasHadPaydayLoanInLast12Months;
		businessAttributes.hasHadPaydayLoanInLast6Months = application.hasHadPaydayLoanInLast6Months;
		businessAttributes.hasHadPaydayLoanInLast3Months = application.hasHadPaydayLoanInLast3Months;
		businessAttributes.currentCCJs = application.currentCCJs;

		businessAttributes.currentAddressAdminAreaName = application.addresses.current.AdminAreaName; // in England equals county. In Northern Ireland, returns 'Northern Ireland'

		businessAttributes.bankAccountName = application.bankAccountName;
		businessAttributes.bankAccountNumber = application.bankAccountNumber;
		businessAttributes.bankSortCode = application.bankSortCode;
		businessAttributes.loanPurpose = application.loanPurpose;
		businessAttributes.postCodeApp1Current = application.addresses.current.PostalCode;

		businessAttributes.residencyStatus = application.residencyStatus;

		Joi.validate(businessAttributes, businessRulesSchema, {
			abortEarly: false,
		}, function (error, value) {
			if (error) {
				if (error.isJoi) { // business rules validation failed. create a custom error with all validation errors
					const boomError = Boom.preconditionFailed();
					const errorMessages = error.details.map(function (o) { // Joi with abortEarly false, so returning array with all messages
						return o.message;
					});
					boomError.output.payload.message = errorMessages; // because errorMessages is an array, have to manually attach and array for it to be then parsed by front end

					return reject(boomError);
				}
				return reject(error);
			}
			return resolve();
		});
	});
};

module.exports.payloadSchemaScore = {
	application: Joi.object().keys({
		addresses: Joi.object().keys({
			current: Joi.object().required(),
			currentApp2: Joi.object(),
			previous: Joi.object(),
			previousApp2: Joi.object(),
			employers: Joi.object().required(),
			employersApp2: Joi.object(),
			previousEmployers: Joi.object(),
			previousEmployersApp2: Joi.object(),
		}),
		grossAnnualIncome: Joi.number(),
		netMonthlyIncome: Joi.number(),
		mortgageRentPayment: Joi.number(),
		monthlyCreditCardPayment: Joi.number().optional(),
		totalCreditCardDebt: Joi.number().optional(),
		monthlyLoansPayment: Joi.number().optional(),
		totalLoansDebt: Joi.number().optional(),
		monthlyHirePurchasePayment: Joi.number().optional(),
		totalHirePurchaseDebt: Joi.number().optional(),
		monthlyOverdraftPayment: Joi.number().optional(),
		totalOverdraftDebt: Joi.number().optional(),
		monthlyDebtPayments: Joi.number().optional(),
		outstandingDebt: Joi.number().optional(),
		expenditure: Joi.number().optional(),
		otherMonthlyExpenses: Joi.number(),
		employersName: Joi.string(),
		employersPhone: Joi.string().length(11),
		bankAccountName: Joi.string(),
		bankSortCode: Joi.string(),
		bankAccountNumber: Joi.string(),
		bankruptInsolvent: Joi.string(),
		currentCCJs: Joi.boolean(),
		brokerDeclaration: Joi.boolean(),
		expectingChangesInCircumstances: Joi.boolean(),
		detailsOfAnticipatedChangesInCircumstances: Joi.string().optional(),
		app2: Joi.object(),
		yearsAtCurrentAddress: Joi.number(),
		monthsAtCurrentAddress: Joi.number(),
		scoreAgainst: Joi.string().valid(['app1', 'app2', 'joint']),
		loanTerm: Joi.number().min(12).max(240),
		loanAmount: Joi.number().min(0).max(250000),
		businessLoanUse: Joi.object().optional(),
		businessSector: Joi.object().optional(),
		businessSubSector: Joi.object().optional(),
		disposableIncome: Joi.number(),
		hasHadPaydayLoanInLast12Months: Joi.boolean(),
		hasHadPaydayLoanInLast6Months: Joi.boolean(),
		hasHadPaydayLoanInLast3Months: Joi.boolean(),
		britishNational: Joi.boolean(),
		existingSecuredLoan: Joi.boolean().optional(),
		securedLoanLender: Joi.string().optional(),
		securedLoanBalance: Joi.string().optional(),
		securedLoanMonthlyPayments: Joi.string().optional(),
		mortgagePersonalContribution: Joi.string().optional(),
		permissionToCreditSearch: Joi.boolean().required(),
		permissionToCreditSearchJoint: Joi.boolean().optional(),
		mortgageTime: Joi.string().optional(),
		yearsWithBank: Joi.number().optional(),
		monthsWithBank: Joi.number().optional(),
		dateOfBirth: Joi.string().optional(),
		maritalStatus: Joi.string().optional(),
		loanLookingToClearEarly: Joi.boolean().optional(),
		weAndOtherCompaniesAllowedToContact: Joi.boolean().optional(),
		firstName: Joi.string().optional(),
		middleName: Joi.string().optional(),
		lastName: Joi.string().optional(),
		occupation: Joi.object(),
		gender: Joi.string().optional(),
		employedTypeOfPayslips: Joi.string().optional(),
		paymentFrequency: Joi.string().optional(),
		timeInJob: Joi.string().optional(),
		homePhone: Joi.string().min(11).optional(),
		mobilePhone: Joi.string().min(11).optional(),
		workPhone: Joi.string().min(11).optional(),
		hasEmail: Joi.string().optional(),
		bestTimeToCall: Joi.string().optional(),
		outstandingMortgage: Joi.string().optional(),
		yearsAtCurrentJob: Joi.number().optional(),
		monthsAtCurrentJob: Joi.number().optional(),
		yearsAtPreviousJob: Joi.number().optional(),
		monthsAtPreviousJob: Joi.number().optional(),
		opinionOnCreditScore: Joi.string().optional(),
		preferredBudget: Joi.string().optional(),
		loanPurpose: Joi.any().optional(),
		residencyStatus: Joi.string(),
		occupationMain: Joi.any().optional(),
		employmentStatus: Joi.string(),
		creditOutstanding: Joi.string().optional(),
		creditCost: Joi.string().optional(),
		title: Joi.string().required(),
		titleApp2: Joi.string().optional(),
		fullNameApp2: Joi.string().optional(),
		propertyValue: Joi.string().optional(),
		yearsAtPreviousAddress: Joi.number().optional(),
		monthsAtPreviousAddress: Joi.number().optional(),
		consolidationsDebts: Joi.any().optional(),
		propertyPurchasePrice: Joi.number().optional(),
	}),
	uuid: Joi.string(),
	token: Joi.string(),
};

module.exports.payloadSchemaAccept = Joi.object().keys({
	uuid: Joi.string().required(),
	token: Joi.string().required(),
	hitachiSecurityAnswer: Joi.string().optional(),
	hitachiBankSortCode: Joi.string().optional(),
	hitachiBankAccountNumber: Joi.string().optional(),
	hitachiBankAccountName: Joi.string().optional(),
	loanOptions: Joi.object().optional(),
}).unknown();


module.exports.getResidencyStatusCode = function (residencyStatus) {
	if (residencyStatus) {
		let residencyStatusCode;
		switch (residencyStatus.toLowerCase().trim()) {
			case 'homeowner':
				residencyStatusCode = 'HWM';
				break;
			case 'council tenant':
				residencyStatusCode = 'COU';
				break;
			case 'living with parents':
				residencyStatusCode = 'LWP';
				break;
			case 'tenant':
			case 'private tenant':
				residencyStatusCode = 'TPR';
				break;
			case 'shared ownership scheme':
				residencyStatusCode = 'O';
				break;
			default:
				return 'HWM';
		}
		return residencyStatusCode;
	}
	return '';
};

module.exports.getEmploymentStatusCode = function (employmentStatus) {
	if (employmentStatus) {
		let employmentStatusCode;
		switch (employmentStatus.toLowerCase().trim()) {
			case 'employed ft':
				employmentStatusCode = 'EFT';
				break;
			case 'employed pt':
				employmentStatusCode = 'EPT';
				break;
			case 'benefits':
				employmentStatusCode = 'OTH';
				break;
			case 'disabled':
				employmentStatusCode = 'DIS';
				break;
			case 'ft carer':
				employmentStatusCode = 'OTH';
				break;
			case 'house person':
				employmentStatusCode = 'HOU';
				break;
			case 'retired':
				employmentStatusCode = 'RET';
				break;
			case 'self employed':
				employmentStatusCode = 'SEL';
				break;
			case 'student':
				employmentStatusCode = 'STU';
				break;
			case 'unemployed':
				employmentStatusCode = 'UNE';
				break;
			default:
				employmentStatusCode = 'OTH';
		}
		return employmentStatusCode;
	}
	return '';
};