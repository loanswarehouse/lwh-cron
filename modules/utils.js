'use strict';

const _ = require('lodash');

// Allows to serialize objects that have properties as functions values
const serialize = function serialize(obj) {
	const object = obj;
	return _.forEach(object, function (value, key) {
		if (typeof value === 'function') {
			object[key] = value();
			return;
		}

		if (typeof value === 'object') {
			object[key] = serialize(value);
			return;
		}

		object[key] = value;
	});
};

module.exports = {
	serialize: serialize,
};
