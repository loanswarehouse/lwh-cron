'use strict';
var cron = require('cron');
const Moment = require('moment-timezone');
const CronHelper = require('../modules/cronjobs_helper');

exports.register = function (server, options, next) {
    try {
        const cronJob = cron.job("59 59 23 * * *", function () {
            // Select query
            const selectQuery = `SELECT null,
                                date(uns.createdAt) AS day,
                                uns.lenderName, 
                                IFNULL(uns.result,'error') AS result,
                                count(DISTINCT uns.id) AS requests,
                                count(DISTINCT uns.goldmineId) AS users,
                                uns.author,
                                count(uns.customerAccepted) AS accepts
                                FROM UnsecuredScores AS uns
                                WHERE uns.id > 1084853
                                GROUP BY date(uns.createdAt),
                                uns.lenderName,
                                IFNULL(uns.result,'error'),
                                uns.author`;

            server.plugins.sequelize.db.sequelize.query(selectQuery).then((scores) => {
                if (scores && scores.length > 0) {
                    let truncateQuery = `TRUNCATE TABLE UnsecuredScoresOverview; `;
                    let query = `INSERT INTO UnsecuredScoresOverview VALUES `;
                    scores[0].map((score, index) => {
                        query += `(NULL,'${Moment(score.day).format('YYYY-MM-DD')}','${CronHelper.helper.getLenderDisplayName(score.lenderName)}','${score.result}',${score.requests},${score.users},'${score.author}',${score.accepts})`;
                        if (index < scores[0].length - 1) {
                            query += `, `;
                        } else {
                            query += ` `;
                        }
                    });
                    query += `;`;
                    server.plugins.sequelize.db.sequelize.query(truncateQuery).then(() => {
                        server.plugins.sequelize.db.sequelize.query(query).catch((insertError) => {
                            if (insertError) {
                            }
                        });
                    }).catch((truncateError) => {
                        if (truncateError) {
                        }
                    });
                }
            }).catch((selectError) => {
                if (selectError) {

                }
            });

            // const query = `truncate TABLE UnsecuredScoresOverview;
            //                insert INTO UnsecuredScoresOverview SELECT null,
            //                date(uns.createdAt) AS day,
            //                uns.lenderName, 
            //                IFNULL(uns.result,'error'),
            //                count(DISTINCT uns.id) AS requests,
            //                count(DISTINCT uns.goldmineId) AS users,
            //                uns.author,
            //                count(uns.customerAccepted) AS accepts
            //                FROM UnsecuredScores AS uns
            //                WHERE uns.id > 1084853
            //                GROUP BY date(uns.createdAt),
            //                uns.lenderName,
            //                IFNULL(uns.result,'error'),
            //                uns.author`;

            // server.plugins.sequelize.db.sequelize.query(query)
            //     .catch((error) => { });
        });
        cronJob.start();
    } catch (e) { };
    return next();
};
exports.register.attributes = {
    name: 'unsecured-reporting',
};