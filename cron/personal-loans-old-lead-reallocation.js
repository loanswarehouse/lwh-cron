'use strict';

const Promise = require('bluebird');
const Joi = require('joi');
const VError = require('verror');
const Moment = require('moment-timezone');

const internals = {};

internals.schema = Joi.object().keys({
    subscribeTo: Joi.string().required(), // SQS Queue URL to pull events from
    publishTo: Joi.object().keys({
        dxiRecordCreated: Joi.string().required(), // SNS Topic ARN to publish events to
    }).required(),
    datasets: Joi.object().keys({
        secured: Joi.number().required().example('5'),
        secured22: Joi.number().required().example('5'),
        secured23: Joi.number().required().example('5'),
        secured24: Joi.number().required().example('5'),
        secured26: Joi.number().required().example('5'),
        secured27: Joi.number().required().example('5'),
        secured28: Joi.number().required().example('5'),
        securedOOH: Joi.number().required().example('5'),
        unsecured: Joi.number().required().example('6'),
        unsecured33: Joi.number().required().example('6'),
        unsecured34: Joi.number().required().example('6'),
        unsecured35: Joi.number().required().example('6'),
    }).required(),
    outcomes: Joi.object().keys({
        open: Joi.number().required().label('outcome code'),
        closed: Joi.number().required().label('outcome code'),
    }).required(),
}).required();

exports.register = function (server, options, next) {
    Joi.assert(options, internals.schema, 'Invalid options');

    internals.server = server;
    internals.options = options;

    server.dependency(['goldmine', 'dxi', 'sequelize'], function (server, next) {

        // Cron that runs at 08:00 every weekday and at 09:00 on Saturday
        // Gets all leads that are not assigned to agents yet (holding pot dataset)
        // Reallocate leads from holding pot dataset to the available agents.

        const now = new Date();
        let millisTillNextShift = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 2, 0, 0, 0) - now;
        if (millisTillNextShift < 0) {
            millisTillNextShift += 86400000;
        }

        setTimeout(function () {
            // This will run when its 00:00 mid night.

            function execute() {
                // We need to skip Sundays.

                const currentDate = Moment().tz('Europe/London');

                if (currentDate.day() !== 0) {
                    processLeads();
                }
            }

            function processLeads() {
                internals.getNeedsMoreWorkLeadsWebPersonalChasingHomeowners()
                    .then(internals.getNewProspectLeadsWebPersonalChasingHomeowners)
                    .then(internals.getNeedsMoreWorkLeadsWebPersonalChasingTenants)
                    .then(internals.getNewProspectLeadsWebPersonalChasingTenants)
                    // .then(internals.getNeedsMoreWorkLeadsWebPersonalAbandons)
                    // .then(internals.getNewProspectLeadsWebPersonalAbandons)
                    .then(internals.getNeedsMoreWorkLeadsWebPersonalTPDAccepts)
                    .then(internals.getNewProspectLeadsWebPersonalTPDAccepts)
                    .then(internals.getNeedsMoreWorkLeadsChooseWiselyHomeowners)
                    .then(internals.getNewProspectLeadsChooseWiselyHomeowners)
                    .then(internals.getNeedsMoreWorkLeadsChooseWiselyTenants)
                    .then(internals.getNewProspectLeadsChooseWiselyTenants)
                    .then(() => {
                        let leads = [];

                        if (internals.needsMoreWorkLeadsWebPersonalChasingHomeowners) {
                            leads = leads.concat(internals.needsMoreWorkLeadsWebPersonalChasingHomeowners);
                        }

                        if (internals.newProspectLeadsWebPersonalChasingHomeowners) {
                            leads = leads.concat(internals.newProspectLeadsWebPersonalChasingHomeowners);
                        }

                        if (internals.needsMoreWorkLeadsWebPersonalChasingTenants) {
                            leads = leads.concat(internals.needsMoreWorkLeadsWebPersonalChasingTenants);
                        }

                        if (internals.newProspectLeadsWebPersonalChasingTenants) {
                            leads = leads.concat(internals.newProspectLeadsWebPersonalChasingTenants);
                        }

                        // if (internals.needsMoreWorkLeadsWebPersonalAbandons) {
                        //     leads = leads.concat(internals.needsMoreWorkLeadsWebPersonalAbandons);
                        // }

                        // if (internals.newProspectLeadsWebPersonalAbandons) {
                        //     leads = leads.concat(internals.newProspectLeadsWebPersonalAbandons);
                        // }

                        if (internals.needsMoreWorkLeadsWebPersonalTPDAccepts) {
                            leads = leads.concat(internals.needsMoreWorkLeadsWebPersonalTPDAccepts);
                        }

                        if (internals.newProspectLeadsWebPersonalTPDAccepts) {
                            leads = leads.concat(internals.newProspectLeadsWebPersonalTPDAccepts);
                        }

                        if (internals.needsMoreWorkLeadsChooseWiselyHomeowners) {
                            leads = leads.concat(internals.needsMoreWorkLeadsChooseWiselyHomeowners);
                        }

                        if (internals.newProspectLeadsChooseWiselyHomeowners) {
                            leads = leads.concat(internals.newProspectLeadsChooseWiselyHomeowners);
                        }

                        if (internals.needsMoreWorkLeadsChooseWiselyTenants) {
                            leads = leads.concat(internals.needsMoreWorkLeadsChooseWiselyTenants);
                        }

                        if (internals.newProspectLeadsChooseWiselyTenants) {
                            leads = leads.concat(internals.newProspectLeadsChooseWiselyTenants);
                        }

                        if (leads.length > 0) {
                            internals.currentDate = Moment().tz('Europe/London').startOf('day');

                            leads.map((lead) => {
                                if (lead.goldmine) {
                                    return internals.server.plugins.sequelize.db.Lead.find({
                                        where: {
                                            goldmineId: lead.goldmine,
                                        },
                                    })
                                        .then((dbLead) => {
                                            if (dbLead.status === 'ENQ') {
                                                let dueDate = Moment(dbLead.createdAt);

                                                if (dueDate.hour() >= 11) {
                                                    dueDate = dueDate.clone().startOf('day').add(1, 'days');
                                                }

                                                if (dueDate.day() === 5 && dueDate.format('DD/MM/YYYY') === '23/12/2022') { // CHRISTMAS EVE
                                                    dueDate = dueDate.clone().startOf('day').add(7, 'days');


                                                } else if ((dueDate.day() === 6 && dueDate.format('DD/MM/YYYY') === '24/12/2022') || (dueDate.day() === 0 && dueDate.format('DD/MM/YYYY') === '25/12/2022') || (dueDate.day() === 1 && dueDate.format('DD/MM/YYYY') === '26/12/2022')) { // CHRISTMAS
                                                    dueDate = dueDate.clone().startOf('day').add(6, 'days');


                                                } else if (dueDate.day() === 1 && dueDate.format('DD/MM/YYYY') === '27/12/2022') { // CHRISTMAS
                                                    dueDate = dueDate.clone().startOf('day').add(5, 'days');


                                                } else if (dueDate.day() === 2 && dueDate.format('DD/MM/YYYY') === '28/12/2022') { // CHRISTMAS
                                                    dueDate = dueDate.clone().startOf('day').add(4, 'days');


                                                } if (dueDate.day() === 4 && dueDate.format('DD/MM/YYYY') === '02/06/2022') { // BANK HOLIDAY / THURSDAY
                                                    dueDate = dueDate.clone().startOf('day').add(8, 'days');


                                                } else if (dueDate.day() === 5 && dueDate.format('DD/MM/YYYY') === '03/06/2022') { // BANK HOLIDAY / FRIDAY
                                                    dueDate = dueDate.clone().startOf('day').add(7, 'days');


                                                } else if (dueDate.day() === 6 && dueDate.format('DD/MM/YYYY') === '17/09/2022') { // BANK HOLIDAY / SATURDAY
                                                    dueDate = dueDate.clone().startOf('day').add(6, 'days');


                                                } else if (dueDate.day() === 0 && dueDate.format('DD/MM/YYYY') === '18/09/2022') { // BANK HOLIDAY / SUNDAY
                                                    dueDate = dueDate.clone().startOf('day').add(5, 'days');


                                                } else if (dueDate.day() === 1 && dueDate.format('DD/MM/YYYY') === '19/09/2022') { // BANK HOLIDAY / MONDAY
                                                    dueDate = dueDate.clone().startOf('day').add(4, 'days');


                                                } else {
                                                    if (dueDate.day() === 0) {
                                                        dueDate = dueDate.clone().startOf('day').add(4, 'days');
                                                    } else if (dueDate.day() === 4 || dueDate.day() === 5 || dueDate.day() === 6) {
                                                        dueDate = dueDate.clone().startOf('day').add(5, 'days');
                                                    } else {
                                                        dueDate = dueDate.clone().startOf('day').add(3, 'days');
                                                    }
                                                }

                                                if (dueDate.isSame(internals.currentDate, 'day') || dueDate.isBefore(internals.currentDate, 'day')) {
                                                    internals.server.plugins.dxi.moveRecordToDataset(dbLead.dxiId, 89)
                                                        .catch((error) => {
                                                        });
                                                }
                                            }
                                        });
                                }
                            });
                        }
                    });
            }

            execute();
            setInterval(execute, 86400000);
        }, millisTillNextShift);


        return next();
    });

    return next();
};

internals.isWorkingHours = function (timezone) {
    const workingHours = [
        [undefined, undefined], // Sunday
        [Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Monday 09:00 - 19:00
        [Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Tuesday 09:00 - 19:00
        [Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Wednesday 09:00 - 19:00
        [Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Thursday 09:00 - 19:00
        [Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(17, 'hours').add(30, 'minutes')], // Friday 08:00 - 17:30
        [Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(14, 'hours')], // Saturday 09:00 - 14:00
    ];
    const today = (Moment().tz(timezone).day());
    return Moment().tz(timezone).isBetween(workingHours[today][0], workingHours[today][1]);
};

internals.getNeedsMoreWorkLeadsWebPersonalChasingHomeowners = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 87,
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.needsMoreWorkLeadsWebPersonalChasingHomeowners = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getNewProspectLeadsWebPersonalChasingHomeowners = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 87,
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.newProspectLeadsWebPersonalChasingHomeowners = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getNeedsMoreWorkLeadsWebPersonalChasingTenants = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 88,
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.needsMoreWorkLeadsWebPersonalChasingTenants = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getNewProspectLeadsWebPersonalChasingTenants = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 88,
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.newProspectLeadsWebPersonalChasingTenants = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getNeedsMoreWorkLeadsWebPersonalAbandons = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 82,
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.needsMoreWorkLeadsWebPersonalAbandons = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getNewProspectLeadsWebPersonalAbandons = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 82,
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.newProspectLeadsWebPersonalAbandons = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getNeedsMoreWorkLeadsWebPersonalTPDAccepts = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 90,
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.needsMoreWorkLeadsWebPersonalTPDAccepts = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getNewProspectLeadsWebPersonalTPDAccepts = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 82,
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.newProspectLeadsWebPersonalTPDAccepts = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getNeedsMoreWorkLeadsChooseWiselyHomeowners = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 77,
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.needsMoreWorkLeadsChooseWiselyHomeowners = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getNewProspectLeadsChooseWiselyHomeowners = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 77,
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.newProspectLeadsChooseWiselyHomeowners = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getNeedsMoreWorkLeadsChooseWiselyTenants = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 86,
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.needsMoreWorkLeadsChooseWiselyTenants = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getNewProspectLeadsChooseWiselyTenants = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 86,
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.newProspectLeadsChooseWiselyTenants = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

exports.register.attributes = {
    name: 'personal-loans-old-lead-reallocation',
};
