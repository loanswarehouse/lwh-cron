'use strict';

const Promise = require('bluebird');
const Joi = require('joi');
const VError = require('verror');
const Moment = require('moment-timezone');

const internals = {};

internals.schema = Joi.object().keys({
	subscribeTo: Joi.string().required(), // SQS Queue URL to pull events from
	publishTo: Joi.object().keys({
		dxiRecordCreated: Joi.string().required(), // SNS Topic ARN to publish events to
	}).required(),
	datasets: Joi.object().keys({
		secured: Joi.number().required().example('5'),
		secured22: Joi.number().required().example('5'),
		secured23: Joi.number().required().example('5'),
		secured24: Joi.number().required().example('5'),
		secured26: Joi.number().required().example('5'),
		secured27: Joi.number().required().example('5'),
		secured28: Joi.number().required().example('5'),
		securedOOH: Joi.number().required().example('5'),
		unsecured: Joi.number().required().example('6'),
		unsecured33: Joi.number().required().example('6'),
		unsecured34: Joi.number().required().example('6'),
		unsecured35: Joi.number().required().example('6'),
	}).required(),
	outcomes: Joi.object().keys({
		open: Joi.number().required().label('outcome code'),
		closed: Joi.number().required().label('outcome code'),
	}).required(),
}).required();

exports.register = function (server, options, next) {
	Joi.assert(options, internals.schema, 'Invalid options');

	internals.server = server;
	internals.options = options;

	internals.securedAgents = [
		{ agentId: '578429', name: 'Luke Hawes', dataset: 'secured23' },
		{ agentId: '779506', name: 'Toni Lamberti', dataset: 'secured27' },
		{ agentId: '878153', name: 'Shannon Stocks', dataset: 'secured22' },
		{ agentId: '878053', name: 'Chris Rex', dataset: 'secured26' },
		{ agentId: '875803', name: 'Marlon Martins', dataset: 'secured24' },
		{ agentId: '798672', name: 'John Pullen', dataset: 'secured28' },
	];

	internals.unsecuredAgents = [
		{ agentId: '743148', name: 'James Irving', dataset: 'unsecured34' },
		{ agentId: '647882', name: 'Sariah Bagley', dataset: 'unsecured35' },
		{ agentId: '796536', name: 'Kate Carns', dataset: 'unsecured33' },
	];

	internals.checkedAgents = [];

	server.dependency(['goldmine', 'dxi', 'sequelize'], function (server, next) {
		// Cron that runs every two minutes
		// Gets all leads that are not assigned to agents yet (holding pot dataset)
		// Reallocate leads from holding pot dataset to the available agents.

		let runningCron = false;

		function execute() {
			// We need it to delay one hour on Saturday since we open at 09:00.
			// We need to skip Sundays.

			if (internals.isWorkingHours('Europe/London')) {
				processLeads();
			}
		}

		function processLeads() {
			internals.getDXIAgents()
				.then(internals.getLeadsFromHoldingPot)
				.then((leads) => {
					if (leads) {
						let delay = 3000;
						internals.checkedAgents = [];

						let sortedLeads = [];
						const newProspects = leads.filter((item) => {
							return item.ProcessType.toLowerCase() === 'new prospect';
						});

						const otherLeads = leads.filter((item) => {
							return item.ProcessType.toLowerCase() !== 'new prospect' && item.ProcessType.toLowerCase() !== 'complete';
						});

						if (newProspects) {
							sortedLeads = sortedLeads.concat(newProspects);
						}

						if (otherLeads) {
							sortedLeads = sortedLeads.concat(otherLeads);
						}

						if (sortedLeads.length > 0) {
							sortedLeads = sortedLeads.slice(0, 6); // Select the first 6 leads to run.
						}


						sortedLeads.map((lead) => {
							setTimeout(function () {
								const data = {};
								data.dxiLead = lead;

								if (data.dxiLead.goldmine) {

									return internals.server.plugins.sequelize.db.Lead.find({
										where: {
											goldmineId: data.dxiLead.goldmine,
										},
									})
										.then((dbLead) => {
											if (dbLead) {
												data.lead = dbLead;
												internals.server.plugins.sequelize.db.AgentsOrder.findAll({
													limit: 1,
													where: {
														generalDataset: 'secured',
													},
													order: [['id', 'DESC']],
												})
													.then(function (entries) {
														let i = 0;
														let currentOrder = [];
														let selectedAgentId = '';

														if (entries && entries.length > 0) {
															currentOrder = JSON.parse(entries[0].currentOrder);
														}
														else {
															for (i = 0; i < internals.securedAgents.length; i++) {
																currentOrder.push(internals.securedAgents[i].agentId);
															}
														}

														if (currentOrder && currentOrder.length > 0) {
															if (internals.dxiAgents) {
																for (i = 0; i < currentOrder.length; i++) {
																	const agent = internals.dxiAgents.find((item) => {
																		return item.uid === currentOrder[i];
																	});

																	if (agent && (agent.status && agent.status.toString().toLowerCase().trim() !== 'offline' && agent.status.toString().toLowerCase().trim() !== 'wrap') && (agent.customer && agent.customer === '0')) {
																		// Any status other than offline means that the agent is logged in to dialer.
																		selectedAgentId = currentOrder[i];
																	}

																	if (selectedAgentId !== '') {
																		// Exit for loop since we found an available agent.
																		break;
																	}
																}
															}

															if (selectedAgentId !== '' && internals.checkedAgents.indexOf(selectedAgentId) === -1) {
																internals.checkedAgents.push(selectedAgentId);

																// Get agent dataset name.
																const securedAgent = internals.securedAgents.find(function (item) {
																	return item.agentId === selectedAgentId;
																});

																// Move the id of the selected agent to the end of the array.
																currentOrder.push(currentOrder.splice(currentOrder.indexOf(selectedAgentId), 1)[0]);

																const lastLeadAssignedTo = {
																	leadId: data.lead.id,
																	goldmineId: data.lead.goldmineId,
																	dataset: securedAgent.dataset,
																	agent: securedAgent.name,
																	source: 'CronJob',
																};

																const params = {
																	generalDataset: 'secured',
																	lastLeadAssignedTo: JSON.stringify(lastLeadAssignedTo),
																	currentOrder: JSON.stringify(currentOrder),
																};

																internals.server.plugins.sequelize.db.AgentsOrder.create(params)
																	.catch((error) => {
																	});


																// Move lead to the selected agent dataset.
																internals.server.plugins.dxi.moveRecordToDataset(data.dxiLead.id, internals.options.datasets[securedAgent.dataset])
																	.catch((error) => {
																	});

																const leadOptions = {
																	dataset: securedAgent.dataset,
																};

																data.lead.updateAttributes(leadOptions)
																	.catch((error) => {
																	});
															}
														}
													});
											}
										});
								}
							}, delay);

							delay += 10000;
						});
					}
				})
				.finally(() => {
					runningCron = false;
				});
		}

		execute();
		setInterval(
			function () {
				if (runningCron === false) {
					runningCron = true;
					execute();
				}
			}, 60000); // To run once every 1 minute

		return next();
	});

	return next();
};

internals.isWorkingHours = function (timezone) {
	const workingHours = [
		[undefined, undefined], // Sunday
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Monday 09:00 - 18:00
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Tuesday 09:00 - 18:00
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Wednesday 09:00 - 18:00
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Thursday 09:00 - 18:00
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(17, 'hours').add(30, 'minutes')], // Friday 08:00 - 17:30
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(14, 'hours')], // Saturday 09:00 - 14:00
	];
	const today = (Moment().tz(timezone).day());
	return Moment().tz(timezone).isBetween(workingHours[today][0], workingHours[today][1]);
};

internals.getDXIAgents = function () {
	return new Promise(function (resolve, reject) {
		internals.server.plugins.dxi.getAllAgents(internals.isWorkingHours('Europe/London'))
			.then((response) => {
				if (response) {
					internals.dxiAgents = response.list;
				}
				return resolve();
			})
			.catch((error) => reject(new VError(error, 'error plugins.dxi.getAllAgents()')));
	});
}

internals.getLeadsFromHoldingPot = function () {
	return new Promise(function (resolve, reject) {
		return internals.server.plugins.dxi.findRecords({
			dataset: 64, // Holding pot dataset
		})
			.then((results) => resolve(results))
			.catch((error) => reject(new VError(error, 'error dxi.findRecords()')));
	});
};

exports.register.attributes = {
	name: 'leads-distribution',
};
