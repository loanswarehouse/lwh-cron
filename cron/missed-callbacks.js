'use strict';

const Promise = require('bluebird');
const Request = require('request');
const Moment = require('moment-timezone');
const _ = require('lodash');

const internals = {};

exports.register = function (server, options, next) {
    internals.server = server;

    server.dependency(['sequelize'], function (server, next) {
        function execute() {
            // Implementation goes here
            _getGMCallbacks().then(_checkGMFutureCallbacks).then(_processGMCallbacks).then(_sendEmailAlert).catch(() => {
            });

            function _getGMCallbacks() {
                return new Promise(function (resolve, reject) {
                    Request({
                        method: 'GET',
                        url: 'http://192.168.1.24:3000/goldmine-historic-callbacks?from=' + Moment().format('YYYY-MM-DD') + '&to=' + Moment().add(1, 'days').format('YYYY-MM-DD'),
                    }, (error, response, body) => {
                        if (body) {
                            return resolve(JSON.parse(body));
                        }
                        reject('Error retrieving GM callbacks');
                    });
                });
            }

            function _checkGMFutureCallbacks(gmCallbacks) {
                return new Promise(function (resolve, reject) {
                    if (!gmCallbacks || gmCallbacks.length === 0 || !Array.isArray(gmCallbacks)) {
                        return reject('No callbacks were returned');
                    }
                    try {
                        Request({
                            method: 'GET',
                            url: 'http://192.168.1.24:3000/goldmine-callbacks?minutes=1000000',
                        }, (error, response, body) => {
                            if (body) {
                                let futureCallbacks = JSON.parse(body);
                                if (futureCallbacks) {
                                    gmCallbacks = gmCallbacks.filter((cbk) => {
                                        let futureIndex = futureCallbacks.findIndex((fcbk) => {
                                            return fcbk.goldmineId === cbk.goldmineId;
                                        });
                                        return (futureIndex === -1);
                                    });
                                }
                            }
                            resolve(gmCallbacks);
                        });
                    } catch (e) {
                        resolve(gmCallbacks);
                    }
                });
            }

            function _processGMCallbacks(gmCallbacks) {
                return new Promise(function (resolve, reject) {
                    if (gmCallbacks) {
                        let missedCallbacks = [];

                        gmCallbacks.sort(function (a, b) {
                            return new Date(b.callbackDueTime) - new Date(a.callbackDueTime);
                        });

                        gmCallbacks = _.uniqBy(gmCallbacks, function (cbk) {
                            return cbk.goldmineId;
                        });

                        gmCallbacks.map((cbk) => {
                            if (cbk.status === 'CBK') {
                                let dueDateTime = Moment(cbk.callbackDueTime).format('YYYY-MM-DD HH:mm');
                                if (Math.abs(Moment(dueDateTime).diff(Moment(), 'minutes')) >= 60 && Math.abs(Moment(dueDateTime).diff(Moment(), 'minutes')) < 65) {
                                    missedCallbacks.push(cbk);
                                }
                            }
                        });
                        return resolve(missedCallbacks);
                    }
                    reject('No callbacks were returned');
                });
            }

            function _sendEmailAlert(missedCallbacks) {
                return new Promise(function (resolve, reject) {
                    if (missedCallbacks && missedCallbacks.length > 0) {
                        let strCallbacks = ``;
                        missedCallbacks.map((cbk) => {
                            strCallbacks += `GM Ref: ${cbk.goldmineId} - Source: ${cbk.source} - Fullname: ${cbk.fullName} - Callback date/time: ${Moment(cbk.callbackDueTime).format('YYYY-MM-DD HH:mm')} - By ${cbk.callbackUser}<br>`
                        });

                        internals.server.plugins.aws.ses.sendEmail({
                            Source: 'it@loanswarehouse.co.uk',
                            Destination: { ToAddresses: ['newbusiness@loanswarehouse.co.uk'] },
                            Message: {
                                Subject: {
                                    Data: 'IMPORTANT - Missed Callbacks'
                                },
                                Body: {
                                    Html: {
                                        Data: 'The following callbacks were due one hour ago however they\'re still in CBK<br><br>' + strCallbacks,
                                    }
                                }
                            }
                        }, function () {
                            // Do nothing
                        });
                        return resolve();
                    }

                    reject('No missed callbacks were returned');
                });
            }
        }
        execute();
        setInterval(execute, 300000);
        return next();
    });

    return next();
};


exports.register.attributes = {
    name: 'missed-callbacks',
};