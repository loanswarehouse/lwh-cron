'use strict';

const Promise = require('bluebird');
const Joi = require('joi');
const VError = require('verror');
const Moment = require('moment-timezone');
const Request = require('request');
const CronHelper = require('../modules/cronjobs_helper');

const internals = {};

internals.schema = Joi.object().keys({
    subscribeTo: Joi.string().required(), // SQS Queue URL to pull events from
    publishTo: Joi.object().keys({
        dxiRecordCreated: Joi.string().required(), // SNS Topic ARN to publish events to
    }).required(),
    datasets: Joi.object().keys({
        secured: Joi.number().required().example('5'),
        secured22: Joi.number().required().example('5'),
        secured23: Joi.number().required().example('5'),
        secured24: Joi.number().required().example('5'),
        secured26: Joi.number().required().example('5'),
        secured27: Joi.number().required().example('5'),
        secured28: Joi.number().required().example('5'),
        securedOOH: Joi.number().required().example('5'),
        unsecured: Joi.number().required().example('6'),
        unsecured33: Joi.number().required().example('6'),
        unsecured34: Joi.number().required().example('6'),
        unsecured35: Joi.number().required().example('6'),
    }).required(),
    outcomes: Joi.object().keys({
        open: Joi.number().required().label('outcome code'),
        closed: Joi.number().required().label('outcome code'),
    }).required(),
}).required();

exports.register = function (server, options, next) {
    Joi.assert(options, internals.schema, 'Invalid options');

    internals.server = server;
    internals.options = options;

    server.dependency(['goldmine', 'dxi', 'sequelize'], function (server, next) {
        function execute() {
            processLeads();
        }

        function processLeads() { 
            internals.getRecordsFromWebPersonalLoansDiallerNewProspects()
                .then(internals.getRecordsFromWebPersonalLoansDiallerNeedsMoreWork)
                .then(internals.getRecordsFromWebPersonalLoansChasingHomeownersDiallerNewProspects)
                .then(internals.getRecordsFromWebPersonalLoansChasingHomeownersDiallerNeedsMoreWork)
                .then(internals.getRecordsFromWebPersonalLoansChasingTenantsDiallerNewProspects)
                .then(internals.getRecordsFromWebPersonalLoansChasingTenantsDiallerNeedsMoreWork)
                .then(internals.getRecordsFromWebPersonalLoansAbandonsDiallerNewProspects)
                .then(internals.getRecordsFromWebPersonalLoansAbandonsDiallerNeedsMoreWork)
                .then(() => {
                    let monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                    let today = new Date();

                    let arrLeads = [];
                    if (internals.webPersonalLeadsNewProspects && internals.webPersonalLeadsNewProspects.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsNewProspects);
                    }

                    if (internals.webPersonalLeadsNeedsMoreWork && internals.webPersonalLeadsNeedsMoreWork.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsNeedsMoreWork);
                    }

                    if (internals.webPersonalLeadsChasingHomeownersNewProspects && internals.webPersonalLeadsChasingHomeownersNewProspects.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsChasingHomeownersNewProspects);
                    }

                    if (internals.webPersonalLeadsChasingHomeownersNeedsMoreWork && internals.webPersonalLeadsChasingHomeownersNeedsMoreWork.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsChasingHomeownersNeedsMoreWork);
                    }

                    if (internals.webPersonalLeadsChasingTenantsNewProspects && internals.webPersonalLeadsChasingTenantsNewProspects.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsChasingTenantsNewProspects);
                    }

                    if (internals.webPersonalLeadsChasingTenantsNeedsMoreWork && internals.webPersonalLeadsChasingTenantsNeedsMoreWork.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsChasingTenantsNeedsMoreWork);
                    }

                    if (internals.webPersonalLeadsAbandonsNewProspects && internals.webPersonalLeadsAbandonsNewProspects.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsAbandonsNewProspects);
                    }

                    if (internals.webPersonalLeadsAbandonsNeedsMoreWork && internals.webPersonalLeadsAbandonsNeedsMoreWork.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsAbandonsNeedsMoreWork);
                    }

                    if (arrLeads.length > 0) {

                        arrLeads.sort((a, b) => (parseInt(a.id) > parseInt(b.id)) ? -1 : 1);
                        let timeout = 500;
                        
                        arrLeads.map((lead) => {
                            if (lead.goldmine) {
                                setTimeout(function () {
                                    return internals.server.plugins.sequelize.db.Lead.find({
                                        where: {
                                            goldmineId: lead.goldmine,
                                        },
                                    })
                                    .then((dbLead) => {    
                                        //Get the list of all the lenders scored based on goldmine id
                                        let dataX = {};                               
                                        return CronHelper.helper.getUnsecuredScores(internals,dataX,lead.goldmine)
                                        .then(() => {
                                            let unsecuredScores = dataX.unsecuredScores;
                                            let dateReceived = Moment(dbLead.createdAt);
                                            let gmSnapshot = JSON.parse(dbLead.snapshot);

                                            let minutesCheck = 2;

                                            if (unsecuredScores && unsecuredScores.length > 0) {
                                                if (Moment().diff(Moment(unsecuredScores[0].createdAt), 'minutes') >= minutesCheck) {   
                                                    let newStatus = '';
                                                    let newDatasetId;    
                                                    let arrLenders = unsecuredScores;                                               

                                                    //Find out lender's datasetId from the settings by checking the loanTypeId
                                                    //loanTypeId represent which loanType is the particular ID belongs to.  
                                                    //Check loanTypeOptions anum for the actual names
                                                    //Pick the correct status and dataset based on the loanTypeId checking 
                                                    try {
                                                        if (dbLead && dbLead.referral.toLowerCase().trim() === 'guarantor my loan') {
                                                            arrLenders = arrLenders.filter((lender) => {
                                                                return (lender.loanTypeId !== 3);
                                                            });
                                                        }
                                                    } catch (e) {}

                                                    arrLenders.map((lender) => {

                                                        if (!lender.result && !lender.lenderResponse) {
                                                            lender.result = { "result": "declined" };
                                                        } else {
                                                            if (lender.result && (lender.result === 'error' || lender.result === 'declined')) {
                                                                lender.result = { "result": "declined" };
                                                            } else if (lender.result && lender.result === 'skipped') {
                                                                lender.result = {
                                                                    "result": "skip",
                                                                };
                                                            } else if (lender.result && (lender.result === 'accepted' || lender.result === 'warning') && lender.requestResponse) {
                                                                let lenderResponse = JSON.parse(lender.requestResponse);
                                                                delete lenderResponse.message;
                                                                delete lenderResponse.applicationUrl;
                                                                delete lenderResponse.loanType;
                                                                lender.result = lenderResponse;
                                                            }
                                                        }                                                        

                                                        if (!lender.result) {
                                                            lender.result = { "result": "declined" };
                                                        }
                                                    });

                                                    if (dbLead && dbLead.referral.toLowerCase().trim() === 'my community finance') {
                                                        arrLenders = unsecuredScores.filter((lender) => {
                                                            if (lender.result.result === 'accepted' || lender.result.result === 'warning') {
                                                                if (parseFloat(lender.result.apr) > 69.9) {
                                                                    return false;
                                                                }
                                                            }
                                                            //return (lender.lenderName !== 'mcb' && lender.loanTypeId !== 3);

                                                        });
                                                    }

                                                    let acceptedScores = arrLenders.filter((lender) => {
                                                        return (lender.result.result === 'accepted' || lender.result.result === 'warning');
                                                    });

                                                    let cheapestLender = acceptedScores[0];                                                                                                                         
                                                    let lenderSettings;
                                                    let result = false;
                                                    let loanTypeId;
                                                    if(cheapestLender){
                                                        lenderSettings = JSON.parse(cheapestLender.lenderSettings);
                                                        result = cheapestLender.result;
                                                        loanTypeId = cheapestLender.loanTypeId;
                                                    }   
                                                       
                                                    let guarantorOption = '';
                                                    // If lender is a personal loan lender but has a guarantor option, and the selected loan type is guarantor
                                                    if (lenderSettings.settings_hasGuarantorOption && lenderSettings.settings_hasGuarantorOption === 1 && loanTypeId === CronHelper.loanTypeOptions.guarantorLoan.id) {
                                                        // Applicable for Bamboo only at the moment
                                                        guarantorOption = 'Guarantor';
                                                    }                                                 
                                                    
                                                    if (result && (result === 'accepted' || result === 'warning')) {
                                                        if (gmSnapshot.residencyStatusCode === 'HWM' || gmSnapshot.residencyStatusCode === 'H') {
                                                            newDatasetId    = lenderSettings["settings_recordDiallerDatasetOnProceedHomeowners"+guarantorOption].id;
                                                            newDatasetName  = lenderSettings["settings_recordDiallerDatasetOnProceedHomeowners"+guarantorOption].name;
                                                            newStatus       = lenderSettings["settings_recordStatusOnProceedHomeowners"+guarantorOption];
                                                        } else {
                                                            newDatasetId    = lenderSettings["settings_recordDiallerDatasetOnProceedTenants"+guarantorOption].id;
                                                            newDatasetName  = lenderSettings["settings_recordDiallerDatasetOnProceedTenants"+guarantorOption].name;
                                                            newStatus       = lenderSettings["settings_recordStatusOnProceedTenants"+guarantorOption];                                                        
                                                        }

                                                    } else {
                                                        newStatus = 'REJ';
                                                        newDatasetId = 95; // REJ Online Declines
                                                    } 

                                                    //Only move first lender to dxi dataset
                                                    if (newDatasetId) {
                                                        internals.server.plugins.dxi.moveRecordToDataset(dbLead.dxiId, newDatasetId).catch((error) => {});

                                                        if (newDatasetId === 95 && (gmSnapshot.residencyStatusCode !== 'HWM' && gmSnapshot.residencyStatusCode !== 'H')) {
                                                            // Close REJ Online Declines - Tenants
                                                            internals.server.plugins.dxi.updateOutcome(dbLead.dxiId, 506410).catch((error) => {});
                                                        }
                                                    }

                                                    const recid = '_' + new Date().getTime();

                                                    const oldStatus = dbLead.status;

                                                    //Get goldmine record by goldmineId
                                                    internals.server.plugins.goldmine.getRecord(dbLead.goldmineId).then((goldmineRecord) => {
                                                        if (goldmineRecord) {
                                                            // Update lead status in CONTACT1 table (MSSQL database - Goldmine)
                                                            let strNotes = '';
                                                            if (newStatus === 'REJ') {
                                                                strNotes = 'I am sorry, we have received your application but you have been declined by all lenders, we will review your application manually and contact you if we find a lender';
                                                            } else {
                                                                if (cheapestLender) {
                                                                    strNotes = 'Status changed to ' + newStatus + ' - Client has been ' + (cheapestLender.result.guaranteed === true ? 'Pre-Approved' : 'Provisionally Approved') + ' by ' + cheapestLender.lenderName;
                                                                } else {
                                                                    strNotes = 'Status changed to ' + newStatus + ' - online scoring';
                                                                }
                                                            }

                                                            const gmOptions = {
                                                                id: dbLead.goldmineId,
                                                                newStatus: newStatus,
                                                                notes: '*** MASTER (Master) *** ' + today.getDate() + ' ' + monthNames[today.getMonth()] + ' ' + today.getFullYear() + ' ' + today.getHours() + ':' + today.getMinutes() + '\r\n' + strNotes,
                                                            }

                                                            //Update goldmine status record
                                                            internals.server.plugins.goldmine.updateRecordStatus(gmOptions).then(() => {
                                                                    if (newStatus !== oldStatus) {
                                                                        // Add record in CONTHIST table to log the status change (MSSQL database - Goldmine)
                                                                        const gmConthistOptions = {
                                                                            accountNo: goldmineRecord.accountNo,
                                                                            oldStatus: oldStatus,
                                                                            newStatus: newStatus,
                                                                            recid: recid,
                                                                        }
                                                                        internals.server.plugins.goldmine.addStatusUpdateLog(gmConthistOptions)
                                                                            .then(() => {
                                                                                // Add record in LOG table to log the status change (MSSQL database - UpdateGM)
                                                                                const updateGMLogOptions = {
                                                                                    accountNo: goldmineRecord.accountNo,
                                                                                    oldStatus: oldStatus,
                                                                                    newStatus: newStatus,
                                                                                }
                                                                                internals.server.plugins.updateGM.addStatusUpdateLog(updateGMLogOptions)
                                                                                    .then(() => {
                                                                                        // Add record in LOGDISTINCT table to log the status change (MSSQL database - UpdateGM)
                                                                                        
                                                                                        // ------------------------------------------------------------------------------------
                                                                                    })
                                                                                    .catch((error) => {
                                                                                    });
                                                                                // ----------------------------------------------------------------------------
                                                                            })
                                                                            .catch((error) => {
                                                                            });
                                                                        // ---------------------------------------------------------------------------------
                                                                    }//End status changed check

                                                            }).catch((error) => {});
                                                            // End updateRecordStatus                                                                                                                      
                                                            // The below tasks no longer need to be created in GM
                                                            /*try {
                                                                let taskReference = '';
                                                                if (newStatus === 'UNS') {
                                                                    taskReference = 'Chase Personal Loan Accept - ' + (cheapestLender ? cheapestLender.lenderName + (cheapestLender.result.guaranteed === true ? ' (Pre-Approved)' : '') : 'Unknown');
                                                                } else if (newStatus === 'ENQ' || newStatus === 'TPD') {
                                                                    taskReference = 'Chase Secured Accept - ' + (cheapestLender ? cheapestLender.lenderName : 'Unknown');
                                                                } else if (newStatus === 'GUA') {
                                                                    taskReference = 'Chase Guarantor Accept - ' + (cheapestLender ? cheapestLender.lenderName : 'Unknown');
                                                                } else if (newStatus === 'REJ') {
                                                                    taskReference = 'Review Declined Application';
                                                                }

                                                                const gmCreateTaskOptions = {
                                                                    userId: (taskReference.toLowerCase().indexOf('evolution money') > -1 ? 'ANGELA' : 'UNSECURE'),
                                                                    accountNo: goldmineRecord.accountNo,
                                                                    customerName: goldmineRecord.app1.firstName + ' ' + goldmineRecord.app1.lastName,
                                                                    reference: taskReference,
                                                                    recid: recid,
                                                                }
                                                                internals.server.plugins.goldmine.createNewGMTask(gmCreateTaskOptions)
                                                                    .catch((error) => {
                                                                    });
                                                            } catch (e) {
                                                            }*/
                                                            // -----------------------------------------------------------------                                                        
                                                        }

                                                    }).catch((error) => {
                                                    });
                                                    //End of get goldmine details by goldmineId
                                                }//End minutesCheck

                                            } else {
                                                //unsecuredScores not exist.
                                                try {
                                                    internals.server.plugins.sequelize.db.UnsecuredWebRequest.findAll({
                                                        where: {
                                                            applicationId: dbLead.goldmineId,
                                                        },
                                                    }).then((unsecuredWebRequests) => {
                                                        let done = false;

                                                        if (dbLead.status === 'ENQ' && unsecuredWebRequests && unsecuredWebRequests.length > 0) {
                                                            let incompleteAppLogs = unsecuredWebRequests.filter((webRequest) => {
                                                                return webRequest.requestName.toString().toUpperCase() === 'INCOMPLETED APP';
                                                            });
                                                            if (incompleteAppLogs && incompleteAppLogs.length > 0 && Moment().diff(Moment(incompleteAppLogs[0].createdAt), 'minutes') >= 5) {
                                                                done = true;
                                                                if (lead.datasetid.toString() !== '82') {
                                                                    
                                                                    internals.server.plugins.dxi.moveRecordToDataset(dbLead.dxiId, 82).catch((error) => {});
                                                                }
                                                            }
                                                        }

                                                        if (done === false && Moment().diff(dateReceived, 'minutes') >= 30) {
                                                            if ((gmSnapshot.residencyStatusCode === 'HWM' || gmSnapshot.residencyStatusCode === 'O') && gmSnapshot.loanAmount >= 5000) {
                                                                if (lead.datasetid !== '87') {
                                                                    internals.server.plugins.dxi.moveRecordToDataset(dbLead.dxiId, 87) // Web leads chasing homeowners
                                                                    .catch((error) => {});
                                                                }
                                                            } else {
                                                                if (lead.datasetid !== '88') {
                                                                    internals.server.plugins.dxi.moveRecordToDataset(dbLead.dxiId, 88) // Web leads chasing tenants
                                                                    .catch((error) => {});
                                                                }
                                                            }
                                                        }
                                                    });
                                                } catch (e) {
                                                }
                                            }
                                        });
                                    });
                                }, timeout);
                            }
                            timeout += 500;
                        });
                    }
                });
        }

        execute();
        setInterval(execute, 120000);
        return next();
    });

    return next();
};

internals.getRecordsFromWebPersonalLoansDiallerNewProspects = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 81, // Web personal loans dataset ID
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.webPersonalLeadsNewProspects = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansDiallerNeedsMoreWork = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 81, // Web personal loans dataset ID
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.webPersonalLeadsNeedsMoreWork = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansChasingHomeownersDiallerNewProspects = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 87, // Web personal loans dataset ID
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.webPersonalLeadsChasingHomeownersNewProspects = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansChasingHomeownersDiallerNeedsMoreWork = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 87, // Web personal loans dataset ID
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.webPersonalLeadsChasingHomeownersNeedsMoreWork = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansChasingTenantsDiallerNewProspects = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 88, // Web personal loans dataset ID
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.webPersonalLeadsChasingTenantsNewProspects = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansChasingTenantsDiallerNeedsMoreWork = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 88, // Web personal loans dataset ID
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.webPersonalLeadsChasingTenantsNeedsMoreWork = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansAbandonsDiallerNewProspects = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 82,
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.webPersonalLeadsAbandonsNewProspects = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansAbandonsDiallerNeedsMoreWork = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 82,
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.webPersonalLeadsAbandonsNeedsMoreWork = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

exports.register.attributes = {
    name: 'personal-loans-applications',
};
