'use strict';

const Promise = require('bluebird');
const Joi = require('joi');
const VError = require('verror');
const Moment = require('moment-timezone');
const Request = require('request');

const internals = {};

exports.register = function (server, options, next) {
    internals.server = server;
    internals.options = options;

    server.dependency(['goldmine', 'dxi', 'sequelize'], function (server, next) {
        function execute() {
            processLeads();
        }

        function processLeads() {
            internals.getNeedsMoreWorkLeadsFinderHoldingZone()
                .then(internals.getNewProspectLeadsFinderHoldingZone)
                .then(() => {
                    let leads = [];

                    if (internals.needsMoreWorkLeadsFinderHoldingZone) {
                        leads = leads.concat(internals.needsMoreWorkLeadsFinderHoldingZone);
                    }

                    if (internals.newProspectLeadsFinderHoldingZone) {
                        leads = leads.concat(internals.newProspectLeadsFinderHoldingZone);
                    }

                    if (leads.length > 0) {
                        leads.map((lead) => {
                            if (lead && lead.goldmine) {
                                return internals.server.plugins.sequelize.db.Lead.find({
                                    where: {
                                        goldmineId: lead.goldmine,
                                    },
                                }).then((dbLead) => {
                                    if (dbLead && dbLead.dxiId) {
                                        if (dbLead.referral && dbLead.referral.toLowerCase().trim().indexOf('quotes.co.uk') > -1 && Moment().diff(Moment(dbLead.createdAt), 'minutes') >= 15) {
                                            internals.server.plugins.dxi.moveRecordToDataset(dbLead.dxiId, 94) // Finder.com Abandons - This is now used for all secured abandons
                                                .catch((error) => {
                                                });
                                        } else if (Moment().diff(Moment(dbLead.createdAt), 'minutes') >= 60) {
                                            internals.server.plugins.dxi.moveRecordToDataset(dbLead.dxiId, 94) // Finder.com Abandons - This is now used for all secured abandons
                                                .catch((error) => {
                                                });

                                            let newLeadSource = '';
                                            if (dbLead.referral) {
                                                if (dbLead.referral.toLowerCase().trim().indexOf('finder') > -1) {
                                                    newLeadSource = 'Abd Finder.com';
                                                } else if (dbLead.referral.toLowerCase().trim().indexOf('quick consumer') > -1) {
                                                    newLeadSource = 'Abd Quick Consumer Finance Secured';
                                                } else if (dbLead.referral.toLowerCase().trim().indexOf('must compare') > -1) {
                                                    newLeadSource = 'Abd Must Compare';
                                                } else if (dbLead.referral.toLowerCase().trim().indexOf('move iq') > -1) {
                                                    newLeadSource = 'Abd Move iQ';
                                                } else if (dbLead.referral.toLowerCase().trim().indexOf('choose wisely') > -1) {
                                                    newLeadSource = 'Abd Choose Wisely';
                                                } else if (dbLead.referral.toLowerCase().trim().indexOf('what mortgage') > -1) {
                                                    newLeadSource = 'Abd What Mortgage';
                                                } else if (dbLead.referral.toLowerCase().trim().indexOf('key secured') > -1) {
                                                    newLeadSource = 'Abd Key Secured';
                                                }
                                            }

                                            internals.server.plugins.goldmine.updateRecordSource(dbLead.goldmineId.toString(), newLeadSource.substring(0, 20)).catch((error) => {
                                            });

                                            dbLead.updateAttributes({
                                                referral: newLeadSource.substring(0, 20).toUpperCase(),
                                            }).catch((error) => {
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
        }

        execute();
        setInterval(execute, 60000); // 1 min
        return next();
    });

    return next();
};

internals.getNeedsMoreWorkLeadsFinderHoldingZone = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 93,
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.needsMoreWorkLeadsFinderHoldingZone = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getNewProspectLeadsFinderHoldingZone = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 93,
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.newProspectLeadsFinderHoldingZone = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

exports.register.attributes = {
    name: 'secured-findercom-leads',
};
