'use strict';

const Promise = require('bluebird');
const Joi = require('joi');
const VError = require('verror');
const Moment = require('moment-timezone');
const Async = require('async');
const Boom = require('boom');
const Request = require('request');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const Crypto = require('crypto');

const log4js = require('log4js');
log4js.configure({
	appenders: { cron: { type: 'file', filename: 'logger.log' } },
	categories: { default: { appenders: ['cron'], level: 'debug' } }
});

const logger = log4js.getLogger('cron');
const internals = {};

exports.register = function (server, options, next) {

	internals.server = server;
	server.dependency(['goldmine', 'maxcontact', 'sequelize'], function (server, next) {

		// Cron that runs at 08:00 every weekday and at 09:00 on Saturday
		// Gets all leads that are not assigned to agents yet (holding pot dataset)
		// Reallocate leads from holding pot dataset to the available agents.

		const now = new Date();
		let millisTillNextShift = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 1, 0, 0) - now;
		if (millisTillNextShift < 0) {
			millisTillNextShift += 86400000;
		}

		setTimeout(function () {
			// This will run when its 00:00 mid night.

			function execute() {
				// We need to skip Sundays.

				const currentDate = Moment().tz('Europe/London');

				if (currentDate.day() !== 0) {
					processLeads();
				}
			}

			function processLeads() {
				console.log('foobar');
				const data = {
					sourcesForCampaign: ['money saving advisor'],
				};
				// Assemble a datasets map
				const datasets = {
					'secured20000': 80,
					'secured': 81,
					'msaUnemcumbered': 82
				};
				retrieveSecuredLeads()
					.then(handlerOverThreeDaysLeads)
					.catch((error) => {
						logger.error('Error ocurred in processLeads()', error);
						console.log('main error handler', error);
					})

				function retrieveSecuredLeads() {
					return new Promise((resolve, reject) => {
						return internals.server.plugins.sequelize.db.Lead.findAll({
							where: {
								createdAt: {
									$gte: '2024-02-22 00:00:00',
								},
								dataset: {
									$in: ['secured', 'msaUnemcumbered', 'secured20000', 'chooseWisely']
								},
								status: 'ENQ',
								dxiOtherActions: {
									$is: null	
								}
							},
							order: 'createdAt DESC',
							attributes: ['id', 'goldmineId', 'dataset', 'dxiOtherActions', 'createdAt', 'referral']
						})
						.then((result) => {
							console.log('sec leads result: ', result);
							data.securedLeads = result || [];
							resolve();
							
						})
						.catch((e) => {
							reject(e);
						});
					})
				}
				
				function handlerOverThreeDaysLeads() {
					return new Promise((resolve, reject) => {
						const diallersNocStatusRequired = ['chooseWisely'];
						const currentStartOfDay = Moment().tz('Europe/London').startOf('day');
						// const dueLeads = data.securedLeads.filter(lead => data.sourcesForCampaign.indexOf(lead.referral.toLowerCase()) < 0 && internals.getDueDate(lead).isSame(currentStartOfDay, 'day'));
						const dueLeads = data.securedLeads.filter(lead => internals.getDueDate(lead).isSame(currentStartOfDay, 'day'));
						if (dueLeads.length > 0) {
							const ids = dueLeads.map((dueLead) => dueLead.goldmineId);
							logger.info('due leads found in old-leads-reallocation: ', ids);
							Async.eachSeries(dueLeads, function(dueLead, callback) {
								decideLeadOverThreeDaysAction()
									.then((options) => {
										return dueLead.updateAttributes(options);
									})
									.then(() => {
										callback();
									})
									.catch((error) => reject(error));

								function decideLeadOverThreeDaysAction() {
									return new Promise((resolve, reject) => {
										const columns = {};
										const nocStatusPromise = new Promise((nocResolve, nocReject) => {
											if (diallersNocStatusRequired.indexOf(dueLead.dataset) > -1) {
												// close lead in dialler and set status to NOC in Goldmine
												internals.server.plugins.goldmine.getRecord(dueLead.goldmineId)
													.then((goldmineRecord) => {
														return handleNOCStatusUpdate(
															{
																reference: dueLead.goldmineId, 
																accountNo: goldmineRecord.accountNo, 
																status: goldmineRecord.status
															}, 
															'via the cronjob'
														)
														
													})
													.then(() => {
														columns.status = 'NOC';
														nocResolve();
													})
													.catch((error) => nocReject(error));
											} else {
												nocResolve(); // No NOC status needed
											}
										})
										
										const moveLeadPromise = internals.server.plugins.maxcontact.moveLeadByLwhRefId({
											newListId: diallersNocStatusRequired.indexOf(dueLead.dataset) > -1 ? 79 : datasets[dueLead.dataset],
											outcome: diallersNocStatusRequired.indexOf(dueLead.dataset) > -1 ? 1 : 0, 
											referenceId: dueLead.goldmineId
										})
										.then(() => {
											columns.dxiOtherActions = diallersNocStatusRequired.indexOf(dueLead.dataset) > -1 ? 2 : 1
										}).catch((error) => console.log(error))

										// Run and wait both promises to complete 
										internals.allSettled([nocStatusPromise, moveLeadPromise])
											.then(() => resolve(columns))
											.catch((error) => reject(error))
									})
								}
							}, function(error) {
								if (error) {
									return reject(error);
								} 
								return resolve();
							})
						} else {
							console.log('No due leads!!')
							resolve();
						}
					})
				}

				function postLeadsToAiBot() {
					return new Promise((resolve, reject) => {
						const dueNoContactCampaignLeads = data.securedLeads//data.securedLeads.filter(lead => data.sourcesForCampaign.indexOf(lead.referral.toLowerCase()) >= 0) // remember to filter also the dueDays
						
						if (dueNoContactCampaignLeads.length > 0) {
							Async.eachSeries(dueNoContactCampaignLeads, processLead, function(err) {
								if (err) {
									return reject(err);
								}
								resolve();
							})
							const processLead = function (lead, callback) {
								internals.server.plugins.goldmine.getRecord(lead.goldmineId)
								.then((goldmineRecord) => {
									if (!goldmineRecord) {
										return reject(new Error('No goldmineRecord'));
									}
									const serviceContent = {
										campaign_id: '116',
										lead: {
											lead_ref: goldmineRecord.id,
											first_name: goldmineRecord.app1.firstName,
											last_name: goldmineRecord.app1.lastName,
											email: goldmineRecord.email,
											phone: goldmineRecord.app1.mobilePhone || goldmineRecord.app1.homePhone || goldmineRecord.app1.workPhone,
											
										},
										marketing: {
											utm_source: '',
										}
									}
										console.log('service content: ', serviceContent);
										/*console.log('status code: ', response.statusCode);
										if (response.statusCode === 200) {
											dxiMoveFieldResult = 2;
											internals.server.plugins.maxcontact.deactivateLeadByLwhRefId({ referenceId:  lead.goldmineId })
											.catch((error) => reject(error)) // keep running it in the background
	
											internals.server.plugins.goldmine.updateRecordStatus({
												id: '2127665',
												newStatus: 'NOC',
												notes: '*** MASTER (Master) ***' + noteDate + 'Status changed to NOC and has been passed over to agent Emma (Loans Warehouse AI)'
											}).then(() => {
												const gmConstHistOptions = {
													accountNo: goldmineRecord.accountNo,
													oldStatus: 'ENQ',
													newStatus: 'NOC',
													recid: Crypto.randomBytes(15).toString('hex').slice(0, 15)
												}
												internals.server.plugins.goldmine.addStatusUpdateLog(gmConstHistOptions)
													.then(() => {
														delete gmConstHistOptions.recid;
														internals.server.plugins.updateGM.addStatusUpdateLog(gmConstHistOptions).then(() => {
															internals.server.plugins.updateGM.addStatusUpdateLogDistinct(gmConstHistOptions)
															.then(() => callback())
															.catch((e) => reject(e));
														}).catch((e) => reject(e));
													})
											}).catch((e) => {
												reject(e)
												console.log('er', e);
											})
										} else {
											// set dxiMove column to 3 because lead was NOT accepted for a reason so it is not with our AI agent Emma. Let our agents to still dial 
											dxiMoveFieldResult = 3; callback()
											// internals.server.plugins.maxcontact.moveLeadByLwhRefId({
											// 	newListId: datasets[lead.dataset],
											// 	outcome: 0, 
											// 	referenceId: lead.goldmineId
											// }).then(() => callback()).catch((e) => reject(e));
										}*/
									Request({
										method: 'POST',
										url: 'https://lms.moredemand.io/api/lead/create',
										headers: {
											Authorization: 'Bearer 23|LyYm9E47NEVLYd5KrT016X8grfKe68RaTPyT2aVQ',
											Accept: 'application/json'
										},
										json: serviceContent
									}, function(error, response, body) {
										console.log('error', error);
										console.log('status code: ', response.statusCode);
										console.log('body: ', body);
										if (response.statusCode === 200) {
											internals.server.plugins.maxcontact.deactivateLeadByLwhRefId({ referenceId:  lead.goldmineId })
											.catch((error) => reject(error)) // keep running it in the background
	
											// GOLDMINE STATUS UPDATE CALL 
											handleNOCStatusUpdate({accountNo: goldmineRecord.accountNo, status: goldmineRecord.status}, 'and has been passed over to (Loans Warehouse AI bot)')
										} else {
											// set dxiMove column to 3 because lead was NOT accepted for a reason so it is not with our AI agent Emma. Let our agents to still dial 
											callback() 	
											// internals.server.plugins.maxcontact.moveLeadByLwhRefId({
											// 	newListId: datasets[lead.dataset],
											// 	outcome: 0, 
											// 	referenceId: lead.goldmineId
											// }).then(() => callback()).catch((e) => reject(e));
											
											
										}
										
									})
	
								})
								.catch((e) => {console.log(e)});
							}
							
						} else {
							resolve();
						}
					})
				}

				function handleNOCStatusUpdate(goldmineRecord, nocReason) {
					return new Promise((resolve, reject) => {
						const monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
						const today = new Date();
						const noteDate = today.getDate() + ' ' + monthNames[today.getMonth()] + ' ' + today.getFullYear() + ' ' + today.getHours() + ':' + today.getMinutes() + '\r\n' 									
						
						internals.server.plugins.goldmine.updateRecordStatus({
							id: goldmineRecord.reference,
							newStatus: 'NOC',
							notes: '*** MASTER (Master) ***' + noteDate + 'Status changed to NOC ' + nocReason
						}).then(() => {
							const gmConstHistOptions = {
								accountNo: goldmineRecord.accountNo,
								oldStatus: goldmineRecord.status,
								newStatus: 'NOC',
								recid: Crypto.randomBytes(15).toString('hex').slice(0, 15)
							}
							internals.server.plugins.goldmine.addStatusUpdateLog(gmConstHistOptions)
								.then(() => {
									delete gmConstHistOptions.recid;
									internals.server.plugins.updateGM.addStatusUpdateLog(gmConstHistOptions).then(() => {
										internals.server.plugins.updateGM.addStatusUpdateLogDistinct(gmConstHistOptions)
										.then(() => resolve())
										.catch((e) => reject(e));
									}).catch((e) => reject(e));
								})
						}).catch((e) => {
							reject(e)
							console.log('er', e);
						})
					})
				}
			}

			execute();
			setInterval(execute, 86400000);
		}, millisTillNextShift);


		return next();
	});

	return next();
};


internals.getDueDate = function (dbLead) {
	let dueDate = Moment(dbLead.createdAt);

	if (dueDate.hour() >= 11) {
		dueDate = dueDate.clone().startOf('day').add(1, 'days');
	}

	if (dueDate.day() === 5 && dueDate.format('DD/MM/YYYY') === '23/12/2022') { // CHRISTMAS EVE
		dueDate = dueDate.clone().startOf('day').add(7, 'days');


	} else if ((dueDate.day() === 6 && dueDate.format('DD/MM/YYYY') === '24/12/2022') || (dueDate.day() === 0 && dueDate.format('DD/MM/YYYY') === '25/12/2022') || (dueDate.day() === 1 && dueDate.format('DD/MM/YYYY') === '26/12/2022')) { // CHRISTMAS
		dueDate = dueDate.clone().startOf('day').add(6, 'days');


	} else if (dueDate.day() === 2 && dueDate.format('DD/MM/YYYY') === '27/12/2022') { // CHRISTMAS
		dueDate = dueDate.clone().startOf('day').add(5, 'days');


	} else if (dueDate.day() === 3 && dueDate.format('DD/MM/YYYY') === '28/12/2022') { // CHRISTMAS
		dueDate = dueDate.clone().startOf('day').add(4, 'days');


	} else if (dueDate.day() === 5 && dueDate.format('DD/MM/YYYY') === '30/12/2022') { // NEW YEAR
		dueDate = dueDate.clone().startOf('day').add(7, 'days');


	} else if (dueDate.day() === 6 && dueDate.format('DD/MM/YYYY') === '31/12/2022') { // NEW YEAR
		dueDate = dueDate.clone().startOf('day').add(6, 'days');


	} else if (dueDate.day() === 0 && dueDate.format('DD/MM/YYYY') === '01/01/2023') { // NEW YEAR
		dueDate = dueDate.clone().startOf('day').add(5, 'days');


	} else if (dueDate.day() === 1 && dueDate.format('DD/MM/YYYY') === '02/01/2023') { // NEW YEAR
		dueDate = dueDate.clone().startOf('day').add(4, 'days');


	} else if (dueDate.day() === 2 && dueDate.format('DD/MM/YYYY') === '03/01/2023') { // NEW YEAR
		dueDate = dueDate.clone().startOf('day').add(3, 'days');


	} else if (dueDate.day() === 5 && dueDate.format('DD/MM/YYYY') === '07/04/2023') { // GOOD FRIDAY
		dueDate = dueDate.clone().startOf('day').add(7, 'days');


	} else if (dueDate.day() === 6 && dueDate.format('DD/MM/YYYY') === '08/04/2023') { // EASTER WEEKEND / SATURDAY
		dueDate = dueDate.clone().startOf('day').add(6, 'days');


	} else if (dueDate.day() === 0 && dueDate.format('DD/MM/YYYY') === '09/04/2023') { // EASTER WEEKEND / SUNDAY
		dueDate = dueDate.clone().startOf('day').add(5, 'days');


	} else if (dueDate.day() === 1 && dueDate.format('DD/MM/YYYY') === '10/04/2023') { // EASTER MONDAY
		dueDate = dueDate.clone().startOf('day').add(4, 'days');


	} else if (dueDate.day() === 6 && dueDate.format('DD/MM/YYYY') === '29/04/2023') { // EARLY MAY BANK HOLIDAY / SATURDAY
		dueDate = dueDate.clone().startOf('day').add(6, 'days');


	} else if (dueDate.day() === 0 && dueDate.format('DD/MM/YYYY') === '30/04/2023') { // EARLY MAY BANK HOLIDAY / SUNDAY
		dueDate = dueDate.clone().startOf('day').add(5, 'days');


	} else if (dueDate.day() === 1 && dueDate.format('DD/MM/YYYY') === '01/05/2023') { // EARLY MAY BANK HOLIDAY
		dueDate = dueDate.clone().startOf('day').add(4, 'days');


	} else if (dueDate.day() === 4 && dueDate.format('DD/MM/YYYY') === '02/06/2022') { // SPRING BANK HOLIDAY / THURSDAY
		dueDate = dueDate.clone().startOf('day').add(8, 'days');


	} else if (dueDate.day() === 5 && dueDate.format('DD/MM/YYYY') === '03/06/2022') { // PLATINUM JUBILEE BANK HOLIDAY / FRIDAY
		dueDate = dueDate.clone().startOf('day').add(7, 'days');


	} else if (dueDate.day() === 6 && dueDate.format('DD/MM/YYYY') === '04/06/2022') { // SPRING BANK HOLIDAY / SATURDAY
		dueDate = dueDate.clone().startOf('day').add(6, 'days');


	} else if (dueDate.day() === 0 && dueDate.format('DD/MM/YYYY') === '05/06/2022') { // SPRING BANK HOLIDAY / SUNDAY
		dueDate = dueDate.clone().startOf('day').add(5, 'days');

	} else if (dueDate.day() === 6 && dueDate.format('DD/MM/YYYY') === '27/05/2023') { // BANK HOLIDAY / SATURDAY
		dueDate = dueDate.clone().startOf('day').add(6, 'days');


	} else if (dueDate.day() === 0 && dueDate.format('DD/MM/YYYY') === '28/05/2023') { // BANK HOLIDAY / SUNDAY
		dueDate = dueDate.clone().startOf('day').add(5, 'days');


	} else if (dueDate.day() === 1 && dueDate.format('DD/MM/YYYY') === '29/05/2023') { // BANK HOLIDAY / MONDAY
		dueDate = dueDate.clone().startOf('day').add(4, 'days');


	} else {
		if (dueDate.day() === 0) {
			dueDate = dueDate.clone().startOf('day').add(4, 'days');
		} else if (dueDate.day() === 4 || dueDate.day() === 5 || dueDate.day() === 6) {
			dueDate = dueDate.clone().startOf('day').add(5, 'days');
		} else {
			dueDate = dueDate.clone().startOf('day').add(3, 'days');
		}
	}

	return dueDate;
};

internals.isWorkingHours = function (timezone) {
	const workingHours = [
		[undefined, undefined], // Sunday
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Monday 09:00 - 18:00
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Tuesday 09:00 - 18:00
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Wednesday 09:00 - 18:00
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Thursday 09:00 - 18:00
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(17, 'hours').add(30, 'minutes')], // Friday 09:00 - 17:30
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(14, 'hours')], // Saturday 09:00 - 14:00
	];
	const today = (Moment().tz(timezone).day());
	return Moment().tz(timezone).isBetween(workingHours[today][0], workingHours[today][1]);
};

internals.allSettled = function (promises) {
	return Promise.all(promises.map(promise => promise.then(value => ({ status: 'fulfilled'})).catch(error => ({ status: 'rejected', reason: error}))))
}
exports.register.attributes = {
	name: 'old-leads-reallocation',
};
