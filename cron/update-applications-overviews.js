'use strict';

const Promise = require('bluebird');
const Joi = require('joi');
const VError = require('verror');
const Moment = require('moment-timezone');

const internals = {};

exports.register = function (server, options, next) {
    internals.server = server;

    server.dependency(['goldmine', 'sequelize'], function (server, next) {

        const now = new Date();
        let millisTillNextShift = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 1, 0, 0) - now;
        if (millisTillNextShift < 0) {
            millisTillNextShift += 86400000;
        }

        setTimeout(function () {
            function execute() {
                try {
                    processApplications();
                } catch (e) {
                }
            }

            function processApplications() {
                internals.getPendingApplications()
                    .then(() => internals.getUnsecuredScores30Days())
                    .then(() => {
                        if (internals.applicationOverviwes && internals.applicationOverviwes.length > 0 && internals.allUnsecuredScores && internals.allUnsecuredScores.length > 0) {
                            // let timeout = 500;
                            internals.applicationOverviwes.map((applicationOverview) => {
                                // setTimeout(function () {
                                let updateOptions = {};

                                let unsecuredScores = internals.allUnsecuredScores.filter((score) => {
                                    return parseInt(score.goldmineId) === parseInt(applicationOverview.applicationId);
                                });


                                let customerAcceptedScores;

                                if (unsecuredScores) {
                                    customerAcceptedScores = unsecuredScores.filter((score) => {
                                        return (score.customerAccepted && score.customerAccepted !== null);
                                    });
                                    if (customerAcceptedScores) {
                                        customerAcceptedScores = customerAcceptedScores.sort(function (a, b) {
                                            return new Date(b.customerAccepted) - new Date(a.customerAccepted);
                                        });
                                        customerAcceptedScores.reverse();
                                    }
                                }

                                if (unsecuredScores && unsecuredScores.length > 0) {
                                    updateOptions.enquiry = true;
                                    updateOptions.enquiryDate = Moment(unsecuredScores[0].createdAt).format('YYYY-MM-DD HH:mm:ss');

                                    let acceptedScores = unsecuredScores.filter((unsecuredScore) => {
                                        return (unsecuredScore.result === 'accepted' || unsecuredScore.result === 'warning');
                                    });

                                    if (acceptedScores && acceptedScores.length > 0) {
                                        updateOptions.eligible = true;
                                        let offeredLoanTypes = [];
                                        acceptedScores.map((score) => {
                                            if (score.loanTypeId === 1) {
                                                if (offeredLoanTypes.indexOf('H') === -1) {
                                                    offeredLoanTypes.push('H')
                                                }
                                            } else if (score.loanTypeId === 2) {
                                                if (offeredLoanTypes.indexOf('P') === -1) {
                                                    offeredLoanTypes.push('P')
                                                }
                                            } else if (score.loanTypeId === 3) {
                                                if (offeredLoanTypes.indexOf('G') === -1) {
                                                    offeredLoanTypes.push('G')
                                                }

                                                if (!applicationOverview.guarantorSigned && score.lenderStatusUpdates && score.lenderStatusUpdates.length > 0) {
                                                    if (score.lenderStatusUpdates.toLowerCase().indexOf('pending_funds_release') > -1 || score.lenderStatusUpdates.toLowerCase().indexOf('guarantor e-signed') > -1 || score.lenderStatusUpdates.toLowerCase().indexOf('guarantor signed') > -1) {
                                                        updateOptions.guarantorSigned = true;
                                                    }
                                                }
                                            } else if (score.loanTypeId === 4) {
                                                if (offeredLoanTypes.indexOf('HP') === -1) {
                                                    offeredLoanTypes.push('HP')
                                                }
                                            } else if (score.loanTypeId === 5) {
                                                if (offeredLoanTypes.indexOf('CS') === -1) {
                                                    offeredLoanTypes.push('CS')
                                                }
                                            } else if (score.loanTypeId === 6) {
                                                if (offeredLoanTypes.indexOf('PCP') === -1) {
                                                    offeredLoanTypes.push('PCP')
                                                }
                                            } else {
                                                if (offeredLoanTypes.indexOf('P') === -1) {
                                                    offeredLoanTypes.push('P')
                                                }
                                            }
                                        })
                                        updateOptions.offeredLoanTypes = offeredLoanTypes.join(',');
                                    }

                                    if (applicationOverview.introducer && applicationOverview.introducer.toLowerCase().trim() === 'finder.com personal') {
                                        updateOptions.commission = 3;
                                    }
                                }
                                // }

                                // Check if proceeded then update the proceed details.
                                if (customerAcceptedScores && customerAcceptedScores.length > 0) {
                                    let acceptedScore = customerAcceptedScores[0];

                                    updateOptions.proceed = true;
                                    updateOptions.proceedDate = Moment(acceptedScore.customerAccepted).format('YYYY-MM-DD HH:mm:ss');


                                    if (acceptedScore) {
                                        let requestResponse = JSON.parse(acceptedScore.requestResponse);

                                        updateOptions.proceedLenderName = (requestResponse.lenderName ? requestResponse.lenderName : internals.getLenderDisplayName(acceptedScore.lenderName));
                                        updateOptions.proceedLoanAmount = requestResponse.loanAmount;

                                        switch (acceptedScore.loanTypeId) {
                                            case 1:
                                                updateOptions.proceedLoanType = 'Homeowner Loan';
                                                break;
                                            case 2:
                                                updateOptions.proceedLoanType = 'Personal Loan';
                                                break;
                                            case 3:
                                                updateOptions.proceedLoanType = 'Guarantor Loan';
                                                break;
                                            case 4:
                                                updateOptions.proceedLoanType = 'Hire Purchase';
                                                break;
                                            case 5:
                                                updateOptions.proceedLoanType = 'Conditional Sale';
                                                break;
                                            case 6:
                                                updateOptions.proceedLoanType = 'PCP';
                                                break;
                                            default:
                                                updateOptions.proceedLoanType = 'Personal Loan';
                                                break;
                                        }
                                    }
                                }

                                // Check if GM status is ULC
                                internals.server.plugins.goldmine.getRecordStatusHistory(applicationOverview.applicationId.toString())
                                    .then((goldmineRecord) => {
                                        if (goldmineRecord) {
                                            if (goldmineRecord.statusChangeHistory && goldmineRecord.statusChangeHistory.length > 0 && goldmineRecord.statusChangeHistory.indexOf('SIG') > -1) {
                                                updateOptions.signed = true;
                                            }
                                            if (goldmineRecord.status === 'ULC' || goldmineRecord.status === 'SLC') {
                                                updateOptions.completed = true;
                                                if (goldmineRecord.lastStatusChange) {
                                                    updateOptions.completionDate = goldmineRecord.lastStatusChange.createdAt;
                                                } else {
                                                    updateOptions.completionDate = Moment.utc().subtract(1, 'days').startOf('day');
                                                }
                                            }

                                            try {
                                                if (!applicationOverview.obCompleted && unsecuredScores && unsecuredScores.length > 0) {
                                                    // OB hasn't been completed
                                                    if (goldmineRecord.notes && goldmineRecord.notes.toLowerCase().indexOf('completed open banking: yes') > -1) {
                                                        // This note will exist only for customers who completed open banking
                                                        updateOptions.obCompleted = true;
                                                    } else {
                                                        if (Math.abs(Moment(applicationOverview.createdAt).diff(Moment(unsecuredScores[0].createdAt), 'minutes')) > 10) {
                                                            // Scoring was done 10 mins after application is received - ABANDONED
                                                            // if (!applicationOverview.obAbandoned) {
                                                            updateOptions.obAbandoned = true;
                                                            updateOptions.obSkipped = false;
                                                            // }
                                                        } else {
                                                            // SKIPPED
                                                            // if (!applicationOverview.obSkipped) {
                                                            updateOptions.obSkipped = true;
                                                            updateOptions.obAbandoned = false;
                                                            // }
                                                        }
                                                    }

                                                }
                                            } catch (e) {
                                                console.log("AHMAD - APPLICATION OVERVIEWS - OB STATUS ERROR", e);
                                            }
                                        }

                                        if (Object.keys(updateOptions).length > 0) {
                                            applicationOverview.updateAttributes(updateOptions);
                                        }
                                    }).catch((goldmineClonerError) => {
                                        if (goldmineClonerError) {

                                        }
                                    });
                            });
                        }
                    });
            }

            execute();
            setInterval(execute, 86400000);
        }, millisTillNextShift);

        return next();
    });

    return next();
};

internals.getPendingApplications = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.sequelize.db.ApplicationOverview.findAll({
            where: {
                completed: false,
                receivedOn: {
                    $ne: null,
                    $gte: Moment.utc().subtract(30, 'days').startOf('day'),
                }
            }
        }).then((results) => {
            internals.applicationOverviwes = results;
            resolve();
        }).catch(function (error) {
            reject(new VError(error, 'error - could not fetch applications'))
        });
    });
}

internals.getUnsecuredScores30Days = function () {
    return new Promise(function (resolve, reject) {
        // return internals.server.plugins.sequelize.db.UnsecuredScore.findAll({
        //     where: {
        //         createdAt: {
        //             $gte: Moment.utc().subtract(15, 'days').startOf('day'),
        //         }
        //     },
        //     order: [['createdAt', 'DESC']]
        // }).then((results) => {
        //     internals.allUnsecuredScores = results;
        //     resolve();
        // }).catch(function (error) {
        //     reject(new VError(error, 'error - could not fetch unsecured scores'))
        // });


        let selectQuery = `SELECT goldmineId, loanTypeId, lenderName, result, requestResponse, customerAccepted, LenderStatusUpdates, createdAt FROM UnsecuredScores 
                           WHERE createdAt >= '${Moment.utc().subtract(30, 'days').startOf('day').format('YYYY-MM-DD')}' 
                           ORDER BY createdAt DESC`;

        return internals.server.plugins.sequelize.db.sequelize.query(selectQuery).then((results) => {            
            internals.allUnsecuredScores = results[0];
            resolve();
        }).catch(function (error) {
            reject(new VError(error, 'error - could not fetch unsecured scores'))
        });
    });
}

internals.getUnsecuredWebRequests = function (applicationId) {
    return new Promise(function (resolve, reject) {
        internals.server.plugins.sequelize.db.UnsecuredWebRequest.findAll({
            where: {
                applicationId: applicationId,
            }
        }).then((unsecuredWebRequests) => {
            internals.applicationWebRequests = unsecuredWebRequests;
            resolve();
        }).catch(function (error) {
            reject(new VError(error, 'error dxi.findRecords()'))
        });
    });
}

internals.getLenderDisplayName = function (lenderName) {
    if (lenderName) {
        let lenderDisplayName;
        if (lenderName.toLowerCase().indexOf('smart-quote') > -1) {
            lenderName = lenderName.replace('smart-quote-', '').replace(/-/g, ' ');

            let splitStr = lenderName.toLowerCase().split(' ');
            for (var i = 0; i < splitStr.length; i++) {
                splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
            }
            lenderDisplayName = splitStr.join(' ');
        } else {
            switch (lenderName.toLowerCase()) {
                case 'amigo':
                    lenderDisplayName = 'Amigo';
                    break;
                case 'amigo-online':
                    lenderDisplayName = 'Amigo';
                    break;
                case 'azure-money':
                    lenderDisplayName = 'Azure Money';
                    break;
                case 'evolution':
                    lenderDisplayName = 'Evolution Money';
                    break;
                case 'guarantor':
                    lenderDisplayName = 'Guarantor My Loan';
                    break;
                case 'everydayloans':
                    lenderDisplayName = 'Everyday Loans';
                    break;
                case 'shawbrook-online':
                    lenderDisplayName = 'Shawbrook Bank';
                    break;
                case 'mcb':
                    lenderDisplayName = 'My Community Finance';
                    break;
                case 'firststop-group':
                    lenderDisplayName = 'First Stop Personal Loans';
                    break;
                case 'likelyloans':
                    lenderDisplayName = 'Likely Loans';
                    break;
                case 'lendable':
                    lenderDisplayName = 'Lendable';
                    break;
                case 'lendingworks':
                case 'lendingworks-online':
                    lenderDisplayName = 'Lending Works';
                    break;
                case 'lendingworks-online':
                    lenderDisplayName = 'Lending Works';
                    break;
                case 'bamboo':
                    lenderDisplayName = 'Bamboo';
                    break;
                case 'progressive':
                    lenderDisplayName = 'Progressive';
                    break;
                case 'hitachi':
                    lenderDisplayName = 'Hitachi Capital Finance';
                    break;
                case 'besavvi':
                    lenderDisplayName = 'Besavvi';
                    break;
                case 'zopa':
                    lenderDisplayName = 'Zopa';
                    break;
                case '1plus1loans':
                    lenderDisplayName = '1Plus1 Loans';
                    break;
                case 'leap-lending':
                    lenderDisplayName = 'Leap Lending';
                    break;
                case 'buddy-loans':
                    lenderDisplayName = 'Buddy Loans';
                    break;
                case 'livelend':
                    lenderDisplayName = 'LiveLend';
                    break;
                case 'betterborrow':
                    lenderDisplayName = 'BetterBorrow';
                    break;
                case 'koyo-loans':
                    lenderDisplayName = 'Koyo Loans';
                    break;
                case 'oplo':
                    lenderDisplayName = 'Oplo';
                    break;
                case 'tm-advances':
                    lenderDisplayName = 'TM Advances';
                    break;
                case '118118-money':
                    lenderDisplayName = '118118 Money';
                    break;
                case 'rac':
                    lenderDisplayName = 'RAC';
                    break;
                case 'salad-money':
                    lenderDisplayName = 'Salad Money';
                    break;
                case 'fintern':
                    lenderDisplayName = 'Fintern';
                    break;
                default:
                    lenderDisplayName = lenderName;
                    break;

            }
        }
        return lenderDisplayName;
    }
    return lenderName;
};

exports.register.attributes = {
    name: 'update-applications-overviews',
};
