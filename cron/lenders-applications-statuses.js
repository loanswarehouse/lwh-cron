'use strict';

const Promise = require('bluebird');
const Joi = require('joi');
const VError = require('verror');
const Moment = require('moment-timezone');
const Request = require('request');
const MailComposer = require('mailcomposer');
const XMLParser = require('fast-xml-parser');

const internals = {};

exports.register = function (server, options, next) {
    internals.server = server;
    internals.options = options;

    server.dependency(['goldmine', 'sequelize'], function (server, next) {
        let runningCron = false;

        function execute() {
            processApplications();
        }

        function processApplications() {
            internals.getAmigoApplications()
                .then(internals.getAcceptedScores)
                .then(() => {
                    if (internals.acceptedScores) {
                        const timezone = 'Europe/London';

                        internals.acceptedScores.map((unsecuredScore) => {
                            let lenderRequest;
                            if (unsecuredScore.lenderRequest) {
                                try {
                                    lenderRequest = JSON.parse(unsecuredScore.lenderRequest);
                                } catch (e) {
                                    lenderRequest = XMLParser.parse(unsecuredScore.lenderRequest);
                                }
                            } else {
                                lenderRequest = {};
                            }

                            let lenderResponse;
                            if (unsecuredScore.lenderResponse) {
                                try {
                                    lenderResponse = JSON.parse(unsecuredScore.lenderResponse);
                                } catch (e) {
                                    lenderResponse = XMLParser.parse(unsecuredScore.lenderResponse);
                                }
                            } else {
                                lenderResponse = {};
                            }

                            let lenderStatusUpdates = [];
                            if (unsecuredScore.lenderStatusUpdates) {
                                lenderStatusUpdates = JSON.parse(unsecuredScore.lenderStatusUpdates);
                            }
                            let lenderReference;
                            let trackingReference;
                            let lwhReference;
                            let firstName = '';
                            let lastName = '';
                            let loanAmount;
                            let skipRequest = false;
                            let createCompletionTask = false;
                            let updateGM = true;
                            if (unsecuredScore.lenderName === 'amigo-online' && lenderRequest.request && lenderRequest.request.Borrower && lenderRequest.request.Borrower.Personal) {
                                try {
                                    firstName = lenderRequest.request.Borrower.Personal.Firstname;
                                    lastName = lenderRequest.request.Borrower.Personal.Surname;
                                    lenderReference = lenderResponse.AgreementNumber;
                                    trackingReference = lenderResponse.AgreementNumber;
                                    if (lenderResponse.LeadResultStatus && lenderResponse.LeadResultStatus !== 4) {
                                        skipRequest = true;
                                    }
                                    if (lenderStatusUpdates.length > 0 && (lenderStatusUpdates[lenderStatusUpdates.length - 1].toLowerCase().trim() === 'paid out' || lenderStatusUpdates[lenderStatusUpdates.length - 1].toLowerCase().trim() === 'cancelled application')) {
                                        skipRequest = true;
                                    }
                                    if (trackingReference && skipRequest === false && internals.amigoApplications && internals.amigoApplications.length > 0) {
                                        let amigoApp = internals.amigoApplications.find((app) => {
                                            return app.AgreementNumber === trackingReference;
                                        });
                                        if (amigoApp) {
                                            loanAmount = amigoApp.LoanAmount;

                                            let newStatus = '';
                                            let newGMStatus = '';
                                            let smsActions = [];
                                            if (amigoApp.ApplicationStatus && amigoApp.ApplicationStatus.toLowerCase().trim() !== 'active application' && amigoApp.ApplicationStatus.toLowerCase().trim() !== 'loan paid out') {
                                                newStatus = amigoApp.ApplicationStatus;
                                            } else {
                                                if (amigoApp.PaidOut && amigoApp.PaidOut === true) {
                                                    newStatus = 'Paid Out';
                                                    createCompletionTask = true;
                                                } else if (amigoApp.GTEsig && amigoApp.GTEsig === true) {
                                                    newStatus = 'Guarantor E-Signed';
                                                } else if (amigoApp.AppEsig && amigoApp.AppEsig === true) {
                                                    newStatus = 'App E-Signed';
                                                    newGMStatus = 'SIG';
                                                    if (newStatus.toLowerCase().trim() !== lenderStatusUpdates[lenderStatusUpdates.length - 1].toLowerCase().trim() && unsecuredScore.goldmineId && parseInt(unsecuredScore.goldmineId) > 0) {
                                                        try {
                                                            smsActions = smsActions.concat(internals.convertActionsToUtc([{
                                                                goldmineId: unsecuredScore.goldmineId,
                                                                type: 'cmdSendText',
                                                                meta: JSON.stringify({
                                                                    templateId: '88',
                                                                    personalLoanOnlineJourney: true,
                                                                    statusUpdateMessage: true,
                                                                }),
                                                                execution_localtime: Moment(),
                                                            }], timezone));
                                                        } catch (e) { }
                                                    }
                                                } else if (amigoApp.ApplicationStatus) {
                                                    newStatus = amigoApp.ApplicationStatus;
                                                }
                                                if (smsActions.length > 0) {
                                                    internals.server.plugins.sequelize.db.Action.bulkCreate(smsActions)
                                                        .catch((error) => {
                                                        });
                                                }

                                                // if (amigoApp.CaselistBucket && amigoApp.CaselistBucket.toLowerCase().trim() === 'vc ready') {
                                                //     newStatus += ' - VC Ready';
                                                // }
                                            }

                                            if (newStatus.length > 0) {
                                                internals.server.plugins.goldmine.getRecordSummary(unsecuredScore.goldmineId)
                                                    .then((goldmineRecord) => {
                                                        if (!goldmineRecord) {
                                                            updateGM = false;
                                                        }
                                                        internals.updateLenderStatusAndFireEmailAlert(unsecuredScore, lenderReference, lenderStatusUpdates, firstName, lastName, newStatus, loanAmount, goldmineRecord.source, newGMStatus, goldmineRecord.status, goldmineRecord.accountNo, createCompletionTask, updateGM);
                                                    }).catch((goldmineRecordError) => {
                                                    });
                                            }
                                        }
                                    }
                                } catch (e) {
                                }
                            } else {
                                if (unsecuredScore.lenderName === 'mcb') {
                                    firstName = lenderRequest.user['first_name'];
                                    lastName = lenderRequest.user['last_name'];
                                    loanAmount = lenderRequest.loan['amount'];
                                    lenderReference = lenderResponse.loanId;
                                    trackingReference = lenderResponse.loanId;
                                    if (lenderStatusUpdates.length > 0 && lenderStatusUpdates[lenderStatusUpdates.length - 1].toLowerCase().trim() === 'disbursement complete') {
                                        skipRequest = true;
                                    }
                                } else if (unsecuredScore.lenderName === 'shawbrook-online') {
                                    firstName = lenderRequest.applicants[0]['forename'];
                                    lastName = lenderRequest.applicants[0]['surname'];
                                    loanAmount = lenderRequest.loanAmount;
                                    lenderReference = (lenderResponse.Financial && lenderResponse.Financial['AgreementReference'] ? lenderResponse.Financial['AgreementReference'].toString() : lenderResponse.Decision['ShawbrookTrackingId'].toString());
                                    trackingReference = lenderResponse.Decision['ShawbrookTrackingId'].toString();
                                    lwhReference = lenderResponse.PartnerInfo['PartnerTrackingId'].toString();
                                    if (lenderStatusUpdates.length > 0 && lenderStatusUpdates[lenderStatusUpdates.length - 1].toLowerCase().trim() === 'paidout') {
                                        skipRequest = true;
                                    }
                                } else if (unsecuredScore.lenderName === 'rac') {
                                    firstName = lenderRequest.applicants[0]['forename'];
                                    lastName = lenderRequest.applicants[0]['surname'];
                                    loanAmount = lenderRequest.loanAmount;
                                    lenderReference = (lenderResponse.Financial && lenderResponse.Financial['AgreementReference'] ? lenderResponse.Financial['AgreementReference'].toString() : lenderResponse.Decision['ShawbrookTrackingId'].toString());
                                    trackingReference = lenderResponse.Decision['ShawbrookTrackingId'].toString();
                                    lwhReference = lenderResponse.PartnerInfo['PartnerTrackingId'].toString();
                                    if (lenderStatusUpdates.length > 0 && lenderStatusUpdates[lenderStatusUpdates.length - 1].toLowerCase().trim() === 'paidout') {
                                        skipRequest = true;
                                    }
                                } else if (unsecuredScore.lenderName === 'hitachi' && unsecuredScore.meta && !isNaN(parseInt(unsecuredScore.meta))) {
                                    try {
                                        lenderReference = parseInt(unsecuredScore.meta).toString();
                                        trackingReference = lenderReference;
                                        if (lenderRequest['SOAP-ENV:Envelope'] && lenderRequest['SOAP-ENV:Envelope']['SOAP-ENV:Body'] && lenderRequest['SOAP-ENV:Envelope']['SOAP-ENV:Body']['createQuoteRequest']) {
                                            firstName = lenderRequest['SOAP-ENV:Envelope']['SOAP-ENV:Body']['createQuoteRequest']['applicant']['forename'];
                                            lastName = lenderRequest['SOAP-ENV:Envelope']['SOAP-ENV:Body']['createQuoteRequest']['applicant']['surname'];
                                            if (lenderRequest['SOAP-ENV:Envelope']['SOAP-ENV:Body']['createQuoteRequest']['loanAmount']) {
                                                loanAmount = lenderRequest['SOAP-ENV:Envelope']['SOAP-ENV:Body']['createQuoteRequest']['loanAmount'];
                                            }
                                        }
                                        if (lenderReference === '' || (lenderStatusUpdates.length > 0 && lenderStatusUpdates[lenderStatusUpdates.length - 1].toUpperCase().trim() === 'COMPLETED')) {
                                            skipRequest = true;
                                        }
                                    } catch (e) {
                                    }
                                } else if (unsecuredScore.lenderName === 'besavvi' && unsecuredScore.meta && !isNaN(parseInt(unsecuredScore.meta))) {
                                    try {
                                        lenderReference = parseInt(unsecuredScore.meta).toString();
                                        trackingReference = lenderReference;
                                        if (lenderRequest['SOAP-ENV:Envelope'] && lenderRequest['SOAP-ENV:Envelope']['SOAP-ENV:Body'] && lenderRequest['SOAP-ENV:Envelope']['SOAP-ENV:Body']['createQuoteRequest']) {
                                            firstName = lenderRequest['SOAP-ENV:Envelope']['SOAP-ENV:Body']['createQuoteRequest']['applicant']['forename'];
                                            lastName = lenderRequest['SOAP-ENV:Envelope']['SOAP-ENV:Body']['createQuoteRequest']['applicant']['surname'];
                                            if (lenderRequest['SOAP-ENV:Envelope']['SOAP-ENV:Body']['createQuoteRequest']['loanAmount']) {
                                                loanAmount = lenderRequest['SOAP-ENV:Envelope']['SOAP-ENV:Body']['createQuoteRequest']['loanAmount'];
                                            }
                                        }
                                        if (lenderReference === '' || (lenderStatusUpdates.length > 0 && lenderStatusUpdates[lenderStatusUpdates.length - 1].toUpperCase().trim() === 'COMPLETED')) {
                                            skipRequest = true;
                                        }
                                    } catch (e) {
                                    }
                                }

                                // Add this check temporarily until all functions are moved to the new API.
                                let baseURL;
                                if (unsecuredScore.lenderName === 'rac') {
                                    baseURL = 'https://lendingapi.loanswarehouse.co.uk';
                                } else {
                                    baseURL = 'http://192.168.1.24:3000';
                                }

                                if (lenderReference && trackingReference && skipRequest === false) {
                                    Request({
                                        method: 'GET',
                                        url: baseURL + '/unsecured-scores/' + trackingReference + (lwhReference ? '|' + lwhReference : '') + '/actions/application-status-' + unsecuredScore.lenderName,
                                    }, function (error, response, body) {
                                        if (body && body.toString().toLowerCase().indexOf('error') === -1) { // body will contain the application's status
                                            internals.server.plugins.goldmine.getRecordSummary(unsecuredScore.goldmineId)
                                                .then((goldmineRecord) => {
                                                    let newGMStatus = '';
                                                    if (goldmineRecord) {
                                                        if (unsecuredScore.lenderName === 'mcb') {
                                                            if (body.toLowerCase().trim() === 'signed agreement') {
                                                                newGMStatus = 'SIG';
                                                            }
                                                            if (body.toLowerCase().trim() === 'disbursement declined') {
                                                                newGMStatus = 'REJ';
                                                            }
                                                            if (body.toLowerCase().trim() === 'cancelled') {
                                                                newGMStatus = 'TUD';
                                                            }
                                                            if (body.toLowerCase().trim() === 'disbursement complete') {
                                                                createCompletionTask = true;
                                                            }
                                                        } else if (unsecuredScore.lenderName === 'shawbrook-online') {
                                                            // updateGM = false;
                                                            if (body.toLowerCase().trim() === 'edocssigned') {
                                                                newGMStatus = 'SIG';
                                                            }
                                                            if (body.toLowerCase().trim() === 'declined') {
                                                                newGMStatus = 'REJ';
                                                            }
                                                            if (body.toLowerCase().trim() === 'cancelled') {
                                                                newGMStatus = 'TUD';
                                                            }
                                                            if (body.toLowerCase().trim() === 'paidout') {
                                                                createCompletionTask = true;
                                                            }
                                                        } else if (unsecuredScore.lenderName === 'rac') {
                                                            // updateGM = false;
                                                            if (body.toLowerCase().trim() === 'edocssigned') {
                                                                newGMStatus = 'SIG';
                                                            }
                                                            if (body.toLowerCase().trim() === 'declined') {
                                                                newGMStatus = 'REJ';
                                                            }
                                                            if (body.toLowerCase().trim() === 'cancelled') {
                                                                newGMStatus = 'TUD';
                                                            }
                                                            if (body.toLowerCase().trim() === 'paidout') {
                                                                createCompletionTask = true;
                                                            }
                                                        } else if (unsecuredScore.lenderName === 'hitachi') {
                                                            if (body.toLowerCase().trim() === 'completed' || body.toLowerCase().trim() === 'offered') {
                                                                newGMStatus = 'SIG';
                                                            }
                                                        } else if (unsecuredScore.lenderName === 'besavvi') {
                                                            if (body.toLowerCase().trim() === 'completed' || body.toLowerCase().trim() === 'offered') {
                                                                newGMStatus = 'SIG';
                                                            }
                                                        }
                                                    } else {
                                                        updateGM = false;
                                                    }
                                                    internals.updateLenderStatusAndFireEmailAlert(unsecuredScore, lenderReference, lenderStatusUpdates, firstName, lastName, body, loanAmount, goldmineRecord.source, newGMStatus, goldmineRecord.status, goldmineRecord.accountNo, createCompletionTask, updateGM);
                                                }).catch((goldmineRecordError) => {
                                                });
                                        }
                                    });
                                }
                            }
                        });
                    }
                })
                .finally(() => {
                    runningCron = false;
                });
        }

        execute();
        setInterval(function () {
            if (runningCron === false) {
                runningCron = true;
                execute();
            }
        }, 300000); // 5 mins
        return next();
    });

    return next();
};

internals.getAcceptedScores = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.sequelize.db.UnsecuredScore.findAll({
            where: {
                goldmineId: {
                    $gt: 0,
                },
                customerAccepted: {
                    $ne: null,
                    $gte: Moment.utc().subtract(30, 'days').startOf('day'),
                }
            },
            order: 'customerAccepted'
        }).then((results) => {
            internals.acceptedScores = results;
            resolve();
        }).catch(function (error) {
            reject(new VError(error, 'error getAcceptedScores'))
        });
    });
};


internals.getAmigoApplications = function () {
    return new Promise(function (resolve, reject) {
        Request({
            method: 'POST',
            url: 'http://192.168.1.24:3000/unsecured-scores/0/actions/application-status-amigo-online',
            body: JSON.stringify({
                fromDate: Moment.utc().subtract(14, 'days').format('YYYY-MM-DD'),
                toDate: Moment.utc().add(1, 'day').format('YYYY-MM-DD'),
            }),
        }, function (error, response, body) {
            if (body) {
                try {
                    internals.amigoApplications = JSON.parse(body);
                } catch (e) {
                    internals.amigoApplications = [];
                }
            }
            return resolve();
        });
    }).catch((error) => {
        internals.amigoApplications = [];
        return resolve();
    });
};

internals.getLenderDisplayName = function (lenderName) {
    if (lenderName) {
        let lenderDisplayName;
        switch (lenderName.toLowerCase()) {
            case 'amigo':
                lenderDisplayName = 'Amigo';
                break;
            case 'amigo-online':
                lenderDisplayName = 'Amigo';
                break;
            case 'azure-money':
                lenderDisplayName = 'Azure Money';
                break;
            case 'evolution':
                lenderDisplayName = 'Evolution Money';
                break;
            case 'guarantor':
                lenderDisplayName = 'Guarantor My Loan';
                break;
            case 'everydayloans':
                lenderDisplayName = 'Everyday Loans';
                break;
            case 'shawbrook-online':
                lenderDisplayName = 'Shawbrook Bank';
                break;
            case 'mcb':
                lenderDisplayName = 'My Community Finance';
                break;
            case 'firststop-group':
                lenderDisplayName = '1st Stop Personal Loans';
                break;
            case 'likelyloans':
                lenderDisplayName = 'Likely Loans';
                break;
            case 'lendable':
                lenderDisplayName = 'Lendable';
                break;
            case 'lendingworks':
            case 'lendingworks-online':
                lenderDisplayName = 'Lending Works';
                break;
            case 'bamboo':
                lenderDisplayName = 'Bamboo';
                break;
            case 'progressive':
                lenderDisplayName = 'Progressive';
                break;
            case 'hitachi':
                lenderDisplayName = 'Hitachi Capital Finance';
                break;
            case 'besavvi':
                lenderDisplayName = 'Besavvi';
                break;
            case 'zopa':
                lenderDisplayName = 'Zopa';
                break;
            case 'leap-lending':
                lenderDisplayName = 'Leap Lending';
                break;
            case 'george-banco':
                lenderDisplayName = 'George Banco';
                break;
            case 'trust-two':
                lenderDisplayName = 'Trust Two';
                break;
            case 'buddy-loans':
                lenderDisplayName = 'Buddy Loans';
                break;
            case 'livelend':
                lenderDisplayName = 'LiveLend';
                break;
            case 'betterborrow':
                lenderDisplayName = 'BetterBorrow';
                break;
            case 'koyo-loans':
                lenderDisplayName = 'Koyo Loans';
                break;
            case 'oplo':
                lenderDisplayName = 'Oplo';
                break;
            case 'tm-advances':
                lenderDisplayName = 'TM Advances';
                break;
            case '118118-money':
                lenderDisplayName = '118118 Money';
                break;
            case 'rac':
                lenderDisplayName = 'RAC';
                break;
            case 'salad-money':
                lenderDisplayName = 'Salad Money';
                break;
            case 'fintern':
                lenderDisplayName = 'Fintern';
                break;
            default:
                lenderDisplayName = lenderName;
                break;
        }
        return lenderDisplayName;
    }
    return lenderName;
};

internals.updateLenderStatusAndFireEmailAlert = function (unsecuredScore, lenderReference, lenderStatusUpdates, firstName, lastName, newStatus, loanAmount, leadSource, newGMStatus, oldGMStatus, goldmineAccountNo, createCompletionTask, updateGM) {
    let fireStatusUpdateEmail = false;
    const gmStatusesToNotUpdate = ['APP', 'REF', 'FIR', 'SEL', 'SLD', 'MII', 'WIP', 'PPO', 'SLC', 'ULC', 'BLC'];
    if (newStatus.indexOf('<html>') < 0) {
        if (lenderStatusUpdates.length === 0) {
            lenderStatusUpdates.push(newStatus);
            fireStatusUpdateEmail = true;
        } else {
            if (newStatus.toLowerCase().trim() !== lenderStatusUpdates[lenderStatusUpdates.length - 1].toLowerCase().trim()) {
                lenderStatusUpdates.push(newStatus);
                fireStatusUpdateEmail = true;
            }
        }
        unsecuredScore.updateAttributes({
            lenderStatusUpdates: JSON.stringify(lenderStatusUpdates),
        });
    }

    if (fireStatusUpdateEmail === true) {
        function _updateGMStatus() {
            return new Promise(function (resolve, reject) {
                if (updateGM) {

                    try {
                        let monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                        let today = new Date();

                        let strNotes = '*** MASTER (Master) *** ' + today.getDate() + ' ' + monthNames[today.getMonth()] + ' ' + today.getFullYear().toString() + ' ' + (today.getHours().toString().length === 1 ? '0' + today.getHours().toString() : today.getHours().toString()) + ':' + (today.getMinutes().toString().length === 1 ? '0' + today.getMinutes().toString() : today.getMinutes().toString()) + '\r\n';
                        strNotes = strNotes + internals.getLenderDisplayName(unsecuredScore.lenderName) + ' application status changed to ' + lenderStatusUpdates[lenderStatusUpdates.length - 1] + '\r\n';

                        const noteOptions = {
                            id: unsecuredScore.goldmineId.toString(),
                            notes: strNotes.replace(/'/g, '\'\''),
                        };

                        internals.server.plugins.goldmine.appendGoldmineNotes(noteOptions).then(() => {
                        }).catch((error) => {
                        });
                    } catch (e) {
                    }

                    const recid = '_' + new Date().getTime();

                    if (newGMStatus && newGMStatus.length > 0 && oldGMStatus && oldGMStatus.length > 0 && newGMStatus !== oldGMStatus && gmStatusesToNotUpdate.indexOf(oldGMStatus) === -1) {
                        try {
                            // Update lead status in CONTACT1 table (MSSQL database - Goldmine)

                            function _updateGMRecordStatus() {
                                return new Promise(function (resolve, reject) {
                                    const gmOptions = {
                                        id: unsecuredScore.goldmineId,
                                        newStatus: newGMStatus,
                                        notes: '',
                                    }

                                    internals.server.plugins.goldmine.updateRecordStatus(gmOptions)
                                        .then(() => {
                                            resolve();
                                        });
                                });
                            }

                            function _addGMStatusUpdateLog() {
                                return new Promise(function (resolve, reject) {
                                    const gmConthistOptions = {
                                        accountNo: goldmineAccountNo,
                                        oldStatus: oldGMStatus,
                                        newStatus: newGMStatus,
                                        recid: recid,
                                    }

                                    internals.server.plugins.goldmine.addStatusUpdateLog(gmConthistOptions)
                                        .then(() => {
                                            resolve();
                                        });
                                });
                            }

                            function _addUpdateGMStatusUpdateLog() {
                                return new Promise(function (resolve, reject) {
                                    const updateGMLogOptions = {
                                        accountNo: goldmineAccountNo,
                                        oldStatus: oldGMStatus,
                                        newStatus: newGMStatus,
                                    }

                                    internals.server.plugins.updateGM.addStatusUpdateLog(updateGMLogOptions)
                                        .then(() => {
                                            resolve();
                                        });
                                });
                            }

                            function _addUpdateGMStatusUpdateLogDistinct() {
                                return new Promise(function (resolve, reject) {
                                    const updateGMLogOptions = {
                                        accountNo: goldmineAccountNo,
                                        oldStatus: oldGMStatus,
                                        newStatus: newGMStatus,
                                    }

                                    internals.server.plugins.updateGM.addStatusUpdateLogDistinct(updateGMLogOptions)
                                        .then(() => {
                                            resolve();
                                        });
                                });
                            }

                            _updateGMRecordStatus()
                                .then(_addGMStatusUpdateLog)
                                .then(_addUpdateGMStatusUpdateLog)
                                .then(_addUpdateGMStatusUpdateLogDistinct)
                                .then(() => {
                                    /*try {
                                        if (createCompletionTask === true) {
                                            const gmCreateTaskOptions = {
                                                userId: 'MATT',
                                                accountNo: goldmineAccountNo,
                                                customerName: firstName + ' ' + lastName,
                                                reference: 'Personal Loan Completions - ' + internals.getLenderDisplayName(unsecuredScore.lenderName),
                                                recid: recid,
                                            }
                                            internals.server.plugins.goldmine.createNewGMTask(gmCreateTaskOptions)
                                                .catch((error) => {
                                                });
                                        }
                                    } catch (e) {
                                    }*/ 

                                    Request({
                                        method: 'GET',
                                        url: 'http://192.168.1.24/goldmine-records/' + unsecuredScore.goldmineId.toString() + '/status',
                                    }, (error, response, body) => {
                                    });

                                    resolve();
                                })
                                .catch((error) => {
                                    resolve();
                                });
                            // ---------------------------------------------------------------                
                        } catch (e) {
                            resolve();
                        }
                    } else {
                        resolve();
                    }
                } else {
                    resolve();
                }
            }).catch((error) => {
                resolve();
            });
        }

        _updateGMStatus()
            .then(() => {
                return new Promise((resolve, reject) => {
                    try {
                        internals.server.plugins.sequelize.db.LenderStatusUpdate.create({
                            applicationId: unsecuredScore.goldmineId,
                            applicationReference: unsecuredScore.applicationReference || '',
                            partnerName: leadSource,
                            firstName: firstName,
                            lastName: lastName,
                            lenderName: internals.getLenderDisplayName(unsecuredScore.lenderName),
                            lenderReference: lenderReference,
                            loanAmount: parseFloat(loanAmount),
                            lenderPayload: '',
                            lenderStatus: lenderStatusUpdates[lenderStatusUpdates.length - 1],
                            lenderStatusDescription: '',
                        }).then(() => {
                            resolve();
                        }).catch((dbError) => {
                            resolve();
                        })
                    } catch (e) {
                        resolve();
                    }
                });
            })
            .then(() => {
                let sendRawEmailPromise;

                let mail = MailComposer({
                    from: 'Loans Warehouse' + ' <' + 'unsecured@loanswarehouse.co.uk' + '>',
                    replyTo: 'unsecured@loanswarehouse.co.uk',
                    to: 'unsecured@loanswarehouse.co.uk',
                    bcc: 'it@loanswarehouse.co.uk',
                    subject: internals.getLenderDisplayName(unsecuredScore.lenderName) + ' Status Update' + (unsecuredScore.goldmineId > 0 ? ' #' + unsecuredScore.goldmineId : '') + ' - ' + lenderStatusUpdates[lenderStatusUpdates.length - 1] + ' ' + Moment().format('HH.mm DD MMMM'),
                    html: '<b>' + (unsecuredScore.goldmineId > 0 ? '#' + unsecuredScore.goldmineId + ' - ' : '') + firstName + ' ' + lastName + '</b>' + (unsecuredScore.applicationReference && unsecuredScore.applicationReference.length > 0 ? '<br><br><b>Application reference: ' + unsecuredScore.applicationReference + '</b>' : '') + '<br><br><b>' + internals.getLenderDisplayName(unsecuredScore.lenderName) + '</b> application status changed' + (lenderStatusUpdates.length > 1 && 1 === 2 ? ' from <b>' + lenderStatusUpdates[lenderStatusUpdates.length - 2] + '</b>' : '') + ' to <b>' + lenderStatusUpdates[lenderStatusUpdates.length - 1] + ' ' + Moment().format('HH.mm DD MMMM') + '</b><br><br>Lender reference: ' + lenderReference + '</b><br><br>Source: ' + leadSource + '</b><br><br>Proceeded amount: £' + loanAmount,
                    attachments: [],
                });

                return new Promise((resolve, reject) => {
                    mail.keepBcc = true;
                    mail.build((err, message) => {
                        if (err) {
                            reject(`Error sending raw email: ${err}`);
                        }
                        sendRawEmailPromise = internals.server.plugins.aws.ses.sendRawEmail({ RawMessage: { Data: message } }).promise();
                    });

                    resolve(sendRawEmailPromise);
                });
            });
    }
};

internals.convertActionsToUtc = function (actions, timezone) {
    return actions.map((action) => {
        action.execution_localtime = Moment(action.execution_localtime).format('YYYY-MM-DD HH:mm:ss');
        action.execution_timezone = timezone;
        action.execution_utc = Moment.utc(Moment(action.execution_localtime).tz(timezone));
        return action;
    });
};

exports.register.attributes = {
    name: 'lenders-applications-statuses',
};
