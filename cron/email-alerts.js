'use strict';
const CronJob = require('cron').CronJob;
const Moment = require('moment-timezone');
const Async = require('async');
const _ = require('lodash');
const fs = require('fs');
const internals = {};
const Boom = require('boom');

exports.register = function (server, options, next) {
    const securedLeadsNotDroppingInDialler = new CronJob('0 */5 * * * *', function() {
        return server.plugins.goldmine.fetchSecuredRecords()
        .then((gmRecords) => {
            return new Promise((resolve, reject) => {
                if (!gmRecords || gmRecords.length === 0) {
                    return reject(new Error('Goldmine records List is empty.'));
                }
                var goldmineIds = gmRecords.map((record) => record.goldmineId);
                return server.plugins.sequelize.db.Lead.findAll({
                    where: {
                        goldmineId: {
                            $in: goldmineIds
                        }
                    },
                    attributes: ['id', 'goldmineId', 'referral', 'dxiCreated']
                })
                    .then((leadRecords) => {
                        var securedNotDroppingInDialler = _.differenceBy(gmRecords, leadRecords, 'goldmineId');
                        var dxiCreatedFailures = leadRecords.every((leadRecord) => leadRecord.dxiCreated === 0);
                        if (dxiCreatedFailures) {
                            leadRecords.forEach(lead => {
                                var goldmineRecord = gmRecords.find(record => record.goldmineId = lead.goldmineId);
                                if (goldmineRecord) {
                                    securedNotDroppingInDialler.push(goldmineRecord);
                                }
                            });
                        }
                        resolve(securedNotDroppingInDialler);
                    })
                    .catch((error) => reject(error));
            })
        })
        .then((missingLeads) => {
            var readJsonLeadsAlreadyEmailed = fs.readFileSync('securedLeadsEmailed.txt', 'utf8');
            var parsedRecords;
            try {
                parsedRecords = JSON.parse(readJsonLeadsAlreadyEmailed);
            } catch (e) {
                parsedRecords = [];
            }
            const newSecuredLeadsNotDroppingInDialler = _.differenceBy(missingLeads, parsedRecords, 'goldmineId');
            const allAppsHaveApplicationType =  missingLeads.every((currentValue) => (currentValue.applicationType !== null && currentValue.applicationType !== '')) 
            console.log('allAppsHaveApplicationType: ', allAppsHaveApplicationType);
            if (newSecuredLeadsNotDroppingInDialler.length > 0) {
                var userOutput = newSecuredLeadsNotDroppingInDialler.map(record => `${record.goldmineId} - ${record.source || '[No source]'} - creator: <strong>${record.creator}</strong>`).join('<br>');                               
                const sourceAction = userOutput.indexOf('No source') >= 0 ? ' and No source added' : '';
                if (allAppsHaveApplicationType) {
                    server.plugins.aws.ses.sendEmail({
                        Source: 'server@loanswarehouse.co.uk',
                            Destination: { ToAddresses: ['it@loanswarehouse.co.uk', 'mgu@loanswarehouse.co.uk'] },
                            Message: {
                                Subject: {
                                    Data: 'Dialler issue'
                                },
                                Body: {
                                    Html: {
                                        Data: 'The following new lead(s) have not dropped in. Please call manually until the issue can be fixed and we received an automtated succesful email notification. <br><br>' + userOutput
                                    }
                                }
                            }
                    }, function (error, data) {
                        if (error) {}
                        console.log('Failure email notification sent.');
                    });
                } else {
                    server.plugins.aws.ses.sendEmail({
                        Source: 'mt@loanswarehouse.co.uk',
                            Destination: { ToAddresses: ['newbusiness@loanswarehouse.co.uk', 'mgu@loanswarehouse.co.uk'] },
                            Message: {
                                Subject: {
                                    Data: 'Action required: Lead Not triggered' + sourceAction
                                },
                                Body: {
                                    Html: {
                                        Data: 'The following new lead(s) require action. <br><br>' + userOutput
                                    }
                                }
                            }
                    }, function (error, data) {
                        if (error) {}
                        console.log('Failure non-triggered leads email notification sent.');
                    });
                }
                fs.writeFileSync('securedLeadsEmailed.txt', JSON.stringify(parsedRecords.concat(newSecuredLeadsNotDroppingInDialler)));
            } else {
                if (parsedRecords.length > 0 && missingLeads.length === 0) {
                    if (parsedRecords[parsedRecords.length -1].applicationType !== null) {
                        server.plugins.aws.ses.sendEmail({
                            Source: 'it@loanswarehouse.co.uk',
                                Destination: { ToAddresses: ['it@loanswarehouse.co.uk', 'mgu@loanswarehouse.co.uk'] },
                                Message: {
                                    Subject: {
                                        Data: 'Dialler Resolved'
                                    },
                                    Body: {
                                        Html: {
                                            Data: 'The issue has been rectified. Both new and past leads should be in the dialler now.'
                                        }
                                    }
                                }
                        }, function (error, data) {
                            if (error) {}
                            console.log('Successful email notification sent.');
                        });
                        
                        var gmIdsArray = parsedRecords.map(record => record.goldmineId);
                        // Use another .txt file to backup the leads that have not dropped in 
                        fs.writeFileSync('securedLeadsBackup.txt', JSON.stringify(gmIdsArray));
                        // Clear leads once issue has been rectified
                        fs.writeFileSync('securedLeadsEmailed.txt', '');
                    } else {
                       console.log('No issue due to users not triggering leads!'); 
                    }
                } else {
                    console.log('No server issues anymore!');
                }
            }
        })
        .catch((error) => {
            console.log('My errors from any function: ', error);
        })
    }, null, true);

    return next();
}   

exports.register.attributes = {
    name: 'email-alerts',
};