'use strict';
const CronJob = require('cron').CronJob;
const Moment = require('moment-timezone');
const Request = require('request');

exports.register = function (server, options, next) {
    try {
        const job = new CronJob("59 59 22 * * 0-4", function() {
            Request({
                method: 'POST',
                url: 'http://192.168.1.24:3000/leads/actions/run-suspicious-report',
                json: {
                    leadsFrom: Moment().subtract(1, 'month').format('YYYY-MM-DD'),
                }	
            }, function(error, response, body) {});
        });
        job.start();
    }catch(e) {};
    return next();
}


exports.register.attributes = {
    name: 'suspicious-leads-report',
};