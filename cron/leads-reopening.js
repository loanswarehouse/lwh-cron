'use strict';

const Promise = require('bluebird');
const Joi = require('joi');
const VError = require('verror');
const Moment = require('moment-timezone');

const internals = {};

exports.register = function (server, options, next) {
	internals.server = server;

	server.dependency(['goldmine', 'dxi', 'sequelize'], function (server, next) {

		const now = new Date();

		// Move due leads to NOC dataset
		let millisTillNextShift = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 1, 0, 0, 0) - now;
		if (millisTillNextShift < 0) {
			millisTillNextShift += 86400000;
		}

		setTimeout(function () {
			function processLeads() {

				internals.getDueGoldmineRecords()
					.then(() => {
						if (internals.dueGoldmineRecords && internals.dueGoldmineRecords.length > 0) {
							internals.dueGoldmineRecords.map((goldmineRecord) => {
								return internals.server.plugins.sequelize.db.Lead.find({
									where: {
										goldmineId: goldmineRecord.COMPANY,
									},
								})
									.then((dbLead) => {
										if (dbLead && dbLead.dataset && dbLead.dataset.toLowerCase().startsWith('secured')) {
											internals.server.plugins.dxi.moveRecordToDataset(dbLead.dxiId, 72) // Move to NOC dataset
												.catch((error) => {
												});
											internals.server.plugins.dxi.updateOutcome(dbLead.dxiId, '506409', 72) // Reopen record
												.catch((error) => {
												});
										}
									});
							});
						}
					});
			}

			processLeads();
			setInterval(processLeads, 86400000);
		}, millisTillNextShift);

		// -----------------------------------------------------------------------------------------------------


		// Clear NOC dataset
		let scheduleTime = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 22, 0, 0, 0) - now;
		setTimeout(function () {

			function executeNOCs() {
				const currentDate = Moment().tz('Europe/London');

				if (currentDate.day() !== 0 && currentDate.day() !== 6) {
					processNOCLeads();
				}
			}

			function processNOCLeads() {
				internals.getCurrentNOCLeads()
					.then(() => {
						if (internals.currentNocLeads && internals.currentNocLeads.length > 0) {
							internals.currentNocLeads.map((item) => {
								internals.server.plugins.dxi.updateOutcome(item.id, '506410') // Close record
									.catch((error) => {
									});
							});
						}
					});
			}


			executeNOCs();
			setInterval(executeNOCs, 86400000);
		}, scheduleTime);
		// -----------------------------------------------------------------------------------------------------


		return next();
	});

	return next();
};

internals.getDueGoldmineRecords = function () {
	return new Promise(function (resolve, reject) {
		internals.server.plugins.goldmine.getDueNOCs({ dateUpdated: Moment().tz('Europe/London').add(-7, 'days').format('YYYY-MM-DD') })
			.then((results) => {
				internals.dueGoldmineRecords = results;
				resolve();
			})
			.catch(function (error) {
				reject(new VError(error, 'error dxi.findRecords()'))
			});
	});
};

internals.getCurrentNOCLeads = function () {
	return new Promise(function (resolve, reject) {
		return internals.server.plugins.dxi.findRecords({
			dataset: 72,
			'search[ProcessType]': 'NEEDSMOREWORK',
		})
			.then((results) => {
				internals.currentNocLeads = results;
				resolve();
			})
			.catch(function (error) {
				reject(new VError(error, 'error dxi.findRecords()'))
			});
	});
};


exports.register.attributes = {
	name: 'leads-reopening',
};
