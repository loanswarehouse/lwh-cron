'use strict';

const Promise = require('bluebird');
const Joi = require('joi');
const VError = require('verror');
const Moment = require('moment-timezone');
const Request = require('request');

const internals = {};

internals.schema = Joi.object().keys({
    subscribeTo: Joi.string().required(), // SQS Queue URL to pull events from
    publishTo: Joi.object().keys({
        dxiRecordCreated: Joi.string().required(), // SNS Topic ARN to publish events to
    }).required(),
    datasets: Joi.object().keys({
        secured: Joi.number().required().example('5'),
        secured22: Joi.number().required().example('5'),
        secured23: Joi.number().required().example('5'),
        secured24: Joi.number().required().example('5'),
        secured26: Joi.number().required().example('5'),
        secured27: Joi.number().required().example('5'),
        secured28: Joi.number().required().example('5'),
        securedOOH: Joi.number().required().example('5'),
        unsecured: Joi.number().required().example('6'),
        unsecured33: Joi.number().required().example('6'),
        unsecured34: Joi.number().required().example('6'),
        unsecured35: Joi.number().required().example('6'),
    }).required(),
    outcomes: Joi.object().keys({
        open: Joi.number().required().label('outcome code'),
        closed: Joi.number().required().label('outcome code'),
    }).required(),
}).required();

exports.register = function (server, options, next) {
    Joi.assert(options, internals.schema, 'Invalid options');

    internals.server = server;
    internals.options = options;

    server.dependency(['goldmine', 'dxi', 'sequelize'], function (server, next) {
        function execute() {
            processLeads();
        }

        function processLeads() {
            internals.getRecordsFromWebPersonalLoansDiallerNewProspects()
                .then(internals.getRecordsFromWebPersonalLoansDiallerNeedsMoreWork)
                .then(internals.getRecordsFromWebPersonalLoansChasingHomeownersDiallerNewProspects)
                .then(internals.getRecordsFromWebPersonalLoansChasingHomeownersDiallerNeedsMoreWork)
                .then(internals.getRecordsFromWebPersonalLoansChasingTenantsDiallerNewProspects)
                .then(internals.getRecordsFromWebPersonalLoansChasingTenantsDiallerNeedsMoreWork)
                .then(internals.getRecordsFromWebPersonalLoansAbandonsDiallerNewProspects)
                .then(internals.getRecordsFromWebPersonalLoansAbandonsDiallerNeedsMoreWork)
                .then(() => {
                    let monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                    let today = new Date();

                    let arrLeads = [];
                    if (internals.webPersonalLeadsNewProspects && internals.webPersonalLeadsNewProspects.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsNewProspects);
                    }

                    if (internals.webPersonalLeadsNeedsMoreWork && internals.webPersonalLeadsNeedsMoreWork.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsNeedsMoreWork);
                    }

                    if (internals.webPersonalLeadsChasingHomeownersNewProspects && internals.webPersonalLeadsChasingHomeownersNewProspects.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsChasingHomeownersNewProspects);
                    }

                    if (internals.webPersonalLeadsChasingHomeownersNeedsMoreWork && internals.webPersonalLeadsChasingHomeownersNeedsMoreWork.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsChasingHomeownersNeedsMoreWork);
                    }

                    if (internals.webPersonalLeadsChasingTenantsNewProspects && internals.webPersonalLeadsChasingTenantsNewProspects.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsChasingTenantsNewProspects);
                    }

                    if (internals.webPersonalLeadsChasingTenantsNeedsMoreWork && internals.webPersonalLeadsChasingTenantsNeedsMoreWork.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsChasingTenantsNeedsMoreWork);
                    }

                    if (internals.webPersonalLeadsAbandonsNewProspects && internals.webPersonalLeadsAbandonsNewProspects.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsAbandonsNewProspects);
                    }

                    if (internals.webPersonalLeadsAbandonsNeedsMoreWork && internals.webPersonalLeadsAbandonsNeedsMoreWork.length > 0) {
                        arrLeads = arrLeads.concat(internals.webPersonalLeadsAbandonsNeedsMoreWork);
                    }

                    if (arrLeads.length > 0) {
                        arrLeads.sort((a, b) => (parseInt(a.id) > parseInt(b.id)) ? -1 : 1);
                        let timeout = 500;
                        arrLeads.map((lead) => {
                            if (lead.goldmine) {
                                setTimeout(function () {
                                    return internals.server.plugins.sequelize.db.Lead.find({
                                        where: {
                                            goldmineId: lead.goldmine,
                                        },
                                    })
                                        .then((dbLead) => {
                                            //See if customer has already checked eligibility
                                            if (dbLead && dbLead.goldmineId) {
                                                return internals.server.plugins.sequelize.db.UnsecuredScore.findAll({
                                                    where: {
                                                        goldmineId: dbLead.goldmineId,
                                                    },
                                                }).then((unsecuredScores) => {
                                                    let dateReceived = Moment(dbLead.createdAt);

                                                    let gmSnapshot = JSON.parse(dbLead.snapshot);

                                                    let minutesCheck = 2;

                                                    if (unsecuredScores && unsecuredScores.length > 0) {
                                                        if (Moment().diff(Moment(unsecuredScores[0].createdAt), 'minutes') >= minutesCheck) {
                                                            unsecuredScores = unsecuredScores.filter((score) => {
                                                                return score.lenderName !== 'azure-money';
                                                            });
                                                            let arrLenders = [
                                                                {
                                                                    lenderDisplayName: internals.getLenderDisplayName('hitachi'),
                                                                    lenderName: 'hitachi',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                }, /*{
                                                                lenderDisplayName: internals.getLenderDisplayName('besavvi'),
                                                                lenderName: 'besavvi',
                                                                loanType: 'Personal Loan',
                                                                rateGuaranteed: true,
                                                            },*/ {
                                                                    lenderDisplayName: internals.getLenderDisplayName('zopa'),
                                                                    lenderName: 'zopa',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('lendingworks-online'),
                                                                    lenderName: 'lendingworks-online',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('shawbrook-online'),
                                                                    lenderName: 'shawbrook-online',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('lendable'),
                                                                    lenderName: 'lendable',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('mcb'),
                                                                    lenderName: 'mcb',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('likelyloans'),
                                                                    lenderName: 'likelyloans',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('evolution'),
                                                                    lenderName: 'evolution',
                                                                    loanType: 'Homeowner Loan',
                                                                    rateGuaranteed: false,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('everydayloans'),
                                                                    lenderName: 'everydayloans',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('firststop-group'),
                                                                    lenderName: 'firststop-group',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('bamboo') + ' Personal Loans',
                                                                    lenderName: 'bamboo',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('bamboo') + ' Guarantor Loans',
                                                                    lenderName: 'bamboo',
                                                                    loanType: 'Guarantor Loan',
                                                                    rateGuaranteed: true,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('guarantor'),
                                                                    lenderName: 'guarantor',
                                                                    loanType: 'Guarantor Loan',
                                                                    rateGuaranteed: false,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('1plus1loans'),
                                                                    lenderName: '1plus1loans',
                                                                    loanType: 'Guarantor Loan',
                                                                    rateGuaranteed: false,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('azure-money'),
                                                                    lenderName: 'azure-money',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                }, /* {
                                                                lenderDisplayName: internals.getLenderDisplayName('amigo-online'),
                                                                lenderName: 'amigo-online',
                                                                loanType: 'Guarantor Loan',
                                                                rateGuaranteed: true,
                                                            }, */ {
                                                                    lenderDisplayName: internals.getLenderDisplayName('leap-lending'),
                                                                    lenderName: 'leap-lending',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('george-banco'),
                                                                    lenderName: 'george-banco',
                                                                    loanType: 'Guarantor Loan',
                                                                    rateGuaranteed: true,
                                                                }, {
                                                                    lenderDisplayName: internals.getLenderDisplayName('trust-two'),
                                                                    lenderName: 'trust-two',
                                                                    loanType: 'Guarantor Loan',
                                                                    rateGuaranteed: true,
                                                                },
                                                                {
                                                                    lenderDisplayName: internals.getLenderDisplayName('buddy-loans'),
                                                                    lenderName: 'buddy-loans',
                                                                    loanType: 'Guarantor Loan',
                                                                    rateGuaranteed: true,
                                                                },
                                                                {
                                                                    lenderDisplayName: internals.getLenderDisplayName('livelend'),
                                                                    lenderName: 'livelend',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                },
                                                                {
                                                                    lenderDisplayName: internals.getLenderDisplayName('betterborrow'),
                                                                    lenderName: 'betterborrow',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                },
                                                                {
                                                                    lenderDisplayName: internals.getLenderDisplayName('koyo-loans'),
                                                                    lenderName: 'koyo-loans',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                },
                                                                {
                                                                    lenderDisplayName: internals.getLenderDisplayName('oplo'),
                                                                    lenderName: 'oplo',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                },
                                                                {
                                                                    lenderDisplayName: internals.getLenderDisplayName('tm-advances'),
                                                                    lenderName: 'tm-advances',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                },
                                                                {
                                                                    lenderDisplayName: internals.getLenderDisplayName('118118-money'),
                                                                    lenderName: '118118-money',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                },
                                                                {
                                                                    lenderDisplayName: internals.getLenderDisplayName('rac'),
                                                                    lenderName: 'rac',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: true,
                                                                },
                                                                {
                                                                    lenderDisplayName: internals.getLenderDisplayName('salad-money'),
                                                                    lenderName: 'salad-money',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: false,
                                                                },
                                                                {
                                                                    lenderDisplayName: internals.getLenderDisplayName('fintern'),
                                                                    lenderName: 'fintern',
                                                                    loanType: 'Personal Loan',
                                                                    rateGuaranteed: false,
                                                                }
                                                            ];

                                                            try {
                                                                if (dbLead && dbLead.referral.toLowerCase().trim() === 'guarantor my loan') {
                                                                    arrLenders = arrLenders.filter((lender) => {
                                                                        return (lender.loanType.toLowerCase().trim() !== 'guarantor loan');
                                                                    });
                                                                }
                                                            } catch (e) {
                                                            }

                                                            try {
                                                                // Remove otherGMLLenders (gml, 1plus1loans) from arrLenders when no score was found in unsecuredScores table
                                                                const guarantorMyLoanScore = unsecuredScores.find(score => score.lenderName === 'guarantor');
                                                                if (!guarantorMyLoanScore) {
                                                                    const guarantorMyLaonIndex = arrLenders.findIndex(item => item.lenderName === 'guarantor');
                                                                    arrLenders.splice(guarantorMyLaonIndex, 1);
                                                                }
                                                                const _1plus1loansScore = unsecuredScores.find(score => score.lenderName === '1plus1loans');
                                                                if (!_1plus1loansScore) {
                                                                    const _1plus1loansIndex = arrLenders.findIndex(item => item.lenderName === '1plus1loans');
                                                                    arrLenders.splice(_1plus1loansIndex, 1);
                                                                }
                                                            } catch (e) {

                                                            }

                                                            arrLenders.map((lender) => {
                                                                let score;
                                                                if (lender.lenderName === 'bamboo') {
                                                                    let bambooScores = unsecuredScores.filter((score) => {
                                                                        return score.lenderName === lender.lenderName;
                                                                    });
                                                                    if (bambooScores && bambooScores.length > 0) {
                                                                        score = bambooScores.find((score) => {
                                                                            let bambooRequestResponse = JSON.parse(score.requestResponse);
                                                                            if (bambooRequestResponse) {
                                                                                return ((bambooRequestResponse.loanType === 'GUA' && lender.loanType.toLowerCase() === 'guarantor loan') || (bambooRequestResponse.loanType === 'UNS' && lender.loanType.toLowerCase() === 'personal loan'));
                                                                            }
                                                                            return false;
                                                                        });
                                                                    }
                                                                } else {
                                                                    score = unsecuredScores.find((score) => {
                                                                        return score.lenderName === lender.lenderName;
                                                                    });
                                                                }

                                                                if (score) {
                                                                    if (!score.result && !score.lenderResponse) {
                                                                        lender.result = { "result": "declined" };
                                                                    } else {
                                                                        if (score.result && (score.result === 'error' || score.result === 'declined')) {
                                                                            lender.result = { "result": "declined" };
                                                                        } else if (score.result && score.result === 'skipped') {
                                                                            lender.result = {
                                                                                "result": "skip",
                                                                            };
                                                                        } else if (score.result && (score.result === 'accepted' || score.result === 'warning') && score.requestResponse) {
                                                                            let lenderResponse = JSON.parse(score.requestResponse);
                                                                            delete lenderResponse.message;
                                                                            delete lenderResponse.applicationUrl;
                                                                            delete lenderResponse.loanType;
                                                                            lender.result = lenderResponse;
                                                                        }
                                                                    }
                                                                } else {
                                                                    lender.result = { "result": "declined" };
                                                                }
                                                                if (!lender.result) {
                                                                    lender.result = { "result": "declined" };
                                                                }
                                                            });

                                                            let smartQuoteScore = unsecuredScores.find((score) => {
                                                                return (score.lenderName.indexOf('smart-quote') > -1 && score.result === 'accepted' && score.requestResponse);
                                                            });
                                                            if (smartQuoteScore) {
                                                                let smartQuoteRequestResponse = JSON.parse(smartQuoteScore.requestResponse);
                                                                arrLenders.push({
                                                                    lenderDisplayName: (smartQuoteRequestResponse.lenderName ? smartQuoteRequestResponse.lenderName : internals.getLenderDisplayName(smartQuoteScore.lenderName)),
                                                                    lenderName: smartQuoteScore.lenderName,
                                                                    loanType: 'Homeowner Loan',
                                                                    rateGuaranteed: false,
                                                                    result: smartQuoteRequestResponse
                                                                });
                                                            }



                                                            let carbonScore = unsecuredScores.find((score) => {
                                                                return (score.lenderName.indexOf('carbon-finance') > -1 && score.result === 'accepted' && score.requestResponse);
                                                            });
                                                            if (carbonScore) {
                                                                let carbonRequestResponse = JSON.parse(carbonScore.requestResponse);
                                                                arrLenders.push({
                                                                    lenderDisplayName: carbonRequestResponse.lenderName,
                                                                    lenderName: 'carbon-finance',
                                                                    loanType: carbonRequestResponse.loanTypeDescription,
                                                                    rateGuaranteed: false,
                                                                    result: carbonRequestResponse
                                                                });
                                                            }





                                                            if (dbLead && dbLead.referral.toLowerCase().trim() === 'my community finance') {
                                                                arrLenders = arrLenders.filter((lender) => {
                                                                    if (lender.result.result === 'accepted' || lender.result.result === 'warning') {
                                                                        if (parseFloat(lender.result.apr) > 69.9) {
                                                                            return false;
                                                                        }
                                                                    }
                                                                    return (lender.lenderName !== 'mcb' && lender.loanType.toLowerCase().trim() !== 'guarantor loan');
                                                                });
                                                            }

                                                            if (dbLead && (dbLead.referral.toLowerCase().trim() === 'bikbbi' || dbLead.referral.toLowerCase().trim() === 'resi')) {
                                                                arrLenders = arrLenders.filter((lender) => {
                                                                    if (lender.result.result === 'accepted' || lender.result.result === 'warning') {
                                                                        if (parseFloat(lender.result.apr) > 49.9) {
                                                                            return false;
                                                                        }
                                                                    }
                                                                });
                                                            }

                                                            if (dbLead && dbLead.referral.toLowerCase().trim() === 'protect my install') {
                                                                arrLenders = arrLenders.filter((lender) => {
                                                                    if (lender.result.result === 'accepted' || lender.result.result === 'warning') {
                                                                        if (parseFloat(lender.result.apr) > 29.9) {
                                                                            return false;
                                                                        }
                                                                    }
                                                                });
                                                            }

                                                            let guaranteedScores = [];
                                                            let acceptedScores = [];
                                                            let otherScores = [];
                                                            let finalScores = [];

                                                            guaranteedScores = arrLenders.filter((lender) => {
                                                                return ((lender.result.result === 'accepted' || lender.result.result === 'warning') && lender.result.guaranteed === true && lender.loanType.toString().toLowerCase() !== 'guarantor loan' && parseFloat(lender.result.loanAmount) === parseFloat(gmSnapshot.loanAmount));
                                                            });


                                                            acceptedScores = arrLenders.filter((lender) => {
                                                                return ((lender.result.result === 'accepted' || lender.result.result === 'warning') && (!lender.result.guaranteed || lender.result.guaranteed === false || (lender.result.guaranteed === true && parseFloat(lender.result.loanAmount) !== parseFloat(gmSnapshot.loanAmount))));
                                                            });


                                                            otherScores = arrLenders.filter((lender) => {
                                                                return (lender.result.result !== 'accepted' && lender.result.result !== 'warning' && !(lender.lenderName === 'bamboo' && lender.loanType.toLowerCase() === 'guarantor loan'));
                                                            });

                                                            if (guaranteedScores) {
                                                                guaranteedScores = guaranteedScores.sort(internals.sortAccepted);
                                                                finalScores = finalScores.concat(guaranteedScores);
                                                            }

                                                            if (acceptedScores) {
                                                                acceptedScores = acceptedScores.sort(internals.sortAccepted);
                                                                finalScores = finalScores.concat(acceptedScores);
                                                            }

                                                            if (otherScores) {
                                                                finalScores = finalScores.concat(otherScores);
                                                            }

                                                            let newStatus = '';
                                                            let newDatasetId;
                                                            let cheapestLender;

                                                            const oldStatus = dbLead.status;

                                                            if (finalScores[0].result && finalScores[0].result.result === 'accepted' || finalScores[0].result.result === 'warning') {
                                                                if (finalScores[0].loanType.toLowerCase().trim() === 'personal loan' || finalScores[0].loanType.toLowerCase().trim() === 'peer to peer loan') {
                                                                    newStatus = 'UNS';
                                                                    newDatasetId = 90; // UNS Accepts
                                                                } else if (finalScores[0].loanType.toLowerCase().trim() === 'homeowner loan') {
                                                                    if (finalScores[0].lenderName.toLowerCase().trim() === 'evolution') {
                                                                        newStatus = 'TPD';
                                                                        newDatasetId = 92; // TPD Evolution
                                                                    } else {
                                                                        newStatus = 'ENQ';
                                                                        newDatasetId = 97; // ENQ Secured Accepts
                                                                    }
                                                                } else if (finalScores[0].loanType.toLowerCase().trim() === 'guarantor loan') {
                                                                    newStatus = 'GUA';
                                                                    if (gmSnapshot.residencyStatusCode === 'HWM' || gmSnapshot.residencyStatusCode === 'H') {
                                                                        newDatasetId = 85; // GUA Guarantor Loan
                                                                    } else {
                                                                        newDatasetId = 84; // GUA Guarantor Loan
                                                                    }
                                                                }
                                                                cheapestLender = finalScores[0];
                                                            } else {
                                                                if (['ENQ', 'TUD', 'NOC'].indexOf(oldStatus) > -1) {
                                                                    newStatus = 'REJ';
                                                                    newDatasetId = 95; // REJ Online Declines
                                                                }
                                                            }

                                                            if (newDatasetId) {
                                                                if (newDatasetId !== 95 || (newDatasetId === 95 && (gmSnapshot.residencyStatusCode === 'HWM' || gmSnapshot.residencyStatusCode === 'HWM'))) {
                                                                    internals.server.plugins.dxi.moveRecordToDataset(dbLead.dxiId, newDatasetId)
                                                                        .catch((error) => {
                                                                        });
                                                                }

                                                                if (newDatasetId === 95 && (gmSnapshot.residencyStatusCode !== 'HWM' && gmSnapshot.residencyStatusCode !== 'H')) {
                                                                    internals.server.plugins.dxi.updateOutcome(dbLead.dxiId, 506410) // Close REJ Online Declines - Tenants
                                                                        .catch((error) => {
                                                                        });
                                                                }
                                                            }

                                                            if (newStatus && newStatus.length > 0) {

                                                                const recid = '_' + new Date().getTime();

                                                                internals.server.plugins.goldmine.getRecord(dbLead.goldmineId).then((goldmineRecord) => {
                                                                    if (goldmineRecord) {
                                                                        // Update lead status in CONTACT1 table (MSSQL database - Goldmine)
                                                                        let strNotes = '';
                                                                        if (newStatus === 'REJ') {
                                                                            strNotes = 'I am sorry, we have received your application but you have been declined by all lenders, we will review your application manually and contact you if we find a lender';
                                                                        } else {
                                                                            if (cheapestLender) {
                                                                                strNotes = 'Status changed to ' + newStatus + ' - Client has been ' + (cheapestLender.result.guaranteed === true ? 'Pre-Approved' : 'Provisionally Approved') + ' by ' + cheapestLender.lenderDisplayName;
                                                                            } else {
                                                                                strNotes = 'Status changed to ' + newStatus + ' - online scoring';
                                                                            }
                                                                        }

                                                                        const gmOptions = {
                                                                            id: dbLead.goldmineId,
                                                                            newStatus: newStatus,
                                                                            notes: '*** MASTER (Master) *** ' + today.getDate() + ' ' + monthNames[today.getMonth()] + ' ' + today.getFullYear() + ' ' + today.getHours() + ':' + today.getMinutes() + '\r\n' + strNotes,
                                                                        }
                                                                        internals.server.plugins.goldmine.updateRecordStatus(gmOptions)
                                                                            .then(() => {
                                                                                if (newStatus !== oldStatus) {
                                                                                    // Add record in CONTHIST table to log the status change (MSSQL database - Goldmine)
                                                                                    const gmConthistOptions = {
                                                                                        accountNo: goldmineRecord.accountNo,
                                                                                        oldStatus: oldStatus,
                                                                                        newStatus: newStatus,
                                                                                        recid: recid,
                                                                                    }
                                                                                    internals.server.plugins.goldmine.addStatusUpdateLog(gmConthistOptions)
                                                                                        .then(() => {
                                                                                            // Add record in LOG table to log the status change (MSSQL database - UpdateGM)
                                                                                            const updateGMLogOptions = {
                                                                                                accountNo: goldmineRecord.accountNo,
                                                                                                oldStatus: oldStatus,
                                                                                                newStatus: newStatus,
                                                                                            }
                                                                                            internals.server.plugins.updateGM.addStatusUpdateLog(updateGMLogOptions)
                                                                                                .then(() => {
                                                                                                    // Add record in LOGDISTINCT table to log the status change (MSSQL database - UpdateGM)
                                                                                                    internals.server.plugins.updateGM.addStatusUpdateLogDistinct(updateGMLogOptions)
                                                                                                        .then(() => {
                                                                                                            Request({
                                                                                                                method: 'GET',
                                                                                                                url: 'http://192.168.1.24/goldmine-records/' + goldmineRecord.id.toString() + '/status',
                                                                                                            }, (error, response, body) => {
                                                                                                            });
                                                                                                        })
                                                                                                        .catch((error) => {
                                                                                                        });
                                                                                                    // ------------------------------------------------------------------------------------
                                                                                                })
                                                                                                .catch((error) => {
                                                                                                });
                                                                                            // ----------------------------------------------------------------------------
                                                                                        })
                                                                                        .catch((error) => {
                                                                                        });
                                                                                    // ---------------------------------------------------------------------------------
                                                                                }
                                                                            })
                                                                            .catch((error) => {
                                                                            });
                                                                        // ---------------------------------------------------------------    
                                                                        // The below tasks no longer need to be created in GM
                                                                        /*try {
                                                                            let taskReference = '';
                                                                            if (newStatus === 'UNS') {
                                                                                taskReference = 'Chase Personal Loan Accept - ' + (cheapestLender ? cheapestLender.lenderDisplayName + (cheapestLender.result.guaranteed === true ? ' (Pre-Approved)' : '') : 'Unknown');
                                                                            } else if (newStatus === 'ENQ' || newStatus === 'TPD') {
                                                                                taskReference = 'Chase Secured Accept - ' + (cheapestLender ? cheapestLender.lenderDisplayName : 'Unknown');
                                                                            } else if (newStatus === 'GUA') {
                                                                                taskReference = 'Chase Guarantor Accept - ' + (cheapestLender ? cheapestLender.lenderDisplayName : 'Unknown');
                                                                            } else if (newStatus === 'REJ') {
                                                                                taskReference = 'Review Declined Application';
                                                                            }

                                                                            const gmCreateTaskOptions = {
                                                                                userId: (taskReference.toLowerCase().indexOf('evolution money') > -1 ? 'ANGELA' : 'UNSECURE'),
                                                                                accountNo: goldmineRecord.accountNo,
                                                                                customerName: goldmineRecord.app1.firstName + ' ' + goldmineRecord.app1.lastName,
                                                                                reference: taskReference,
                                                                                recid: recid,
                                                                            }
                                                                            internals.server.plugins.goldmine.createNewGMTask(gmCreateTaskOptions)
                                                                                .catch((error) => {
                                                                                });
                                                                        } catch (e) {
                                                                        }*/ 
                                                                        // -----------------------------------------------------------------                                                        
                                                                    }

                                                                }).catch((error) => {
                                                                });
                                                            }
                                                        }
                                                    } else {
                                                        try {
                                                            internals.server.plugins.sequelize.db.UnsecuredWebRequest.findAll({
                                                                where: {
                                                                    applicationId: dbLead.goldmineId,
                                                                },
                                                            }).then((unsecuredWebRequests) => {
                                                                let done = false;

                                                                if (dbLead.status === 'ENQ' && unsecuredWebRequests && unsecuredWebRequests.length > 0) {
                                                                    let incompleteAppLogs = unsecuredWebRequests.filter((webRequest) => {
                                                                        return webRequest.requestName.toString().toUpperCase() === 'INCOMPLETED APP';
                                                                    });
                                                                    if (incompleteAppLogs && incompleteAppLogs.length > 0 && Moment().diff(Moment(incompleteAppLogs[0].createdAt), 'minutes') >= 5) {
                                                                        done = true;
                                                                        if (lead.datasetid.toString() !== '82') {
                                                                            internals.server.plugins.dxi.moveRecordToDataset(dbLead.dxiId, 82) // Web leads abandons
                                                                                .catch((error) => {
                                                                                });
                                                                        }
                                                                    }
                                                                }

                                                                if (done === false && Moment().diff(dateReceived, 'minutes') >= 30) {
                                                                    if ((gmSnapshot.residencyStatusCode === 'HWM' || gmSnapshot.residencyStatusCode === 'H') && gmSnapshot.loanAmount >= 5000) {
                                                                        if (lead.datasetid !== '87') {
                                                                            internals.server.plugins.dxi.moveRecordToDataset(dbLead.dxiId, 87) // Web leads chasing homeowners
                                                                                .catch((error) => {
                                                                                });
                                                                        }
                                                                    } else {
                                                                        if (lead.datasetid !== '88') {
                                                                            internals.server.plugins.dxi.moveRecordToDataset(dbLead.dxiId, 88) // Web leads chasing tenants
                                                                                .catch((error) => {
                                                                                });
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        } catch (e) {
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                }, timeout);
                            }
                            timeout += 500;
                        });
                    }
                });
        }

        execute();
        setInterval(execute, 600000);
        return next();
    });

    return next();
};

internals.getRecordsFromWebPersonalLoansDiallerNewProspects = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 81, // Web personal loans dataset ID
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.webPersonalLeadsNewProspects = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansDiallerNeedsMoreWork = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 81, // Web personal loans dataset ID
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.webPersonalLeadsNeedsMoreWork = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansChasingHomeownersDiallerNewProspects = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 87, // Web personal loans dataset ID
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.webPersonalLeadsChasingHomeownersNewProspects = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansChasingHomeownersDiallerNeedsMoreWork = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 87, // Web personal loans dataset ID
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.webPersonalLeadsChasingHomeownersNeedsMoreWork = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansChasingTenantsDiallerNewProspects = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 88, // Web personal loans dataset ID
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.webPersonalLeadsChasingTenantsNewProspects = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansChasingTenantsDiallerNeedsMoreWork = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 88, // Web personal loans dataset ID
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.webPersonalLeadsChasingTenantsNeedsMoreWork = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansAbandonsDiallerNewProspects = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 82,
            'search[ProcessType]': 'New Prospect',
        })
            .then((results) => {
                internals.webPersonalLeadsAbandonsNewProspects = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getRecordsFromWebPersonalLoansAbandonsDiallerNeedsMoreWork = function () {
    return new Promise(function (resolve, reject) {
        return internals.server.plugins.dxi.findRecords({
            dataset: 82,
            'search[ProcessType]': 'NEEDSMOREWORK',
        })
            .then((results) => {
                internals.webPersonalLeadsAbandonsNeedsMoreWork = results;
                resolve();
            })
            .catch(function (error) {
                reject(new VError(error, 'error dxi.findRecords()'))
            });
    });
};

internals.getLenderDisplayName = function (lenderName) {
    if (lenderName) {
        let lenderDisplayName;
        if (lenderName.toLowerCase().indexOf('smart-quote') > -1) {
            lenderName = lenderName.replace('smart-quote-', '').replace(/-/g, ' ');

            let splitStr = lenderName.toLowerCase().split(' ');
            for (var i = 0; i < splitStr.length; i++) {
                splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
            }
            lenderDisplayName = splitStr.join(' ');
        } else {
            switch (lenderName.toLowerCase()) {
                case 'amigo':
                    lenderDisplayName = 'Amigo';
                    break;
                case 'amigo-online':
                    lenderDisplayName = 'Amigo';
                    break;
                case 'azure-money':
                    lenderDisplayName = 'Azure Money';
                    break;
                case 'evolution':
                    lenderDisplayName = 'Evolution Money';
                    break;
                case 'guarantor':
                    lenderDisplayName = 'Guarantor My Loan';
                    break;
                case 'everydayloans':
                    lenderDisplayName = 'Everyday Loans';
                    break;
                case 'shawbrook-online':
                    lenderDisplayName = 'Shawbrook Bank';
                    break;
                case 'mcb':
                    lenderDisplayName = 'My Community Finance';
                    break;
                case 'firststop-group':
                    lenderDisplayName = 'First Stop Personal Loans';
                    break;
                case 'likelyloans':
                    lenderDisplayName = 'Likely Loans';
                    break;
                case 'lendable':
                    lenderDisplayName = 'Lendable';
                    break;
                case 'lendingworks':
                case 'lendingworks-online':
                    lenderDisplayName = 'Lending Works';
                    break;
                case 'bamboo':
                    lenderDisplayName = 'Bamboo';
                    break;
                case 'progressive':
                    lenderDisplayName = 'Progressive';
                    break;
                case 'hitachi':
                    lenderDisplayName = 'Hitachi Capital Finance';
                    break;
                case 'besavvi':
                    lenderDisplayName = 'Besavvi';
                    break;
                case 'zopa':
                    lenderDisplayName = 'Zopa';
                    break;
                case '1plus1loans':
                    lenderDisplayName = '1Plus1 Loans';
                    break;
                case 'leap-lending':
                    lenderDisplayName = 'Leap Lending';
                    break;
                case 'george-banco':
                    lenderDisplayName = 'George Banco';
                    break;
                case 'trust-two':
                    lenderDisplayName = 'Trust Two';
                    break;
                case 'buddy-loans':
                    lenderDisplayName = 'Buddy Loans';
                    break;
                case 'livelend':
                    lenderDisplayName = 'LiveLend';
                    break;
                case 'betterborrow':
                    lenderDisplayName = 'BetterBorrow';
                    break;
                case 'koyo-loans':
                    lenderDisplayName = 'Koyo Loans';
                    break;
                case 'oplo':
                    lenderDisplayName = 'Oplo';
                    break;
                case 'tm-advances':
                    lenderDisplayName = 'TM Advances';
                    break;
                case '118118-money':
                    lenderDisplayName = '118118 Money';
                    break;
                case 'rac':
                    lenderDisplayName = 'RAC';
                    break;
                case 'salad-money':
                    lenderDisplayName = 'Salad Money';
                    break;
                case 'fintern':
                    lenderDisplayName = 'Fintern';
                    break;
                default:
                    lenderDisplayName = lenderName;
                    break;
            }
        }
        return lenderDisplayName;
    }
    return lenderName;
};

internals.sortAccepted = function (lender1, lender2) {
    let sortBy = [{
        prop: 'loanAmount',
        direction: -1
    }, {
        prop: 'apr',
        direction: 1
    }];

    let i = 0, result = 0;
    while (i < sortBy.length && result === 0) {
        result = sortBy[i].direction * (parseFloat(lender1.result[sortBy[i].prop]) < parseFloat(lender2.result[sortBy[i].prop]) ? -1 : (parseFloat(lender1.result[sortBy[i].prop]) > parseFloat(lender2.result[sortBy[i].prop]) ? 1 : 0));
        i++;
    }
    return result;
};


exports.register.attributes = {
    name: 'unsecured-applications-chasing',
};
