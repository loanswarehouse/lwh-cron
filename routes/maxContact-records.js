'use strict';

const internals = {
	handlers: {},
};

exports.register = function(server, options, next) {
    server.route([
		{
			method: 'GET',
			path: '/max-contact/list-data',
			config: internals.handlers.listData
		}

    ])
    return next();
}

internals.handlers.listData = {
    tags: ['api'],
	description: 'Endpoint to retrieve list data',
	plugins: {
		'hapi-swagger': {
			responses: {
				200: { description: 'Success' },
				400: { description: 'Bad Request' },
				404: { description: 'Not Found' },
			},
		},
    },
    
	handler: function (request, reply) {
        var response = {
            list: []
        };
        const listIds = [117, 118, 75, 76, 8, 35, 9, 39, 7, 36, 16, 38, 80, 81, 82, 116,10, 150].join(',');
        request.server.plugins.maxContactRep.fetchListData(listIds)
            .then((listData) => {
                const diallers = {
                    chooseWisely: {
                        id: 117118,
                        data: []
                    },
                    btPostcodes: {
                        id: 7576,
                        data: [],
                    },
                    lessThan20000: {
                        id: 835,
                        data: []
                    },
                    msa: {
                        id: 939,
                        data: []
                    },
                    moreThan20000:  {
                        id: 736,
                        data: []
                    },
                    evolutionProceeds: {
                        id: 1638,
                        data: []
                    },
                    overThreeDaysLessThan20000: {
                        id: 81,
                        data: [],
                    },
                    overThreeDaysMoreThan2000: {
                        id: 80,
                        data: []
                    }, 
                    overThreeDaysMsa: {
                        id: 82,
                        data: []
                    },
                    securedManuals: {
                        id: 11610,
                        data: []
                    },
                    brokerLeads: {
                        id: 150,
                        data: []
                    }
                };
                listData.forEach((element) => {
                    if (element.list_id === 117 || element.list_id === 118) {
                       
                        diallers.chooseWisely.data.push(element);
                    } else if (element.list_id === 75 || element.list_id === 76) {
                        diallers.btPostcodes.data.push(element)
                    } else if (element.list_id === 8 || element.list_id === 35) {
                        diallers.lessThan20000.data.push(element);
                        
                    } else if (element.list_id === 9 || element.list_id === 39) {
                        
                        diallers.msa.data.push(element);
                    } else if (element.list_id ===  7 || element.list_id === 36) {
                        diallers.moreThan20000.data.push(element);
                        
                    } else if (element.list_id ===  16 || element.list_id === 38) {
                        
                        diallers.evolutionProceeds.data.push(element);
                    } else if (element.list_id ===  81) {
                        diallers.overThreeDaysLessThan20000.data.push(element);
                       
                    } else if (element.list_id ===  80 ) {
                        diallers.overThreeDaysMoreThan2000.data.push(element);
                    } else if (element.list_id ===  82) {
                        diallers.overThreeDaysMsa.data.push(element);
                    } else if (element.list_id === 116 || element.list_id === 10) {
                        diallers.securedManuals.data.push(element);
                        
                    } else if (element.list_id ===  150 ) {
                        diallers.brokerLeads.data.push(element);
                    }
                });
                Object.keys(diallers).forEach((key) => {
                    var fresh = 0;
                    var availableNow = 0;
                    diallers[key].data.forEach((item) => {
                        if (item.total_attempts === 0) {
                            if (item.is_fetched) {
                                if (item.last_result_code !== 'ENDCALL') {
                                    fresh++;
                                }
                            }
                        } else if (item.is_fetched) {
                            availableNow++;
                        }
                    })
                    response.list.push({
                        qid: diallers[key].id,
                        fresh: fresh,
                        further_later: diallers[key].data.length - availableNow,
                        further_now: availableNow
                    });
                });
                reply(response);
            })
            .catch((e) => {
                console.log(e);
                reply(response);
            })
    }
}
exports.register.attributes = {
	name: 'max-contact-records',
};