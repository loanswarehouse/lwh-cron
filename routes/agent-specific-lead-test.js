'use strict';

const Boom = require('boom');
const Promise = require('bluebird');
const Joi = require('joi');
const VError = require('verror');
const Moment = require('moment-timezone');

const internals = {
	handlers: {},
};

exports.register = function (server, options, next) {
	server.route([{
		method: 'GET',
		path: '/agent-specific-lead',
		config: internals.handlers.testAgentSpecificLead,
	}, {
		method: 'GET',
		path: '/scp-endpoint-test',
		config: internals.handlers.testingForScp,
	}]);

	internals.server = server;

	internals.securedAgents = [
		{ agentId: '578429', name: 'Luke Hawes', dataset: 'secured23' },
		{ agentId: '779506', name: 'Toni Lamberti', dataset: 'secured27' },
		{ agentId: '878153', name: 'Shannon Stocks', dataset: 'secured22' },
		{ agentId: '878053', name: 'Chris Rex', dataset: 'secured26' },
		{ agentId: '875803', name: 'Marlon Martins', dataset: 'secured24' },
		{ agentId: '798672', name: 'John Pullen', dataset: 'secured28' },
	];

	internals.unsecuredAgents = [
		{ agentId: '743148', name: 'James Irving', dataset: 'unsecured34' },
		{ agentId: '647882', name: 'Sariah Bagley', dataset: 'unsecured35' },
		{ agentId: '796536', name: 'Kate Carns', dataset: 'unsecured33' },
	];

	server.dependency(['sqs', 'sns', 'goldmine', 'dxi', 'sequelize'], function (server, next) { // This is already found in dxi-ms
		return next();
	});

	return next();
};

internals.isWorkingHours = function (timezone) {
	const workingHours = [
		[undefined, undefined], // Sunday
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Monday 08:00 - 19:00
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Tuesday 08:00 - 19:00
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Wednesday 08:00 - 19:00
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(18, 'hours')], // Thursday 08:00 - 19:00
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(17, 'hours').add(30, 'minutes')], // Friday 08:00 - 17:30
		[Moment().tz(timezone).startOf('day').add(9, 'hours'), Moment().tz(timezone).startOf('day').add(14, 'hours')], // Saturday 09:00 - 14:00
	];
	const today = (Moment().tz(timezone).day());
	return Moment().tz(timezone).isBetween(workingHours[today][0], workingHours[today][1]);
};

internals.handlers.testingForScp = {
	tags: ['api'],
	description: 'Endpoint test path used only for SCP to test connection.',
	notes: 'Only used for testing this endpoint by SCP.',
	plugins: {
		'hapi-swagger': {
			responses: {
				200: { description: 'Success' },
				400: { description: 'Bad Request' },
				404: { description: 'Not Found' },
			},
		},
	},
	validate: {},
	handler: function (request, reply) {
		reply("OK");
	}
}

internals.handlers.testAgentSpecificLead = {
	tags: ['api'],
	description: 'Test the functionality of agent specific lead feature',
	notes: 'Perform as a new lead came in and needs to be assigned to an available agent',
	plugins: {
		'hapi-swagger': {
			responses: {
				200: { description: 'Success' },
				400: { description: 'Bad Request' },
				404: { description: 'Not Found' },
			},
		},
	},
	validate: {},
	handler: function (request, reply) {
		const data = {};
		_getDXIAgents().then(() => reply(data.dxiAgents)).catch((error) => reply(error));
		//.then(_AssignLeadToAgent)
		//.then(_createDxiRecord);

		//internals.getLeadsFromHoldingPot();


		function _getDXIAgents() {
			return new Promise(function (resolve, reject) {
				request.server.plugins.dxi.getAllAgents(internals.isWorkingHours('Europe/London'))
					.then((response) => {
						if (response) {
							data.dxiAgents = response.list;
						}
						return resolve();
					})
					.catch((error) => reject(new VError(error, 'error plugins.dxi.getAllAgents()')));
			});
		}

		function _AssignLeadToAgent() {
			return new Promise(function (resolve, reject) {
				const generalDataset = 'secured';

				if (generalDataset.length > 0) {
					if (generalDataset === 'secured') {
						request.server.plugins.sequelize.db.AgentsOrder.findAll({
							limit: 1,
							where: {
								generalDataset: 'secured',
							},
							order: [['id', 'DESC']],
						})
							.then(function (entries) {
								let i = 0;
								let currentOrder = [];
								let selectedAgentId = '';

								if (entries && entries.length > 0) {
									currentOrder = JSON.parse(entries[0].currentOrder);
								}
								else {
									for (i = 0; i < internals.securedAgents.length; i++) {
										currentOrder.push(internals.securedAgents[i].agentId);
									}
								}

								if (currentOrder && currentOrder.length > 0) {
									if (data.dxiAgents) {
										for (i = 0; i < currentOrder.length; i++) {

											const agent = data.dxiAgents.find((item) => {
												return item.uid === currentOrder[i];
											});

											if (agent && agent.status && agent.status.toString().toLowerCase().trim() !== 'offline') {
												// Any status other than offline means that the agent is logged in to dialer.
												selectedAgentId = currentOrder[i];
											}


											if (selectedAgentId !== '') {
												// Exit for loop since we found an available agent.
												break;
											}
										}
									}
									else {
										selectedAgentId = currentOrder[0];
									}


									if (selectedAgentId === '') {
										selectedAgentId = currentOrder[0];
									}

									// Get agent dataset name.
									const securedAgent = internals.securedAgents.find(function (item) {
										return item.agentId === selectedAgentId;
									});

									data.dataset = securedAgent.dataset;

									// Move the id of the selected agent to the end of the array.
									currentOrder.push(currentOrder.splice(currentOrder.indexOf(selectedAgentId), 1)[0]);

									const lastLeadAssignedTo = {
										leadId: 1,
										goldmineId: 1,
										dataset: securedAgent.dataset,
										agent: securedAgent.name,
									};

									const params = {
										generalDataset: 'secured',
										lastLeadAssignedTo: JSON.stringify(lastLeadAssignedTo),
										currentOrder: JSON.stringify(currentOrder),
									};

									request.server.plugins.sequelize.db.AgentsOrder.create(params);

									return resolve();
								}
							})
							.catch((error) => reject(new VError(error, 'error plugins.sequelize.db.AgentsOrder.findAll()')));
					}
					else {
						data.dataset = generalDataset;
						return resolve();
					}
				}
				else {
					return reject(new VError('didn\'t fit any previous criterias'));
				}

			});
		}

		function _createDxiRecord() {
			return new Promise((resolve, reject) => {
				if (data.dataset === 'skip') {
					return resolve();
				}
			});
		}

	},
};

internals.getDXIAgents = function () {
	return new Promise(function (resolve, reject) {
		internals.server.plugins.dxi.getAllAgents(internals.isWorkingHours('Europe/London'))
			.then((response) => {
				if (response) {
					internals.dxiAgents = response.list;
				}
				return resolve();
			})
			.catch((error) => reject(new VError(error, 'error plugins.dxi.getAllAgents()')));
	});
}

internals.getLeadsFromHoldingPot = function () {
	return new Promise(function (resolve, reject) {
		return internals.server.plugins.dxi.findRecords({
			dataset: 64, // Holding pot dataset
		})
			.then((results) => {
				if (results) {
					let sortedLeads = [];
					const newProspects = results.filter((item) => {
						return item.ProcessType.toLowerCase() === 'new prospect';
					});
					const otherLeads = results.filter((item) => {
						return item.ProcessType.toLowerCase() !== 'new prospect' && item.ProcessType.toLowerCase() !== 'complete';
					});

					if (newProspects) {
						sortedLeads = sortedLeads.concat(newProspects);
					}

					if (otherLeads) {
						sortedLeads = sortedLeads.concat(otherLeads);
					}

					resolve(sortedLeads);
				}
			})
			.catch((error) => reject(new VError(error, 'error dxi.findRecords()')));
	});
};

exports.register.attributes = {
	name: 'routes-agent-specific-lead-test',
};