'use strict';

const pm2 = require('pm2');
const Request = require('request');
const Boom = require('boom');

const internals = {
    handlers: {}
};

exports.register = function (server, options, next) {
    server.route([
		{
			method: 'GET',
			path: '/pm2-control',
			config: internals.handlers.pm2Control
		}

    ])
    return next();
}

internals.handlers.pm2Control = {
	tags: ['api'],
	description: 'Emdpoint to restart lwh-hapi-api',
	plugins: {
		'hapi-swagger': {
			responses: {
				200: { description: 'Success' },
				400: { description: 'Bad Request' },
				404: { description: 'Not Found' },
			},
		},
    },
	handler: function (request, reply) {
		let myTimeOut;
        let isSetTimeoutRunning = false;
        _checkLoginRoute()
        .then(() => {
            clearTimeout(myTimeOut);
            reply('OK');
        })
        .catch((error) => {
            reply(error);
            clearTimeout(myTimeOut);
        })
        function _checkLoginRoute() {
            return new Promise((resolve, reject) => {
                Request({
                    url: 'https://ron.loanswarehouse.co.uk:40001/token/login',
                    method: 'POST',
                    json: {
                        username: "randomUser",
                        password: "*foo7*4"
                    },
                    strictSSL: false
                }, function(error, response, body) {
                    if (!isSetTimeoutRunning) {
                        return reject(Boom.badRequest('Server is running OK'));
                    }
                })
                
                myTimeOut = setTimeout(function() {
                    isSetTimeoutRunning = true;
                    pm2.connect(function(err) {
                        if (err) {
                            reject();
                            console.error('pm2 error at connect: ', err);
                            process.exit(2);
                        } else {
                            pm2.restart('lwh-hapi-api', (err, proc) => {
                                // Disconnects from PM2 on success or failure
                                pm2.disconnect();
                                if (err) {
                                    return reject(err);
                                }
                                resolve();
                            })
                        }
                    })
                }, 5000);
            })
        }
	}
}

exports.register.attributes = {
    name: 'routes-pm2-control'
}