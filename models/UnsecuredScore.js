'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('UnsecuredScore', {
		applicationReference: DataTypes.UUID, // Used for the new API
		requestUuid: DataTypes.STRING,
		goldmineId: DataTypes.STRING,
		lenderId: DataTypes.INTEGER,		// Used for the new API
		loanTypeId: DataTypes.INTEGER,		// Used for the new API	
		lenderName: DataTypes.STRING,
		lenderRequest: DataTypes.TEXT,
		lenderResponse: DataTypes.TEXT,
		requestResponse: DataTypes.TEXT,
		result: DataTypes.STRING,
		customerAccepted: DataTypes.DATE,
		expiresAt: DataTypes.DATE,
		author: DataTypes.STRING,
		meta: DataTypes.TEXT,
		lenderStatusUpdates: DataTypes.TEXT,
	});
};
