'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('SalesScript', {
		goldmineId: DataTypes.STRING,
		application: DataTypes.TEXT,
		isCompleted: {
			type: DataTypes.BOOLEAN,
			defaultValue: 0,
		},
		author: DataTypes.STRING,
		meta: DataTypes.TEXT,
	});
};
