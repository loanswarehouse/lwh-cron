'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('Template', {
		type: DataTypes.STRING,
		body: DataTypes.TEXT,
	});
};
