'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('BulkEmailJob', {
		status: DataTypes.STRING,
		templateId: DataTypes.STRING,
		goldmineRecordsCount: DataTypes.INTEGER,
		meta: DataTypes.TEXT,
		author: DataTypes.STRING,
	});
};
