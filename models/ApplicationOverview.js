'use strict';

module.exports = function (sequelize, DataTypes) {
    let overview = sequelize.define('ApplicationOverview', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        applicationId: DataTypes.INTEGER,
        receivedOn: DataTypes.DATE,
        introducer: DataTypes.STRING,
        introducerReferenceNumber: DataTypes.STRING,
        introducerLoanAmount: DataTypes.FLOAT,
        residencyStatus: DataTypes.STRING,
        launchSource: DataTypes.STRING, // Webpage, Email, Text        
        enquiry: {
            type: DataTypes.BOOLEAN,
            defaultValue: 0,
        },
        enquiryDate: DataTypes.DATE,
        obCompleted: {
            type: DataTypes.BOOLEAN,
            defaultValue: 0,
        },
        obAbandoned: {
            type: DataTypes.BOOLEAN,
            defaultValue: 0,
        },
        obSkipped: {
            type: DataTypes.BOOLEAN,
            defaultValue: 0,
        },
        eligible: {
            type: DataTypes.BOOLEAN,
            defaultValue: 0,
        },
        offeredLoanTypes: DataTypes.STRING,
        proceed: {
            type: DataTypes.BOOLEAN,
            defaultValue: 0,
        },
        proceedSource: DataTypes.STRING, // Webpage, Email, Text
        proceedLenderName: DataTypes.STRING,
        proceedLoanAmount: DataTypes.FLOAT,
        proceedLoanType: DataTypes.STRING,
        signed: {
            type: DataTypes.BOOLEAN,
            defaultValue: 0,
        },
        guarantorSigned: {
            type: DataTypes.BOOLEAN,
            defaultValue: 0,
        },
        completed: {
            type: DataTypes.BOOLEAN,
            defaultValue: 0,
        },
        completionDate: DataTypes.DATE,
        commission: DataTypes.FLOAT,
    });
    return overview;
};