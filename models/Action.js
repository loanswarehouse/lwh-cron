'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('Action', {
		goldmineId: DataTypes.STRING,
		type: DataTypes.STRING, // message_send
		executed: {
			type: DataTypes.BOOLEAN,
			defaultValue: 0,
		},
		cancelled: {
			type: DataTypes.BOOLEAN,
			defaultValue: 0,
		},
		errored: {
			type: DataTypes.BOOLEAN,
			defaultValue: 0,
		},
		locked: {
			type: DataTypes.BOOLEAN,
			defaultValue: 0,
		},
		meta: DataTypes.TEXT,
		execution_localtime: DataTypes.STRING,
		execution_timezone: DataTypes.STRING,
		execution_utc: DataTypes.DATE,
		executed_utc: DataTypes.DATE,
		note: DataTypes.TEXT,
	});
};
