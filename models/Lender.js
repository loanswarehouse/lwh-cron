'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('Lender', {
		lenderName: DataTypes.STRING,
		loanSize: DataTypes.STRING,
		term: DataTypes.STRING,
		rate: DataTypes.STRING,
		ltv: DataTypes.STRING,
		minPropertyValue: DataTypes.FLOAT,
		loanPurposeOptions: DataTypes.STRING,
		countries: DataTypes.STRING,
		showInSecured: DataTypes.BOOLEAN,
		showInUnsecured: DataTypes.BOOLEAN,
		showInBridging: DataTypes.BOOLEAN,
	});
};