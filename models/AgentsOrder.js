'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('AgentsOrder', {
		generalDataset: DataTypes.TEXT,
		lastLeadAssignedTo: DataTypes.TEXT,
		currentOrder: DataTypes.TEXT,
	});
};