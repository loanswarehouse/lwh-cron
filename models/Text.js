'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('Text', {
		goldmineId: DataTypes.STRING,
		actionId: DataTypes.STRING,
		templateId: DataTypes.STRING,
		mobile: DataTypes.STRING,
		body: DataTypes.TEXT,
		result: DataTypes.TEXT,
		success: {
			type: DataTypes.BOOLEAN,
			defaultValue: 0,
		},
		sqsMessageId: DataTypes.STRING,
		snsMessageId: DataTypes.STRING,
	});
};
