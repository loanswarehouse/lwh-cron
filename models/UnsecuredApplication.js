'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('UnsecuredApplication', {
		goldmineId: DataTypes.STRING,
		applicationReference: DataTypes.UUID, // Used for the new API
		application: DataTypes.TEXT,
		author: DataTypes.STRING,
		meta: DataTypes.TEXT,
	});
};
