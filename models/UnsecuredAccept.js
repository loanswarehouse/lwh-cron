'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('UnsecuredAccept', {
		applicationReference: DataTypes.UUID, // Used for the new API
		requestUuid: DataTypes.STRING,
		goldmineId: DataTypes.STRING,
		scoreId: DataTypes.STRING,
		lenderName: DataTypes.STRING,
		lenderRequest: DataTypes.TEXT,
		requestResponse: DataTypes.TEXT,
		lenderResponse: DataTypes.TEXT,
		author: DataTypes.STRING,
		meta: DataTypes.TEXT,
	});
};
