'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('UserGroup', {
		groupName: DataTypes.STRING,
	});
};
