'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('StatusChange', {
		goldmineId: DataTypes.INTEGER,      // goldmineId
		goldmineSnapshot: DataTypes.TEXT,   // JSON snapshot of the goldmine model, excluding notes
		changedTo: DataTypes.STRING,        // status changed to
		changedBy: DataTypes.STRING,        // status changed by
		recid: DataTypes.STRING,            // goldmine recid of status change
	}, {
		instanceMethods: {
			getChangedFrom: () => {}, // TODO: Add the getChangedFrom() instanceMethod from lwh-admin-api and test
		},
	});
};
