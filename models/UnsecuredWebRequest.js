'use strict';

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('UnsecuredWebRequest', {
        requestUuid: DataTypes.STRING,
        applicationId: DataTypes.INTEGER,
		partnerReference: DataTypes.STRING, // Used for the new API
		applicationReference: DataTypes.UUID, // Used for the new API
        requestName: DataTypes.STRING,
        requestBody: DataTypes.TEXT,
        responseBody: DataTypes.TEXT,
        serverLog: DataTypes.TEXT,
		triggeredLeadCreation: DataTypes.BOOLEAN, // Used for the new API
        author: DataTypes.STRING,
        meta: DataTypes.TEXT,
    });
};
