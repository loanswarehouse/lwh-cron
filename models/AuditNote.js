'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('AuditNote', {
		belongs_to: DataTypes.STRING,
		type: DataTypes.STRING,     // e.g. lender or some other actions in the future
		lender: DataTypes.STRING,   // e.g. shawbrook
		action: DataTypes.STRING,   // credit Check, update status, etc
		note: DataTypes.TEXT,       // TODO - user to add if necessary in future
		result: DataTypes.TEXT,
		meta: DataTypes.TEXT,
		author: DataTypes.STRING,
	});
};
