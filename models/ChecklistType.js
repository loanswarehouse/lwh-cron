'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('ChecklistType', {
		name: { // iHaveReadThis
			type: DataTypes.STRING,
			unique: true,
		},
		type: DataTypes.STRING, // button, checkbox, radio, boolean? (correct/incorrect)
		meta: DataTypes.TEXT,
	});
};
