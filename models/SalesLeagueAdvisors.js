'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('SalesLeagueAdvisorParticipant', {
		name: {
			type: DataTypes.STRING,
			allowNull: false,
			unique: true,
		},
		sold: DataTypes.INTEGER,
		wip: DataTypes.INTEGER,
		slc: DataTypes.INTEGER,
	});
};

