'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('Lead', {
		goldmineId: DataTypes.STRING,
		status: DataTypes.STRING,
		referral: DataTypes.STRING,
		messageId: DataTypes.STRING,
		subject: DataTypes.STRING,
		dxiId: DataTypes.STRING,
		dxiCreated: DataTypes.INTEGER,
		dxiOtherActions: DataTypes.INTEGER,
		dataset: DataTypes.STRING,
		reason: DataTypes.STRING,
		snapshot: DataTypes.TEXT,
		meta: DataTypes.TEXT,
	});
};
