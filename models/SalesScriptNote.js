'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('SalesScriptNote', {
		goldmineId: {
			type: DataTypes.STRING,
			allowNull: false,
			unique: true,
			primaryKey: true,
		},
		note: DataTypes.TEXT,
		author: DataTypes.STRING,
		meta: DataTypes.TEXT,
	});
};
