'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('SecuredSubmission', {
		requestUuid: DataTypes.STRING,
		goldmineId: DataTypes.STRING,
		lenderName: DataTypes.STRING,
		lenderRequest: DataTypes.TEXT,
		lenderResponse: DataTypes.TEXT,
		result: DataTypes.STRING,
		author: DataTypes.STRING,
		meta: DataTypes.TEXT,
	});
};
