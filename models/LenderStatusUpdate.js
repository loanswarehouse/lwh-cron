'use strict';

module.exports = function (sequelize, DataTypes) {
    let lenderStatusUpdate = sequelize.define('LenderStatusUpdate', {
        applicationId: DataTypes.INTEGER,
        applicationReference: DataTypes.STRING,
        partnerName: DataTypes.STRING,
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        lenderName: DataTypes.STRING,
        lenderReference: DataTypes.STRING,
        loanAmount: DataTypes.FLOAT,
        lenderPayload: DataTypes.TEXT,
        lenderStatus: DataTypes.STRING,
        lenderStatusDescription: DataTypes.TEXT,
    });
    return lenderStatusUpdate;
};