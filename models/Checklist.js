'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('Checklist', {
		goldmineId: DataTypes.STRING,
		name: DataTypes.STRING,
		checklistType: DataTypes.STRING, // 'iHaveReadThis' (treating it as unique id)
		result: DataTypes.STRING, // not boolean, for future use, e.g. radio, save result
		author: DataTypes.STRING, // who, 
		meta: DataTypes.TEXT,
	});
};

/*
 {{checklist-item goldmineId=model.id name='salesScriptDataProtection' type='iHaveReadThis'}}
 {{checklist-item goldmineId=model.id name='salesScriptPanelSelection' type='iHaveReadThis'}}
 {{checklist-item goldmineId=model.id name='salesScriptInitialDisclosure' type='iHaveReadThis'}}

 {{checklist-item goldmineId=model.id name='salesScriptInitialDisclosure' type='thisIsCorrect'}}

 Checklist table on submitted:

	 goldmineId: model.id
	 name: salesScriptDataProtection
	 type: 1
	 isFulfilled: 1
	 author: Karen

 When loading component, know what to render, do GET /checklists-types?name=iHaveReadThis
    => then, see if already done - GET /checklists?goldmineId=1&name=salesScriptDataProtection // 404 || 200

 */
