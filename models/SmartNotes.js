'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('SmartNotes', {
		result: DataTypes.TEXT,
		meta: DataTypes.TEXT,
		author: DataTypes.STRING,
	});
};