'use strict';

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('OBRequest', {
        applicationReference: DataTypes.UUID, // Used for the new API
        provider: DataTypes.STRING,
        url: DataTypes.STRING,
        requestHeaders: DataTypes.TEXT,
        requestBody: DataTypes.TEXT,
        responseBody: DataTypes.TEXT,
        author: DataTypes.STRING,
        meta: DataTypes.TEXT
    });
};
