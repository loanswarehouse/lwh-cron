'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('NewBusinessSecuredScript', {
		pre_script: DataTypes.TEXT,
		applicant: DataTypes.TEXT,
		applicant1: DataTypes.TEXT,
		applicant2: DataTypes.TEXT,
		applicant3: DataTypes.TEXT,
		applicant4: DataTypes.TEXT,
		loan: DataTypes.TEXT,
		property: DataTypes.TEXT,
		mortgage: DataTypes.TEXT,
		final: DataTypes.TEXT,
		result: DataTypes.TEXT,
		meta: DataTypes.TEXT,
		author: DataTypes.STRING,
	});
};
