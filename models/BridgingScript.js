'use strict';

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('BridgingScript', {
		openingQualifyingInfo: DataTypes.TEXT,
		quoteCalculator: DataTypes.TEXT,
		client: DataTypes.TEXT,
		security: DataTypes.TEXT,
		productShortlist: DataTypes.TEXT,
		result: DataTypes.TEXT,
		meta: DataTypes.TEXT,
		author: DataTypes.STRING,
	});
};